#large, no-limit
#parameters are
./lcif PRUNING_PERF ./data/medical-train.libsvm ./data/medical-test.libsvm
./lcif PRUNING_PERF ./data/corel5k-train.libsvm ./data/corel5k-test.libsvm
./lcif PRUNING_PERF ./data/bibtex-train.libsvm ./data/bibtex-test.libsvm
./lcif PRUNING_PERF ./data/delicious-train.libsvm ./data/delicious-test.libsvm
./lcif PRUNING_PERF ./data/IMDB-F-train.libsvm ./data/IMDB-F-test.libsvm

#extreme, no-limit
./lcif PRUNING_PERF ./data/XMLRepository/Eurlex/eurlex_train.txt  ./data/XMLRepository/Eurlex/eurlex_test.txt
./lcif PRUNING_PERF ./data/XMLRepository/Wiki10/wiki10_train.txt  ./data/XMLRepository/Wiki10/wiki10_test.txt
./lcif PRUNING_PERF ./data/XMLRepository/RCV1-x/rcv1x_train.txt ./data/XMLRepository/RCV1-x/rcv1x_test.txt
./lcif PRUNING_PERF ./data/XMLRepository/AmazonCat/amazonCat_train.txt ./data/XMLRepository/AmazonCat/amazonCat_test.txt
./lcif PRUNING_PERF ./data/XMLRepository/WikiLSHTC/wikiLSHTC_train.txt ./data/XMLRepository/WikiLSHTC/wikiLSHTC_test.txt
