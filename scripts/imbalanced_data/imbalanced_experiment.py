#Script
import pprint
from imbalanced_utils import *
import numpy as np
from collections import Counter

do_run_lcif = True

def run(datasetname, dataset_train, dataset_test, do_run_smote, start_labels=0, end_labels=-1):
    dataset_train_lcif = dataset_train[1:] #input, relative to LCIF_PATH (so ./data, not ../data)
    dataset_test_lcif = dataset_test[1:] #input, relative to LCIF_PATH
    if do_run_lcif:
        run_lcif('GS','macroF1_2',dataset_train_lcif,dataset_test_lcif)
    #LCIF:
    #Best parameters: kRow=26, kRowPower=1.05, kFeature=1, kFeaturePower0.75, lambda:0.27
    #macroF1:  Ins. KNN: 0.345 Feat. KNN: 0.336 LCIF: 0.340
    #macroF1_2:  Ins. KNN: 0.066 Feat. KNN: 0.064 LCIF: 0.062
    predictions_lcif_macro = '../temp/predictions.txt' #output
    
    
    test_data = parse_file_libsvm(dataset_test)
    print(datasetname + " Loading test data {} instances".format(len(test_data)))
    
    test_predictions = parse_file_predictions(predictions_lcif_macro)
    print(datasetname + " Loading predictions data {} instances".format(len(test_predictions)))
    
    #Plot label/label-prediction distributions
    #threshold predicted label, that is take top-5 labels (fcard is 3.5 for corel5k)
    print(datasetname + ' Top 5-labels predicted')
    test_predictionstop5 = []
    for i in range(0,len(test_predictions)):
        test_predictionstop5.append(test_predictions[i][0:5])
    macro1 = evaluate(test_data, test_predictionstop5)
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(macro1)
    show_distribution(datasetname + ' top-5 labels predicted', test_data, test_predictionstop5,start=start_labels,end=end_labels)
    
    #macroF1(test_data, test_predictionstop5)
    evaluate(test_data, test_predictionstop5)
    evaluateCPP(test_data, test_predictionstop5)
    
    #Compute macro results and plot distribution after single threshold
    print(datasetname + ' Running single threshold')
    test_predictionsSTInput = {}
    for i in range(0,len(test_predictions)):
        test_predictionsSTInput[i] = test_predictions[i]
    train_data = parse_file_libsvm(dataset_train)
    vals = [len(labels) for labels,features in  train_data]
    label_cardinality = np.mean(vals) #measures on training data
    #run single threshold
    test_predictions_st_dict = apply_single_threshold(label_cardinality, test_predictionsSTInput)
    #post-processing
    test_predictions_st = [[] for i in range(0,len(test_predictions_st_dict))]
    for testuid, predictionsdict in test_predictions_st_dict.items():
        test_predictions_st[testuid] = [(label, one_value) for label, one_value in predictionsdict.items()]
    #evaluate and plot
    macro1 = evaluate(test_data, test_predictions_st)
    pp.pprint(macro1)
    #macro2 = evaluateCPP(test_data, test_predictions_st)
    show_distribution(datasetname + ' single threshold', test_data, test_predictions_st,start=start_labels,end=end_labels)
    #Compute macro results and plot distribution after ranked based threshold
    print(datasetname + ' Running rank-based threshold')
    test_predictionsSTInput = {}
    for i in range(0,len(test_predictions)):
        test_predictionsSTInput[i] = test_predictions[i]
    countNoTrainingSamplesWithLabel = Counter()
    for labels,features in train_data: #WAS TEST DATA!!!
        for label in labels:
            countNoTrainingSamplesWithLabel[label]+=1
    #run
    test_predictions_mt_dict = apply_rank_based_threshold(len(train_data),len(test_data),countNoTrainingSamplesWithLabel,
                                                          test_predictionsSTInput,smooth=2,verbose=True)
    
    test_predictions_mt = [[] for i in range(0,len(test_predictions_mt_dict))]
    for testuid, predictionsdict in test_predictions_mt_dict.items():
        test_predictions_mt[testuid] = [(label, one_value) for label, one_value in predictionsdict.items()]
    #evaluate and plot
    macro1 = evaluate(test_data, test_predictions_mt)
    #macro2 = evaluateCPP(test_data, test_predictions_mt)
    pp.pprint(macro1)
    show_distribution(datasetname + ' rank threshold', test_data, test_predictions_mt,start=start_labels,end=end_labels)
    
    #Try different instance knn, with feature selection per label
    #new_instance_knn(792, train_data, test_data)
    
    if do_run_smote:
        #Try Smote-like sampling
        print(datasetname + ' Running smote')
        cnt = Counter()
        for labels, features in train_data:
            for label in labels:
                cnt[label]+=1
        print("before: common 5:{}, least common 5:{}".format(cnt.most_common(5), list(cnt.most_common())[-5:-1]))
        downsample = True
        #set dynamic
        IQ1 = len(cnt) // 4
        IQ4 = (3 * len(cnt)) // 4
        threshold_frequency = cnt.most_common()[IQ1][1] 
        target_frequency = cnt.most_common()[IQ4][1]
        #done
        train_data2 = copy.deepcopy(train_data)
        train_data2 = smote_outer(train_data2, downsample, threshold_frequency, target_frequency)
        cnt = Counter()
        for labels, features in train_data2:
            for label in labels:
                cnt[label]+=1
        print("after: common 5:{}, least common 5:{}".format(cnt.most_common(5), list(cnt.most_common())[-5:-1]))
        show_distribution('Smote (label frequency only)', train_data2, [], start=0, end=-1)
        save_to_libsvm(train_data2,dataset_smote)
        
        #Run LCIF on smote output
        run_lcif('GS','macroF1_2',dataset_smote[1:], dataset_test_lcif)
        test_predictions = parse_file_predictions(predictions_lcif_macro)
        print("Loading predictions data {} instances".format(len(test_predictions)))
        train_data = parse_file_libsvm(dataset_smote)
        
        #Compute macro results and plot distribution after single threshold
        print(datasetname + ' Running single threshold SMOTE')
        test_predictionsSTInput = {}
        for i in range(0,len(test_predictions)):
            test_predictionsSTInput[i] = test_predictions[i]
        #run single threshold
        test_predictions_st_dict = apply_single_threshold(label_cardinality, test_predictionsSTInput)
        #post-processing
        test_predictions_st = [[] for i in range(0,len(test_predictions_st_dict))]
        for testuid, predictionsdict in test_predictions_st_dict.items():
            test_predictions_st[testuid] = [(label, one_value) for label, one_value in predictionsdict.items()]
        #evaluate and plot
        macro1 = evaluate(test_data, test_predictions_st)
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(macro1)
        #macro2 = evaluateCPP(test_data, test_predictions_st)
        show_distribution(datasetname + ' Single threshold SMOTE', test_data, test_predictions_st,start=start_labels,end=end_labels)
        
        #Compute macro results and plot distribution after ranked based threshold
        print(datasetname + ' Running rank-based threshold SMOTE')
        test_predictionsSTInput = {}
        for i in range(0,len(test_predictions)):
            test_predictionsSTInput[i] = test_predictions[i]
        countNoTrainingSamplesWithLabel = Counter()
        for labels,features in train_data:
            for label in labels:
                countNoTrainingSamplesWithLabel[label]+=1
        #run
        test_predictions_mt_dict = apply_rank_based_threshold(len(train_data),len(test_data),countNoTrainingSamplesWithLabel,
                                                              test_predictionsSTInput,smooth=2,verbose=True)
        
        test_predictions_mt = [[] for i in range(0,len(test_predictions_mt_dict))]
        for testuid, predictionsdict in test_predictions_mt_dict.items():
            test_predictions_mt[testuid] = [(label, one_value) for label, one_value in predictionsdict.items()]
        #evaluate and plot
        macro1 = evaluate(test_data, test_predictions_mt)
        #macro2 = evaluateCPP(test_data, test_predictions_mt)
        pp.pprint(macro1)
        show_distribution(datasetname + ' Rank threshold SMOTE', test_data, test_predictions_mt,start=start_labels,end=end_labels)
        

def run_all(): 
    run('medical','../data/medical-train.libsvm', '../data/medical-test.libsvm', False, start_labels=0, end_labels=30)
    run('corel5k','../data/corel5k-train.libsvm', '../data/corel5k-test.libsvm', False, start_labels=0, end_labels=30)
    run('bibtex', '../data/bibtex-train.libsvm', '../data/bibtex-test.libsvm', False, start_labels=0, end_labels=30)
    run('delicious', '../data/delicious-train.libsvm', '../data/delicious-test.libsvm', False, start_labels=0, end_labels=30)
    run('IMDB-F', '../data/IMDB-F-train.libsvm', '../data/IMDB-F-test.libsvm', False, start_labels=0, end_labels=30)
    
run_all()