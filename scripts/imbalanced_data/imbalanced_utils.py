from collections import Counter, defaultdict
import matplotlib
from matplotlib import pyplot as plt
import numpy as np
#computing macro F1 with sklearn
import sklearn
import scipy
from sklearn.preprocessing import MultiLabelBinarizer
import math
from math import log2,sqrt,ceil
from random import randint,random
import copy
import subprocess
import os
import os.path

def run_lcif(mode, metric, fname_train, fname_test):
    LCIF_PATH = '/Users/lfereman/git2/lcif_knn_journal'
    if not(os.path.exists(LCIF_PATH)):
        print("ERROR: LCIF_PATH referencing lcif_knn_journal executable not found :{}".format(LCIF_PATH))
    cmd = ["./lcif_knn_journal", str(mode), metric, fname_train, fname_test]
    p = subprocess.Popen(cmd, cwd=LCIF_PATH)
    p.wait()
    
#parse libsvm file and get labels and features
#e.g. 499, 500, 501, 502 19:1 259:1 149:1 329:1 379:1 93:1 461:1 143:1
def parse_line_libsvm(line):
    tokens = line.strip().split(' ')
    labels = [int(s.replace(',','')) for s in tokens if not(':' in s)]
    features = [(int(s.split(':')[0]), float(s.split(':')[1])) for s in tokens if ':' in s]
    return labels, features

def test_parse_line_libsvm():
    file = 'corel5k-test.libsvm'
    file_handle = open(file, 'r')
    print(file_handle.readline()) #first line contains total number of instances,features and label
    line = file_handle.readline()
    v = parse_line_libsvm(line)
    print(line)
    print("parses to {}".format(v))

def parse_file_libsvm(file):
    file_handle = open(file, 'r')
    print(file_handle.readline()) #first line contains total number of instances,features and label
    lst = []
    line = file_handle.readline()
    while(line):
        t = parse_line_libsvm(line)
        lst.append(t)
        line = file_handle.readline()
    return lst

#parse predictions file
#e.g. 501:0.171038 511:0.13691 631:0.120305 514:0.118262 505:0.117397 503:0.107218 504:0.0905922 502:0.0786913 556:0.0748005 557:0.0746934 526:0.070821 697:0.0638165 569:0.0634997 650:0.0615994 581:0.0594155 635:0.0525358 714:0.0515961 577:0.0487256 532:0.0440004 776:0.0409906 777:0.0409681 573:0.0409297 500:0.0352115 632:0.0330876 544:0.0329069 584:0.0303011 508:0.0296001 509:0.0294573 563:0.0293744 600:0.0293051 820:0.0292863 499:0.0282977 715:0.0282582 571:0.026681 531:0.0264836 643:0.0264762 560:0.0264258 510:0.0256718 764:0.0248207 619:0.0246154 791:0.0238191 517:0.0237661 594:0.0233216 595:0.0233216 552:0.0219998 597:0.0218139 553:0.0209395 686:0.0194568 852:0.0194182 649:0.0186048 546:0.0184629 640:0.0178207 801:0.0172273 613:0.0171928 522:0.0170299 765:0.0170256 545:0.0164948 792:0.0163537 519:0.0154115 709:0.0152838 575:0.014347 847:0.014098 780:0.0139872 705:0.0129938 634:0.0124433 838:0.0124433 555:0.0120428 541:0.0118109 513:0.011701 800:0.0111578 592:0.0108837 633:0.0106796 620:0.0105535 803:0.0102823 752:0.0102029 623:0.0101659 690:0.0100959 703:0.0100613 549:0.0100008 550:0.00997243 655:0.00997243 831:0.00988356 524:0.0098447 761:0.00980733 542:0.00968447 724:0.00941926 604:0.0093887 520:0.00924569 849:0.00903797 637:0.0089415 580:0.00885278 624:0.00885278 695:0.00877582 568:0.00809321 551:0.00797802 687:0.00788852 535:0.00780922 576:0.0077839 834:0.00777558 609:0.00773373 516:0.00772506 787:0.00769503 771:0.00766582 774:0.00762163 843:0.00760616 677:0.00759041 850:0.00741267 641:0.00741267 832:0.00741267 708:0.00666935 664:0.00665784 562:0.00664828 574:0.00657844 674:0.00654425 788:0.00647269 561:0.00643963 626:0.00635076 793:0.00629302 682:0.00552612 769:0.00543768 786:0.00543271 605:0.00541029 732:0.00535025 596:0.00535025 789:0.00524386 506:0.00522414 689:0.00522414 770:0.00515074 807:0.00515074 611:0.00513527 618:0.00513527 639:0.00513527 512:0.00511952 658:0.00503065 669:0.00503065 570:0.00503065 837:0.00503065 629:0.00502202 678:0.00494178 586:0.00494178 572:0.00494178 533:0.00477668 529:0.00392869 548:0.00387987 564:0.00387987 735:0.00387987 667:0.00374517 783:0.00362864 857:0.00315143 547:0.00315143 565:0.00315143 719:0.00294737 590:0.00294737 614:0.00279049 630:0.00279049 646:0.00279049 699:0.00279049 768:0.00271948 651:0.00266438 653:0.00266438 663:0.00266438 858:0.00266438 507:0.00266438 539:0.00266438 671:0.00266438 810:0.00266438 536:0.00266438 543:0.00266438 672:0.00266438 864:0.0026447 700:0.00256947 583:0.00255976 558:0.00255976 802:0.00255976 662:0.00255976 528:0.00255976 822:0.00255976 863:0.00255976 745:0.00254633 666:0.00247089 621:0.00247089 773:0.00247089 805:0.00247089 589:0.00247089 766:0.00247089 616:0.00247089 591:0.00247089 746:0.00247089 622:0.00247089 534:0.00247089 521:0.00247089 523:0.00247089 722:0.00247089 848:0.00247089 704:0.00247089 656:0.00247089 691:0.00247089 675:0.00247089 734:0.00247089 743:9.85774e-05 750:5.68018e-05
def parse_line_predictions(line):
    tokens = line.strip().split(' ')
    scores = [(int(s.split(':')[0]), float(s.split(':')[1])) for s in tokens]
    return scores

def test_parse_line_predictions():
    file = '../temp/predictions.txt'
    file_handle = open(file, 'r')
    line = file_handle.readline()
    v = parse_line_predictions(line)
    print(line)
    print("parses to {}".format(v))

def parse_file_predictions(file):
    file_handle = open(file, 'r')
    lst = []
    line = file_handle.readline()
    while(line):
        if line.strip() != '':
            t = parse_line_predictions(line)
            lst.append(t)
        line = file_handle.readline()
    return lst

def test_parse_file_predictions():
    file = '../temp/predictions.txt'
    parse_file_predictions(file)

#test_parse_file_predictions()

def show_distribution(labelplot, test_labels,labels_predicted, colors=['g','y'],start=0,end=30):
    #compute frequencies
    cntAll = Counter()
    cntGroundTruth = Counter()
    for labels,features in test_labels:
        for label in labels:
            cntGroundTruth[label] += 1
            cntAll[label] +=1
    cntPredicted= Counter()
    for labelscores in labels_predicted:
        for predicted_label, score in labelscores[0:5]:
            cntPredicted[predicted_label] += 1
            cntAll[predicted_label] +=1
    #sort labels on frequency
    #data for plotting top-k labels
    plot_labels = None
    if end == -1:
        plot_labels = cntAll.most_common()[start:]
    else:
        plot_labels = cntAll.most_common()[start:end]
    labels_counts_ground_truth = [cntGroundTruth.get(label,0) for label, labelCount in plot_labels]
    labels_counts_predicted = [cntPredicted.get(label,0) for label, labelCount in plot_labels]
    x_axis_labels = [label for label, labelCount in plot_labels]
    #plot
    w = 0.3
    multiple_bars = plt.figure(figsize=(7, 4), dpi=300) #or 80?
    #plt.title(labelplot + '[' + str(start) + ',' + str(end) + ']' + ': Frequency of labels versus predicted labels')
    x_axis = np.array(list(range(0,len(labels_counts_ground_truth))))
    ax = plt.subplot(111)
    ax.bar(x_axis, labels_counts_ground_truth, width=w,color=colors[0],align='center',label='Labels')
    ax.bar(x_axis + w, labels_counts_predicted, width=w,color=colors[1],align='center', label='Predictions')
    
     # Ensure that the axis ticks only show up on the bottom and left of the plot.
    # Ticks on the right and top of the plot are generally unnecessary chartjunk.
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["bottom"].set_visible(True)
    ax.spines["bottom"].set_color('#aaaaaa')
    ax.spines["bottom"].set_linestyle(':')
    ax.spines["left"].set_visible(True)
    ax.spines["left"].set_color('#aaaaaa')
    ax.spines["left"].set_linestyle(':')
    myfontsize = 14
    ax.set_xlabel('Label',fontsize=myfontsize+1)
    ax.set_ylabel('Frequency',fontsize=myfontsize+1)
    #ax.set_xticks(x_axis + 0.1)
    #ax.set_xticklabels(x_axis_labels)
    
    ax.legend(prop={"size":15})
    #plt.show()
    plt.savefig('../images/' + labelplot + '.pdf')

def to_matrix(lst):
    max_lst = max([len(sublist) for sublist in lst])
    #make rectangular
    for i in range(0,len(lst)):
        if len(lst[i]) < max_lst:
            for j in range(0,max_lst - len(lst[i])):
                lst[i] = lst[i] + [0]
        lst[i] = np.asarray(lst[i])
    lst = np.asarray(lst)
    return lst

def macroF1(test_labels,labels_predicted):
    y_true = to_matrix([t[0] for t in test_labels]) #test_data contains labels and features
    y_pred = to_matrix([[label for label, score in labels] for labels in labels_predicted]) #only take labels
    m = MultiLabelBinarizer().fit(y_true)
    macroF1 = sklearn.metrics.f1_score(m.transform(y_true), m.transform(y_pred), average='macro')
    print('macroF1:{}'.format(macroF1))
    return macroF1


#compute macro F1 self
def evaluate(test_data, test_predictions):
    #internal format is dict...
    test_labels = {}
    for i in range(0,len(test_data)):
        test_labels[i] = test_data[i][0] #set of labels
    labels_predicted = {}
    for i in range(0,len(test_predictions)):
        labels_predicted[i] = [label for label,score in test_predictions[i]]
    all_labels = set()
    for uid, labels in test_labels.items():
        for label in labels:
            all_labels.add(label)
    for uid, labels in labels_predicted.items():
        for label in labels:
            all_labels.add(label)
    #done conversion
    example_f1 = 0
    example_acc = 0
    true_pos_per_label = Counter()
    false_pos_per_label = Counter()
    false_neg_per_label = Counter()
    for test_uid, test_uid_labels in test_labels.items():
        test_uid_predicted = labels_predicted.get(test_uid,-666)
        if test_uid_predicted == -666: #no predictions
            continue
        intersection_size = sum([1 for label in test_uid_labels if label in test_uid_predicted])
        example_f1 += 2 * intersection_size / float(len(test_uid_labels) + len(test_uid_predicted))
        example_acc += intersection_size / float(len(test_uid_labels) + len(test_uid_predicted) - intersection_size)
        for label in test_uid_labels:
            if label in test_uid_predicted:
                true_pos_per_label[label] +=1
            else:
                false_neg_per_label[label] +=1
        for label in test_uid_predicted:
            if label not in test_uid_labels:
                false_pos_per_label[label] +=1
    example_f1 = example_f1 / float(len(test_labels))
    example_acc = example_acc / float(len(test_labels))
    precision_macro = 0.0
    recall_macro = 0.0
    f1_macro = 0.0
    f1_macro_regular = 0.0
    f1_macro_results = {}
    for label in all_labels:
        tp  = true_pos_per_label[label]
        fp = false_pos_per_label[label]
        fn = false_neg_per_label[label]
        if tp == 0 and fp == 0 and fn == 0: #1.0 if never true positive nor false positive, than does not occur
            precision_per_label = recall_per_label = f1_per_label = 1.0
        else:
            #if tp + fp = 0, than all cases are predicted negative and result is 0.0
            precision_per_label = tp / float(tp + fp) if tp + fp !=0 else 0.0
            recall_per_label = tp / float(tp + fn) if tp + fn !=0 else 0.0
            #if tp + fn = 0, than all cases are predicted negative and result is 0.0
            f1_per_label = 0.0
            if (precision_per_label + recall_per_label) != 0:
                f1_per_label = 2 * precision_per_label * recall_per_label / (precision_per_label + recall_per_label)
        precision_macro += precision_per_label
        recall_macro += recall_per_label
        f1_macro += f1_per_label
        f1_macro_regular += 0 if tp == 0 and fp == 0 and fn == 0 else f1_per_label
        f1_macro_results[label] = f1_per_label
    precision_macro = precision_macro / float(len(all_labels))
    recall_macro = recall_macro / float(len(all_labels))
    f1_macro = f1_macro / float(len(all_labels))
    f1_macro_regular = f1_macro_regular / float(len(all_labels))
    evals =  {'example acc': example_acc, 'example F1': example_f1, 'macro precision': precision_macro, 'recall macro': recall_macro, 'f1 macro': f1_macro, 'f1_macro_regular': f1_macro_regular}
    return evals


#compute macro F1 self
#WITH CHANGES FROM C++ code
def evaluateCPP(test_data, test_predictions):
    #internal format is dict...
    test_labels = {}
    for i in range(0,len(test_data)):
        test_labels[i] = test_data[i][0] #set of labels
    labels_predicted = {}
    for i in range(0,len(test_predictions)):
        labels_predicted[i] = [label for label,score in test_predictions[i]]
    all_labels = set()
    for uid, labels in test_labels.items():
        for label in labels:
            all_labels.add(label)
    for uid, labels in labels_predicted.items():
        for label in labels:
            all_labels.add(label)
    #done conversion
    example_f1 = 0
    example_acc = 0
    true_pos_per_label = Counter()
    false_pos_per_label = Counter()
    false_neg_per_label = Counter()
    for test_uid, test_uid_labels in test_labels.items():
        test_uid_predicted = labels_predicted.get(test_uid,-666)
        if test_uid_predicted == -666:
            continue
        intersection_size = sum([1 for label in test_uid_labels if label in test_uid_predicted])
        example_f1 += 2 * intersection_size / float(len(test_uid_labels) + len(test_uid_predicted))
        example_acc += intersection_size / float(len(test_uid_labels) + len(test_uid_predicted) - intersection_size)
        for label in test_uid_labels:
            if label in test_uid_predicted:
                true_pos_per_label[label] +=1
            else:
                false_neg_per_label[label] +=1
        for label in test_uid_predicted:
            if label not in test_uid_labels:
                false_pos_per_label[label] +=1
    example_f1 = example_f1 / float(len(test_labels))
    example_acc = example_acc / float(len(test_labels))
    precision_macro = 0.0
    recall_macro = 0.0
    f1_macro = 0.0
    f1_macro_regular = 0.0
    f1_macro_results = {}
    for label in all_labels:
        tp  = true_pos_per_label[label]
        fp = false_pos_per_label[label]
        fn = false_neg_per_label[label]
        #if label never predicted (tp=fp=0) -> 0.0
        precision_per_label = tp / float(tp + fp) if tp + fp !=0 else 0.0
        #if label not in test set -> tp=0 and fn=0 -> recall=1
        #if label in test set, but never predicted -> fn high, but recall 0.0
        recall_per_label = tp / float(tp + fn) if tp + fn !=0 else 1.0
        f1_per_label = 0.0
        #if label never predicted -> f1 will be 0... not good
        if (precision_per_label + recall_per_label) == 0:
            f1_per_label = 1.0 #arguable...
        else:
            f1_per_label = 2 * precision_per_label * recall_per_label / (precision_per_label + recall_per_label)
        if tp == 0 and fp == 0 and fn == 0: #1.0 if never true positive nor false positive, than does not occur
            precision_per_label = recall_per_label = f1_per_label = 1.0
        precision_macro += precision_per_label
        recall_macro += recall_per_label
        f1_macro += f1_per_label
        f1_macro_regular += 0 if tp == 0 and fp == 0 and fn == 0 else f1_per_label
        f1_macro_results[label] = f1_per_label
    precision_macro = precision_macro / float(len(all_labels))
    recall_macro = recall_macro / float(len(all_labels))
    f1_macro = f1_macro / float(len(all_labels))
    f1_macro_regular = f1_macro_regular / float(len(all_labels))
    evals =  {'example acc': example_acc, 'example F1': example_f1, 'macro precision': precision_macro, 'recall macro': recall_macro, 'f1 macro': f1_macro, 'f1_macro_regular': f1_macro_regular}
    return evals


def apply_single_threshold(label_cardinality, scores, levels=3, fixed_threshold=None, verbose=False):
    #min_labels = min(vals)
    #max_labels = max(vals)
    #if verbose:
    #    print("lcard: {}. min_labels: {}, max_labels:{}".format(label_cardinality, min_labels, max_labels))
    t_min_range_level1 = np.arange(0.0,1.0,0.1)
    ranges = [t_min_range_level1, None, None]
    t_chosen = 0
    min_diff = 10000000
    if not fixed_threshold:
        for level, my_range in enumerate(ranges):
            if level + 1 > levels:
                break
            for t_min in my_range:
                label_cardinality_predictions = 0
                for test_uid, iid_predictions in scores.items():
                    label_cardinality_predictions += max(1,sum([1 if score > t_min else 0 for label,score in iid_predictions]))
                label_cardinality_predictions = label_cardinality_predictions / float(len(scores))
                if abs(label_cardinality_predictions - label_cardinality) < min_diff:
                    t_chosen = t_min
                    min_diff = abs(label_cardinality_predictions - label_cardinality)
            #for level 2
            if verbose:
                print('t_min: {}, lcard:{}, abs error: {}'.format(t_chosen, label_cardinality, min_diff))
            if level == 0:
                ranges[1] = np.arange(t_chosen - 0.05, t_chosen + 0.05 ,0.01)
            if level == 1:  #note: original was from first step_size 0.1, than 0.01, but even smaller is better
                ranges[2] = np.arange(t_chosen - 0.005, t_chosen + 0.005 ,0.001)
    if fixed_threshold:
        t_chosen = fixed_threshold
    labels_predicted = defaultdict(dict)
    for test_uid, iid_predictions in scores.items():
        for label,score in iid_predictions:
            if score > t_chosen:
                labels_predicted[test_uid][label] = 1
        if len(iid_predictions) == 0:
            print("Warning: no predictions for {}".format(test_uid))
            labels_predicted[test_uid] = {}
        #special case: predict first
        if len(iid_predictions) != 0 and len(labels_predicted[test_uid].keys()) == 0:
            top_label = iid_predictions[0][0]
            labels_predicted[test_uid][top_label] = 1
    return labels_predicted


#Rank-based threshold
''' Predict top-k scores for each labels '''
def apply_rank_based_threshold(noTrain, noTest, countNoTrainingSamplesWithLabel, scores, smooth=0, verbose=False):
    #estimate top-k for each label, based on distribution in training set
    labels_rank_threshold = {}
    for label in countNoTrainingSamplesWithLabel.keys():
        noTrainingSamplesWithLabel = countNoTrainingSamplesWithLabel[label]
        labels_rank_threshold[label] = int(math.floor(noTrainingSamplesWithLabel/float(noTrain) * noTest)) + smooth
    if verbose:
        print("rank thresholds: {}".format(labels_rank_threshold))
    #select value to threshold on each label
    label_2_scores = defaultdict(list)
    label_value_threshold = {}
    for test_uid, iid_predictions in scores.items():
        for label,score in iid_predictions:
            label_2_scores[label].append(score)
    for label, label_scores in  label_2_scores.items():
        label_scores = sorted(label_scores, reverse=True)
        rank_treshold = labels_rank_threshold.get(label,0)
        label_value_threshold[label] = label_scores[rank_treshold] if rank_treshold < len(label_scores) else label_scores[-1]
        #if verbose:
        #    print('label:{} rank_threshold:{} label_value_threshold:{}'.format(label,
        #                                                                       rank_treshold,label_value_threshold[label]))
    #apply threshold
    labels_predicted = defaultdict(dict)
    for test_uid, iid_predictions in scores.items():
        for label,score in iid_predictions:
            if score > label_value_threshold[label]:
                labels_predicted[test_uid][label] = 1
        if len(iid_predictions) == 0:
            print("Warning: no predictions for {}".format(test_uid))
            labels_predicted[test_uid] = {}
        #special case: predict first
        if len(iid_predictions) != 0 and len(labels_predicted[test_uid].keys()) == 0:
            top_label = iid_predictions[0][0]
            labels_predicted[test_uid][top_label] = 1
    return labels_predicted



def find_1_nn(instance, instances):
    norm_instance = sqrt(sum([score * score for feature,score in instance[1]]))
    min_simil = -100000
    min_instance = -1
    for cand_instance in instances:
        #if equal to training instance: skip
        if cand_instance[0] == instance[0] and cand_instance[1] == instance[1]:
            continue
        simil = 0
        for feature, score in instance[1]:
            for feature2, score2 in cand_instance[1]:
                if feature==feature2:
                    simil = simil + score*score2
        if simil == 0:
            continue
        norm_1 = sqrt(sum([score2 * score2 for feature2,score2 in cand_instance[1]]))
        simil = simil / (norm_1 * norm_instance)
        if simil > min_simil:
            min_simil = simil
            min_instance = cand_instance
    return min_instance, min_simil

#Strategy two: SMOTE: under and oversampling
def smote_outer(training_data, downsample=False, treshold_frequency=60, target_frequency=30):
    #compute label frequency
    cnt = Counter()
    for labels, features in training_data:
        for label in labels:
            cnt[label]+=1
    label_frequencies = list(cnt.most_common())
    label_frequencies.reverse()
    smote_factor = 5
    #5000 samples, lcard is 3.5, noLabels is 374, so expected value is about 50 occurrences per label
    #if label occurs less than 30 times -> smote_factor=2, label occurs less than 10 times -> smote_factor=3,
    #else smote_factor=5
    n = 0
    for target_label, frequency in label_frequencies:
        print(n, end =' ')
        n+=1
        #print('target label:{} freq:{}'.format(target_label,frequency))
        smote_factor = 5
        if frequency > treshold_frequency:
            if downsample:
                smote_factor = frequency // target_frequency * 4 #times 4 is foefelnumber
                #down_sample
                for labels,features in training_data:
                    if target_label in labels:
                        if randint(0,smote_factor) != 0:
                            labels.remove(target_label)
                continue
        else:
            smote_factor = target_frequency // frequency         #get samples
        label_samples = []
        for labels,features in training_data:
            if target_label in labels:
                label_samples.append((labels,features))
        #print('Samples:{}'.format(len(label_samples)))
        synth_samples = []
        no_synth = smote_factor * frequency
        #print(no_synth)
        for i in range(0,no_synth):
            #take random sample:
            sample = label_samples[randint(0,len(label_samples)-1)]
            #knn to random sample
            if frequency > 1:
                ins, simil = find_1_nn(sample, label_samples)
                ins = -1
                if ins == -1:
                    synth_samples.append(([target_label], copy.deepcopy(sample[1])))
                else:
                    #take ensemble of both -> later... now just copy!
                    #FROM SMOTE paper:
                    #Compute: dif = Sample[nnarray[nn]][attr] − Sample[i][attr] 
                    #Compute: gap = random number between 0 and 1 
                    #Synthetic[newindex][attr] = Sample[i][attr] + gap ∗ dif
                    features = copy.deepcopy(sample[1])
                    for i in range(0,len(features)):
                        feature, score = features[i]
                        for featureNN,scoreNN in ins[1]:
                            if feature == featureNN:
                                diff = scoreNN  - score
                                gap = random()
                                new_score = score + gap * diff
                                features[i][1] = new_score
                            else:
                                if randint(0,1) == 1: #not really in paper
                                    features.append((featureNN,scoreNN))
                    synth_samples.append(([target_label], features))
            else:
                synth_samples.append(([target_label], copy.deepcopy(sample[1])))
        training_data.extend(synth_samples)
    print()
    return training_data


#save file
#e.g.
#4500 499 371
#499, 500, 501, 502 19:1 259:1 149:1 329:1 379:1 93:1 461:1 143:1
#...
def inst_to_libsvm(ins):
    labels = ", ".join([str(label) for label in ins[0]])
    features = " ".join([str(feature) + ":" + str(score) for feature,score in ins[1]])
    return labels + " " + features

def save_to_libsvm(data, fname):
    no_instances = len(data)
    no_labels = 0
    no_features = 0
    labelsset = set()
    featuresset = set()
    for labels,features in data:
        for label in labels:
            labelsset.add(label)
        for feature,score in features:
            featuresset.add(feature)
    no_labels = len(labelsset)
    no_features = len(featuresset)
    fh = open(fname,'w')
    fh.write('{} {} {}\n'.format(str(no_instances), str(no_features), str(no_labels)))
    for ins in data:
        if len(ins[0]) == 0:
            continue
        else:
            fh.write(inst_to_libsvm(ins) + '\n')
    fh.close()
    print('Saved {}'.format(fname))

