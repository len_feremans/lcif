#large, no-limit
#parameters are
./lcif PRUNING ./data/medical-train.libsvm ./data/medical-test.libsvm
./lcif PRUNING ./data/corel5k-train.libsvm ./data/corel5k-test.libsvm
./lcif PRUNING ./data/bibtex-train.libsvm ./data/bibtex-test.libsvm
./lcif PRUNING ./data/delicious-train.libsvm ./data/delicious-test.libsvm
./lcif PRUNING ./data/IMDB-F-train.libsvm ./data/IMDB-F-test.libsvm

#extreme, no-limit
./lcif PRUNING ./data/XMLRepository/Eurlex/eurlex_train.txt  ./data/XMLRepository/Eurlex/eurlex_test.txt
./lcif PRUNING ./data/XMLRepository/Wiki10/wiki10_train.txt  ./data/XMLRepository/Wiki10/wiki10_test.txt
./lcif PRUNING ./data/XMLRepository/RCV1-x/rcv1x_train.txt ./data/XMLRepository/RCV1-x/rcv1x_test.txt
./lcif PRUNING ./data/XMLRepository/AmazonCat/amazonCat_train.txt ./data/XMLRepository/AmazonCat/amazonCat_test.txt
./lcif PRUNING ./data/XMLRepository/WikiLSHTC/wikiLSHTC_train.txt ./data/XMLRepository/WikiLSHTC/wikiLSHTC_test.txt
