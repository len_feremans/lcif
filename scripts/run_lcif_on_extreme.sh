#parameters are for lcif binary grid search are: metric, train, test
#kRow, kRowPower, kFeature, kFeaturePower and lambda are optimised using cross-validation on a sample of 1000
metric=$1
./lcif GS_BIG $metric ./data/XMLRepository/Eurlex/eurlex_train.txt  ./data/XMLRepository/Eurlex/eurlex_test.txt
./lcif GS_BIG $metric ./data/XMLRepository/Wiki10/wiki10_train.txt  ./data/XMLRepository/Wiki10/wiki10_test.txt
./lcif GS_BIG $metric ./data/XMLRepository/RCV1-x/rcv1x_train.txt ./data/XMLRepository/RCV1-x/rcv1x_test.txt
./lcif GS_BIG $metric ./data/XMLRepository/AmazonCat/amazonCat_train.txt ./data/XMLRepository/AmazonCat/amazonCat_test.txt
./lcif GS_BIG $metric ./data/XMLRepository/WikiLSHTC/wikiLSHTC_train.txt ./data/XMLRepository/WikiLSHTC/wikiLSHTC_test.txt