./run_lcif_on_large.sh exampleBasedAccuracy >> ./results/large_acc.txt
#./run_lcif_on_large.sh precision >> ./results/large_prec.txt
#./run_lcif_on_large.sh recall >> ./results/large_recall.txt
./run_lcif_on_large.sh microF1 >> ./results/large_microF1.txt
./run_lcif_on_large.sh macroF1 >> ./results/large_macroF1.txt
./run_lcif_on_large.sh hammingLoss  >> ./results/large_hammingloss.txt
