#parameters are for lcif binary grid search are: metric, train, test
#kRow, kRowPower, kFeature, kFeaturePower and lambda are optimised using k-fold cross-validation
metric=$1
./lcif GS $metric ./data/medical-train.libsvm ./data/medical-test.libsvm
./lcif GS $metric ./data/corel5k-train.libsvm ./data/corel5k-test.libsvm
./lcif GS $metric ./data/bibtex-train.libsvm ./data/bibtex-test.libsvm
./lcif GS $metric ./data/delicious-train.libsvm ./data/delicious-test.libsvm
./lcif GS $metric ./data/IMDB-F-train.libsvm ./data/IMDB-F-test.libsvm