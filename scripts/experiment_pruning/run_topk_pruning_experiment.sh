#large, limit
#parameters are: startTopM, endTopM, topK, noTestInstances, train, test
python3 topk_pruning_experiment.py 0 25 100 1000 ../data/medical-train.libsvm ../data/medical-test.libsvm
python3 topk_pruning_experiment.py 0 25 100 1000 ../data/corel5k-train.libsvm ../data/corel5k-test.libsvm
python3 topk_pruning_experiment.py 0 25 100 1000 ../data/delicious-train.libsvm ../data/delicious-test.libsvm
python3 topk_pruning_experiment.py 0 25 100 1000 ../data/bibtex-train.libsvm ../data/bibtex-test.libsvm
python3 topk_pruning_experiment.py 0 25 100 1000 ../data/IMDB-F-train.libsvm ../data/IMDB-F-test.libsvm

#extreme, limit
python3 topk_pruning_experiment.py 0 25 100 1000 ../data/XMLRepository/Eurlex/eurlex_train.txt  ../data/XMLRepository/Eurlex/eurlex_test.txt
python3 topk_pruning_experiment.py 0 25 100 1000 ../data/XMLRepository/Wiki10/wiki10_train.txt  ../data/XMLRepository/Wiki10/wiki10_test.txt
python3 topk_pruning_experiment.py 0 25 100 1000 ../data/XMLRepository/AmazonCat/amazonCat_train.txt ../data/XMLRepository/AmazonCat/amazonCat_test.txt
python3 topk_pruning_experiment.py 0 25 100 1000 ../data/XMLRepository/RCV1-x/rcv1x_train.txt ../data/XMLRepository/RCV1-x/rcv1x_test.txt
python3 topk_pruning_experiment.py 0 25 100 1000 ../data/XMLRepository/WikiLSHTC/wikiLSHTC_train.txt ../data/XMLRepository/WikiLSHTC/wikiLSHTC_test.txt