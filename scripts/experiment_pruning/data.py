from collections import defaultdict, Counter
import re
import numpy as np
from math import sqrt
import matplotlib.pyplot as plt
import math
import time

class Data(object):
    def __init__(self, train_file, test_file, skipFirstLine=False, loadBinary=True):
        self.train_labels = defaultdict(dict)
        self.train_labels_inv = defaultdict(dict)
        self.train_features = defaultdict(dict)
        self.train_features_inv = defaultdict(dict)
        self.test_labels = defaultdict(dict)
        self.test_labels_inv = defaultdict(dict)
        self.test_features = defaultdict(dict)
        self.test_features_inv = defaultdict(dict)
        self.all_features = set()
        self.all_labels = Counter()
        if train_file != None:
            self.load_data(train_file, test_file, skipFirstLine, loadBinary)
               
    def load_data(self, train_file,test_file, skipFirstLine, loadBinary):
        f = open(train_file)
        lines_train = f.readlines()
        f.close()
        f2 = open(test_file)
        lines_test = f2.readlines()
        f2.close()
        if skipFirstLine:
            lines_train = lines_train[1:]
            lines_test = lines_test[1:]
        for lines, dct_labels, dct_features, dct_labels_inv, dct_features_inv in \
                [(lines_train, self.train_labels, self.train_features, self.train_labels_inv, self.train_features_inv), 
                 (lines_test, self.test_labels, self.test_features, self.test_labels_inv, self.test_features_inv)]:
            for i, line in enumerate(lines):
                labels,features = self._parse_line(line, loadBinary)
                self._add_instance(i, labels, features, loadBinary, dct_labels, dct_features, dct_labels_inv, dct_features_inv)
        self.all_labels = list([label for label, count in self.all_labels.most_common()])
    
    def _add_instance(self, i, labels_instance, features_instance, loadBinary, 
                      dct_labels, dct_features, dct_labels_inv, dct_features_inv):
        for label in labels_instance:
            dct_labels[i][label] = 1
            dct_labels_inv[label][i] = 1
            self.all_labels[label]+=1 #both training and test
        if loadBinary:
            for feature in features_instance:
                dct_features[i][feature] = 1
                dct_features_inv[feature][i] = 1
                self.all_features.add(feature)
        else:
            for (feature, value) in features_instance:
                dct_features[i][feature] = value
                dct_features_inv[feature][i] = 1
                self.all_features.add(feature)
                        
    def _parse_line(self, line, loadBinary):
        #labels_or_features = re.split('\,\s*|\s*',line)
        labels_or_features = re.split(',\s*|\s+',line)
        labels = [int(token) for token in labels_or_features if token.strip() != ''  and ':' not in token]
        if loadBinary:
            features = [int(token.split(':')[0]) for token in labels_or_features if token.strip() != '' and ':' in token]
        else:
            features = [(int(token.split(':')[0]), float(token.split(':')[1])) for token in labels_or_features if token.strip() != '' and ':' in token]
        return labels, features
 
    def print_stats(self):
        print("#train: {}, #test:{}".format(len(self.train_features), len(self.test_features)))
        for q in range(1,5,1):
            s = len(self.all_labels)//4
            labels_range = self.all_labels[(q-1) * s: q *s]
            #print percentage:
            print('{} labels in quadrant {};average percentage in training:{:.3f}/{}, average percentage in test:{:.3f}/{}'.format(
                len(labels_range),
                q, 
                np.mean([len(self.train_labels_inv.get(label,[])) for label in labels_range]), len(self.train_labels),
                np.mean([len(self.test_labels_inv.get(label,[]))  for label in labels_range]), len(self.test_labels)
                ))
        print('#labels only in training: {}/{}; #labels only in test: {}/{}; #labels with  stricly less than 3 examples in training: {}/{}'.format(
                len([label for label in self.train_labels_inv if len(self.test_labels_inv.get(label,[])) == 0]), len(self.train_labels_inv),
                len([label for label in self.test_labels_inv if len(self.train_labels_inv.get(label,[])) == 0]), len(self.test_labels_inv),
                len([label for label in self.all_labels if len(self.train_labels_inv.get(label,[])) < 3]), len(self.all_labels)
            ))
        for dct_label, dct in {'train labels' : self.train_labels, 'train features': self.train_features,  'train labels inv': self.train_labels_inv, 
                    'train features inv': self.train_features_inv, 'test labels': self.test_labels, 'test features': self.test_features, 
                    'test labels inv': self.test_labels_inv, 'test features inv': self.test_features_inv}.items():
            vals = [len(value) for key,value in dct.items()]
            print("average {}: {}".format(dct_label,  np.mean(vals)))
            print("median {}: {}".format(dct_label, sorted(vals)[len(vals)//2]))

    #temporary for now
    def prune_dataset_from_frequent(self):
        top_20p = self.all_labels[0:len(self.all_labels)/5]
        #remove labels from train and test
        for frequent_label in top_20p:
             for  dct_labels, dct_labels_inv in [(self.train_labels, self.train_labels_inv), 
                                                (self.test_labels, self.test_labels_inv)]:
                 for feature in dct_labels.keys():
                     if frequent_label in dct_labels[feature]:
                         del dct_labels[feature][frequent_label]
                 if frequent_label in dct_labels_inv:
                     del dct_labels_inv[frequent_label]
        #remove examples from train and test, that are now empty
        for dct_labels, dct_features, dct_labels_inv, dct_features_inv in \
                [(self.train_labels, self.train_features, self.train_labels_inv, self.train_features_inv), 
                 (self.test_labels, self.test_features, self.test_labels_inv, self.test_features_inv)]:
            to_remove = set()
            for test_uid in dct_labels:
                if len(dct_labels[test_uid]) == 0:
                    to_remove.add(test_uid)
            print("pruning removing: {}".format(len(to_remove)))
            for test_uid in to_remove:
                del dct_labels[test_uid]
                del dct_features[test_uid]
            for feature in dct_features_inv.keys():
                 for test_uid in to_remove:
                     if test_uid in dct_features_inv[feature]:
                         del dct_features_inv[feature][test_uid]
    
    #normalize both features and labels
    def normalize_row(self):
        for train_uid, features in self.train_features.items():
            norm = sqrt(sum([value * value for (feature,value) in features.items()]))
            for feature in features:
                self.train_features[train_uid][feature] = self.train_features[train_uid][feature] / norm
        #NEEDED? ANYWHERE?
        #for train_uid, labels in self.train_labels.items():
        #    norm = sqrt(len(labels))
        #    for label in labels:
        #        self.train_labels[train_uid][label] = self.train_labels[train_uid][label] / norm
        for test_uid, features in self.test_features.items():
            norm = sqrt(sum([value * value for (feature,value) in features.items()]))
            for feature in features:
                self.test_features[test_uid][feature] = self.test_features[test_uid][feature] / norm
                
    def normalize_columns(self):
        for feature in self.all_features:
            norm = sqrt(len(self.train_features_inv[feature]) + 1)
            for train_uid in self.train_features_inv[feature]:
                self.train_features[train_uid][feature] =  self.train_features[train_uid][feature] / norm
            for test_uid in self.test_features_inv.get(feature,[]):
                self.test_features[test_uid][feature] =  self.test_features[test_uid][feature] / norm
    
    ''' given equal weight to each label, e.g. number of examples for each label are the same, in training
        for frequent lables: undersample, for infrequent sample: oversample with replacement
        
        resampling works, but no better macro results, can be removed
     '''        
    def resample_with_replacement_uniform(self):
        new_train_uids = []
        for label in self.all_labels[len(self.all_labels)/2:-1]:
            number_of_examples_wanted = len(self.train_features)/len(self.all_labels) * 2
            examples_with_labels = list(self.train_labels_inv[label].keys())
            if len(examples_with_labels) == 0:
                continue
            for i in range(0, number_of_examples_wanted):
                idx = random.randint(0,len(examples_with_labels)-1)
                new_train_uids.append(examples_with_labels[idx])        
        train_labels_new = defaultdict(dict)
        train_labels_inv_new = defaultdict(dict)
        train_features_new = defaultdict(dict)
        train_features_inv_new = defaultdict(dict)
        for i, new_train_uid in enumerate(new_train_uids):
            labels = self.train_labels[new_train_uid]
            features = self.train_features[new_train_uid]
            for label in labels:
                train_labels_new[i][label] = 1
                train_labels_inv_new[label][i] = 1
            for feature in features:
                train_features_new[i][feature] = 1
                train_features_inv_new[feature][i] = 1
        self.train_labels = train_labels_new
        self.train_labels_inv = train_labels_inv_new
        self.train_features = train_features_new
        self.train_features_inv = train_features_inv_new
        