from collections import defaultdict, Counter
import re
import numpy as np
from math import sqrt
import math, random
import time, sys
import heapq
from collections import deque
from copy import copy
from data import Data

'''
Implementation of different algorithms for top-k query retrieval:
- normaal TAAT
- in comment approximate TAAT
- DAAT with FAGIN Threshold pruning
- DAAT with WAND/BRODER pruning
- TAAT top-m + DAAT pruning based on partition by Feremans

Used to evaluate strength of different pruning mechanisms,
not for benchmarking runtime performance

Author: Len Feremans
Date: Last updated 7-02-2018
'''
class TopKPruningTest(object):
    
    def __init__(self, data):
        self.data = data
        self.info = defaultdict(dict)
        self.sorted_train_features_inv = defaultdict(dict)
        self.sorted_train_features_inv_length = defaultdict(dict)
        self.sorted_train_features_inv_docid = defaultdict(dict)
        self.sorted_train_features_inv_maxweight = {}
        self.sorted_features = []
        self.debug = False
        self.super_debug = False
        
    def _print(self, str):
        if self.debug:
            print(str)
        
    def compute_similarity_TAAT_no_pruning(self, test_uid, top_k):
        self._print('compute_similarity_TAAT_no_pruning')
        features = self.data.test_features[test_uid]
        simils_instance = defaultdict(lambda: 0)
        a_start_time = start_time = time.time()
        pairs = 0
        self._print('>sim-start fase 1')
        for feature in self.data.test_features[test_uid]:
            for train_uid in self.data.train_features_inv[feature]:
                simils_instance[train_uid]+= features[feature] *  self.data.train_features[train_uid][feature]
                pairs+=1
        self._print('<sim-end fase 1: elapsed {}'.format(time.time() - start_time))
        self._print('>sim-start fase 2')
        start_time = time.time()
        simils =  [(train_uid, simil) for train_uid, simil in simils_instance.items()] #note: could do partial sort here
        simils = sorted(simils, key=lambda tuple: tuple[1], reverse=True)
        simils = simils[0:min(top_k,len(simils))]
        self._print('<sim-end fase 2: elapsed {}'.format(time.time() - start_time))
        self.info['instances'] = len(simils_instance)
        self.info['pairs'] = pairs
        self.info['elapsed'] = time.time() - a_start_time
        self._print('info: training instances: {}, pairs:{}'.format(len(simils_instance), pairs))
        return simils
    
    '''
    Sort each each dimension on weight: not how it is described in APT.
    Sort each dimension on density
    '''
    def makeIndexes(self):
        start_time = time.time()
        self._print('>makeIndexes fase 1')
        '''Sort each each dimension on feature-values: not how it is described in APT. TODO APT referentie'''
        for (feature, train_instances) in self.data.train_features_inv.items():
            lst = []
            for train_instance in train_instances:
                train_value = self.data.train_features[train_instance][feature]
                max_weight = max(self.data.train_features[train_instance])
                sum_x =  sum(self.data.train_features[train_instance])
                lst.append((train_instance, train_value, sum_x, max_weight))
            self.sorted_train_features_inv[feature] = sorted(lst, key=lambda pair: pair[1], reverse=True)
            #todo: investigate other strategies, e.g. max_weight sorting or sum sorting
        self.sorted_features = {feature: len(train_instances) for (feature, train_instances) in self.data.train_features_inv.items()}
        '''Sort each each dimension on size'''
        for (feature, train_instances) in self.data.train_features_inv.items():
            lst = []
            for train_instance in train_instances:
                train_value = self.data.train_features[train_instance][feature]
                feature_value = self.data.train_features[train_instance][feature]
                feature_length = len(self.data.train_features[train_instance])
                lst.append((train_instance,feature_value,feature_length))
            self.sorted_train_features_inv_length[feature] = sorted(lst, key=lambda pair: pair[2])
        ''' sort features on density '''
        self.sorted_features = {feature: len(train_instances) for (feature, train_instances) in self.data.train_features_inv.items()}
        #print('features sorted: {}'.format(
        #    sorted([(feature, len(train_instances)) for feature, train_instances in self.data.train_features_inv.items()], key=lambda pair: pair[1], reverse=True)))
        self._print('<makeIndexes fase 1:  elapsed {}'.format(time.time() - start_time))
        #MakeIndex2
        self._print('>makeIndexes fase 2')
        start_time = time.time()
        '''Sort each each dimension on doc id '''
        for (feature, train_instances) in self.data.train_features_inv.items():
            lst = []
            for train_instance in train_instances:
                train_value = self.data.train_features[train_instance][feature]
                lst.append((train_instance,train_value))
            self.sorted_train_features_inv_docid[feature] = sorted(lst, key=lambda pair: pair[0])
            self.sorted_train_features_inv_maxweight[feature] = max(lst, key=lambda pair: pair[1])[1]
        self._print('<makeIndexes fase 2: elapsed {}'.format(time.time() - start_time))
        
    def makeIndexesPartition(self, topM):
        start_time = time.time()
        self._print('>makeIndexes fase 3')
        ''' For TAAT + DAAT make partition, and TAAT and DAAT index '''
        I_taat = set()
        for (feature, train_instances) in self.sorted_train_features_inv.items():
            for i in range(0,len(train_instances)):
                if i>=topM:
                    break
                (train_instance,feature_value,sum_x, max_weight) = train_instances[i]
                I_taat.add(train_instance)
        self.train_features_inv_taat = defaultdict(dict)
        for train_instance in I_taat:
            for feature, feature_value in self.data.train_features[train_instance].items():
                self.train_features_inv_taat[feature][train_instance] = feature_value
        #sort TAAT index on value
        for feature, train_instances in self.train_features_inv_taat.items():
            train_instances_sorted = [(train_uid, feature_value) for train_uid, feature_value in train_instances.items()]
            train_instances_sorted.sort(key=lambda pair: pair[1], reverse=True)
            self.train_features_inv_taat[feature] = train_instances_sorted
        I_daat = set()
        for train_uid in self.data.train_features.keys():
            if not train_uid in I_taat:
                I_daat.add(train_uid)
        self.train_features_inv_daat = defaultdict(dict)
        self.train_features_inv_daat_max = {}
        for train_instance in I_daat:
            for feature, feature_value in self.data.train_features[train_instance].items():
                self.train_features_inv_daat[feature][train_instance] = feature_value
        #sort DAAT index on doc id
        for feature, train_instances in self.train_features_inv_daat.items():
            train_instances_sorted = [(train_uid, feature_value) for train_uid, feature_value in train_instances.items()]
            train_instances_sorted.sort(key=lambda pair: pair[0])
            self.train_features_inv_daat_max[feature] = max(train_instances_sorted, key=lambda pair: pair[1])[1]
            self.train_features_inv_daat[feature] = train_instances_sorted
        self._print('<makeIndexes fase 3:  elapsed {}'.format(time.time() - start_time))
        sys.stdout.flush()
        
    '''
    Order each dimension on descending weight. Than only consider first maxK instances in each dimension.
    Obviously approximated.
    Also: does only return top-k of instances computed using partial similarity
    '''
    '''
    def compute_similarity_TAAT_sorted_approximate_top_m(self, test_uid, top_k, maxK = 400):
        features = self.data.test_features[test_uid]
        simils_instance = defaultdict(lambda: 0)
        start_time = time.time()
        print('sim-start fase 1')
        features_sorted = [(feature, value) for (feature,value) in features.items()]
        features_sorted = sorted(features_sorted, key=lambda pair: self.sorted_features[pair[0]]) #sort features on INCREASING density, not trivial!
        pairs = 0
        for (feature,value) in features_sorted:
            cnt = 0
            for (train_uid, feature_value, sum_t, max_t) in self.sorted_train_features_inv[feature]:
                simils_instance[train_uid]+= value *  feature_value
                pairs +=1
                if cnt > maxK:
                    break;
                cnt+=1
        print('training instances: {}, pairs: {}'.format(len(simils_instance), pairs))
        print('sim-end fase 1: elapsed {}'.format(time.time() - start_time))
        print('sim-start fase 2')
        start_time = time.time()
        simils =  [(train_uid, simil) for train_uid, simil in simils_instance.items()] #note: could do partial sort here
        simils = sorted(simils, key=lambda tuple: tuple[1], reverse=True)
        #simils = simils[0:min(top_k,len(simils))]
        print('sim-end fase 2: elapsed {}'.format(time.time() - start_time))
        return simils
 
    #Used by compute_similarity_TAAT_sorted_approximate_top_m_all_weights
    def _merge_n_sort_lists_get_top_k(self, lists, top_k, key=lambda tuple: tuple[1]):
        result = []
        offsets = [0 for lst in lists]#linear time
        for i in range(0, top_k):
            top = [key(lists[i][offset]) if offset != -1 else -99999 for i, offset in enumerate(offsets)]
            value = max(top)
            if value == -99999: #all lists are empty
                return result
            idx_max =  top.index(value)
            result.append((idx_max, lists[idx_max][offsets[idx_max]]))
            if offsets[idx_max] + 1 < len(lists[idx_max]):
                offsets[idx_max] += 1
            else:
                offsets[idx_max] = -1 #list n is empty
        return result
    '''
    '''
    Compute similarity of top-'maxpairs' weights.
    Also: does only return top-k of instances computed using partial similarity
    '''
    '''
    def compute_similarity_TAAT_sorted_approximate_top_m_all_weights(self, test_uid, top_k,  maxPairs = 2412):
        features = self.data.test_features[test_uid]
        simils_instance = defaultdict(lambda: 0)
        start_time = time.time()
        #generate top-k candidates: take top-2000 similarity pairs of each dimension of test instance
        #how?
        #merge join, takes linear time
        print('sim-start fase 1')
        features_sorted = [(feature, value) for (feature,value) in features.items()]
        features_sorted = sorted(features_sorted, key=lambda pair: self.sorted_features[pair[0]]) #sort features on INCREASING density, not trivial!
        #e.g. each lists contrains tuples of form (train_uid, feature_value, sum_t, max_t
        lists =  [self.sorted_train_features_inv[feature] for (feature,value) in features_sorted] 
        sorted_pairs = self._merge_n_sort_lists_get_top_k(lists, maxPairs)
        print('sim-end fase 1: elapsed {}'.format(time.time() - start_time))
        #result contains sorted pairs, e.g (feature_idx, (train_uid, ...))
        start_time = time.time()
        print('sim-start fase 2')
        for i in range(0, len(sorted_pairs)):
            feature_idx, (train_uid, feature_value, sum_t, max_t) = sorted_pairs[i]
            simils_instance[train_uid] += features_sorted[feature_idx][1] *  feature_value
        print('training instances: {}'.format(len(simils_instance)))
        print('sim-end fase 2: elapsed {}'.format(time.time() - start_time))
        start_time = time.time()
        print('sim-start fase 3')
        start_time = time.time()
        norm_test =sqrt(sum([val*val for feature,val in features.items()]))
        for train_uid, sum_simils in simils_instance.items():
            norm_train = self.train_norms[train_uid]
            simils_instance[train_uid] = sum_simils / float(norm_test * norm_train)
        print('sim-end fase 3: elapsed {}'.format(time.time() - start_time))
        start_time = time.time()
        print('sim-start fase 4')
        simils =  [(train_uid, simil) for train_uid, simil in simils_instance.items()] #note: could do partial sort here
        simils = sorted(simils, key=lambda tuple: tuple[1], reverse=True)
        #simils = simils[0:min(top_k,len(simils))]
        print('sim-end fase 4: elapsed {}'.format(time.time() - start_time))
        return simils
    '''
    '''
    First compute partial similarities for all instances in each dimension.
    Then compute partial similarities for all instances in maxK using pruning?
    Weird!
    '''
    '''
    def compute_similarity_TAAT_approximate_with_pruning_on_maxsize_weird(self, test_uid, top_k, maxK = 400):
        features = self.data.test_features[test_uid]
        simils_instance = defaultdict(lambda: 0)
        start_time = time.time()
        print('sim-start fase 1')
        features_sorted = [(feature, value) for (feature,value) in features.items()]
        features_sorted = sorted(features_sorted, key=lambda pair: self.sorted_features[pair[0]]) #sort features on INCREASING density, not trivial!
        print('features_sorted: {}'.format(features_sorted))
        pairs = 0
        #compute min value of t
        heap = []
        for (feature,value) in features_sorted:
            cnt = 0
            for (train_uid, feature_value, sum_t, max_t) in self.sorted_train_features_inv[feature]: #loop over ALL instances
                simils_instance[train_uid]+= value *  feature_value
                heapq.heappush(heap, simils_instance[train_uid])
                if len(heap) > top_k:
                    heapq.heappop(heap)
                pairs +=1
                if cnt > maxK:
                    break;
                cnt+=1
        print('training instances: {}, pairs: {}'.format(len(simils_instance), pairs))
        print('sim-end fase 1: elapsed {}'.format(time.time() - start_time))
        start_time = time.time()
        remainingMaxScore = 0
        for (feature,value) in features_sorted:
            global_max_weight_feature = self.sorted_train_features_inv[feature][0][1] #original pruning, but could also be maxK!!?!?
            remainingMaxScore += global_max_weight_feature * value
        #compute actual top-k, but know prune
        for (feature,value) in features_sorted:
            smallest = heap[0]
            if remainingMaxScore < smallest:
                print('breaking at feature: {}'.format(feature))
                break
            for (train_uid, feature_value, sum_t, max_t) in self.sorted_train_features_inv[feature][maxK:]: #loop over maxK instances
                simils_instance[train_uid]+= value *  feature_value
                pairs +=1
                heapq.heappushpop(heap, simils_instance[train_uid])
            global_max_weight_feature = self.sorted_train_features_inv[feature][0][1]
            remainingMaxScore -= global_max_weight_feature * value
        print('training instances: {}, pairs: {}'.format(len(simils_instance), pairs))
        print('sim-end fase 1B: elapsed {}'.format(time.time() - start_time))
        start_time = time.time()
        print('sim-start fase 2')
        simils =  [(train_uid, simil) for train_uid, simil in simils_instance.items()] #note: could do partial sort here
        simils = sorted(simils, key=lambda tuple: tuple[1], reverse=True)
        #simils = simils[0:min(top_k,len(simils))]
        print('sim-end fase 2: elapsed {}'.format(time.time() - start_time))
        return simils
    '''
    '''
    Firstcompute top-maxK partial similarities, and store set of training UIDS.
    Next compute similarities for remaining instances and prune on the number of features of each instance?
    '''
    '''
    def compute_similarity_2_level_TAAT_first_topk_And_next_pruning_minsize(self, test_uid, top_k, maxK = 400):
        print('compute_similarity_2_level_TAAT_first_topk_And_next_pruning_minsize')
        features = self.data.test_features[test_uid]
        simils_instance = defaultdict(lambda: 0)
        start_time = time.time()
        print('sim-start fase 1')
        features_sorted = [(feature, value) for (feature,value) in features.items()]
        features_sorted = sorted(features_sorted, key=lambda pair: self.sorted_features[pair[0]]) #sort features on INCREASING density, not trivial!
        print('features_sorted: {}'.format(features_sorted))
        pairs = 0
        #compute min value of t
        heap = []
        set_top_400_weights = set() #can 
        for (feature,value) in features_sorted:
            cnt = 0
            for (train_uid, feature_value, sum_t, max_t) in self.sorted_train_features_inv[feature]:
                simils_instance[train_uid]+= value *  feature_value
                heapq.heappush(heap, simils_instance[train_uid])
                set_top_400_weights.add(train_uid) #can be compute during preprocessing for each dimension!
                if len(heap) > top_k:
                    heapq.heappop(heap)
                pairs +=1
                if cnt > maxK:
                    break;
                cnt+=1
        print('training instances: {}, pairs: {}'.format(len(simils_instance), pairs))
        print('sim-end fase 1: elapsed {}'.format(time.time() - start_time))
        start_time = time.time()
        remainingMaxScore = 0
        for (feature,value) in features_sorted:
            l = min(len(self.sorted_train_features_inv[feature]) -1, maxK)
            global_max_weight_feature = self.sorted_train_features_inv[feature][l][1] #original pruning, but could also be maxK!!?!?
            remainingMaxScore += global_max_weight_feature * value
        smallest = heap[0]
        max_weight = max(features_sorted, key=lambda tuple: tuple[1])[1]
        minSize = (smallest / max_weight)**2
        print('minsize: {:.1f}'.format(minSize))
        #compute actual top-k, but know prune
        for (feature,value) in features_sorted:
            smallest = heap[0]
            minSize = (smallest / max_weight)**2 #note: could also updated, each time after heap
            if remainingMaxScore < smallest:
                print('breaking at feature: {}'.format(feature))
                break
            start = 0
            for (train_uid, feature_value, feature_length) in self.sorted_train_features_inv_length[feature]:
                if feature_length < minSize:
                    start+=1
                else:
                    break
            if start != 0:
                print('minsize pruning start: {}'.format(start))
            for (train_uid, feature_value, feature_length) in self.sorted_train_features_inv_length[feature][start:]:
                if train_uid in set_top_400_weights:
                    continue
                simils_instance[train_uid]+= value *  feature_value
                pairs +=1
                heapq.heappushpop(heap, simils_instance[train_uid])
            l = min(len(self.sorted_train_features_inv[feature]) -1, maxK)
            global_max_weight_feature = self.sorted_train_features_inv[feature][l][1]
            remainingMaxScore -= global_max_weight_feature * value
        print('training instances: {}, pairs: {}'.format(len(simils_instance), pairs))
        print('sim-end fase 1B: elapsed {}'.format(time.time() - start_time))
        start_time = time.time()
        print('sim-start fase 2')
        simils =  [(train_uid, simil) for train_uid, simil in simils_instance.items()] #note: could do partial sort here
        simils = sorted(simils, key=lambda tuple: tuple[1], reverse=True)
        #simils = simils[0:min(top_k,len(simils))]
        print('sim-end fase 2: elapsed {}'.format(time.time() - start_time))
        return simils
    
    '''


    '''
    Implementation of Fagin Threshold Algorithm
    https://eclass.uoa.gr/modules/document/file.php/DI416/Slides/TOPK-queries.pdf
    
    
    Loop over i=0,... i=max(posting_lsts) (DAAT)
        Loop over all features (sorted on increasing density):
            Compute full similarity of instances at index i(starts with 0) in posting lists
        Stop outer loop when threshold < min_heap
        threshold = x_q . features-values-of-several-instances-at-index-i   
    Assumes all posting list are sorted on decreasing values
    
    catch: random access required for computing full similarity
    '''
    def compute_similarity_DAAT_pruning_with_Fagin_Threshold(self, test_uid, top_k):
        self._print('>compute_similarity_DAAT_pruning_with_Fagin_Threshold')
        features = self.data.test_features[test_uid]
        simils_instance = defaultdict(lambda: 0)
        a_start_time = start_time = time.time()
        self._print('>sim-start fase 1')
        features_sorted = [(feature, value) for (feature,value) in features.items()]
        features_sorted = sorted(features_sorted, key=lambda pair: self.sorted_features.get(pair[0],sys.maxsize)) #sort features on INCREASING density, not trivial!
        pairs = 0
        pairs_no_zeros = 0
        i = 0
        heap = []
        while True:
            threshold = 0.0
            #loop in sorted order
            all_continue = True
            for (feature, value) in features_sorted:
                if i>= len(self.sorted_train_features_inv[feature]): #default sorted on descending weight
                    continue 
                all_continue = False
                (train_uid,feature_value,sum_x, max_weight) = self.sorted_train_features_inv[feature][i]
                #if train_uid == 1119:
                #    print('train:{}'.format(sorted([(feature, value) for feature, value in self.data.train_features[train_uid].items()], key=lambda tuple: tuple[0])))
                #    print('test: {}'.format(sorted(features_sorted, key=lambda tuple: tuple[0])))
                threshold += feature_value * value
                if simils_instance[train_uid] != 0:
                    continue
                else:
                    #compute complete similarity
                    simil = 0.0
                    train_features = self.data.train_features[train_uid]
                    for (feature2, value2) in features_sorted: 
                        simil += value2 * train_features.get(feature2,0.0)
                        pairs +=1 #als counting ZERO multiplications
                        if train_features.get(feature2,0.0) != 0.0:
                            pairs_no_zeros +=1 
                    simils_instance[train_uid] = simil
                    if len(heap) < top_k:
                        heapq.heappush(heap, (simil, train_uid))
                    else:
                        heapq.heappushpop(heap, (simil, train_uid))
            i+=1
            min_heap_simil = heap[0][0]
            #print('threshold: {}'.format(threshold))
            if len(heap) == top_k and min_heap_simil > threshold:
                self._print('pruning after: {} candidates'.format(i))
                break
            if all_continue:
                break
        result = []
        while len(heap) > 0:
            (simil, train_uid) = heapq.heappop(heap)
            result.append((train_uid, simil))
        result.reverse()
        self._print('<sim-end fase 1: elapsed {}'.format(time.time() - start_time))
        self.info['instances'] = len(simils_instance)
        self.info['pairs'] = pairs_no_zeros
        self.info['elapsed'] = time.time() - a_start_time
        self._print('info: training instances: {}, pairs: {}, pairs (zeros not counting): {}'.format(len(simils_instance), pairs, pairs_no_zeros))
        return result
    
    
    '''
    WAND  fontoura/broder
    
    def compute_similarity_DAAT_pruning_with_WAND(self, test_uid, top_k): 
        self._print('>compute_similarity_DAAT_pruning_with_WAND')
        a_start_time = start_time = time.time()
        self._print('>sim-start fase 1')
        #step1 compute upper bounds
        test_features = [(feature, value) for (feature,value) in  self.data.test_features[test_uid].items()]
        test_features = sorted(test_features, key=lambda pair: self.sorted_features[pair[0]])
        offsets = [0 for i in range(0,len(test_features))]
        heap = []
        pairs = 0
        candidates_total = 0
        min_heap_simil = 0
        while True:
            #traverse in document order
            candidates = []
            for i in range(0,len(test_features)):
                feature, test_feature_value = test_features[i]
                doclist_i = self.sorted_train_features_inv_docid[feature] #sorted on document order
                if offsets[i] >= len(doclist_i):
                    continue
                train_uid, train_feature_value= doclist_i[offsets[i]]
                candidates.append((train_uid, i))
            candidates = sorted(candidates, key=lambda tuple: tuple[0]) #sort on docId
            pivot_term = None
            bound_pivot = 0
            for train_uid, i in candidates:
                feature ,test_feature_value = test_features[i]
                bound_pivot += self.sorted_train_features_inv_maxweight[feature] * test_features[i][1] #previously: bug by forgotting to multiply with test_feature_value
                if len(heap) < top_k or bound_pivot > min_heap_simil:
                    pivot_term = (train_uid, i)
                    break
                else:
                    continue 
    
            if len(candidates) == 0:
                break
            if pivot_term is None: #CHANGE LEN!
                pivot_term = candidates[-1]
            #compute similarity of pivot
            candidates_total+=1
            pivot_train_uid = pivot_term[0]
            simil = 0
            for i in range(0,len(test_features)):
                feature, test_feature_value = test_features[i]
                doclist_i = self.sorted_train_features_inv_docid[feature]
                next_index, train_feature_value = self._search_doclist(doclist_i, offsets[i], pivot_train_uid) 
                offsets[i] = next_index #main wand/m-wand difference from fontoura
                if train_feature_value != 0.0:
                    pairs +=1
                    simil += test_feature_value * train_feature_value
            if len(heap) < top_k:
                heapq.heappush(heap, (simil, pivot_train_uid))
            else:
                heapq.heappushpop(heap, (simil, pivot_train_uid))
                min_heap_simil = heap[0][0]
            if self.super_debug:
                candidates_print = {}
                for (train_uid, i) in candidates:
                    if train_uid in candidates_print:
                        candidates_print[train_uid]+=1
                    else:
                        candidates_print[train_uid] =1
                self._print('candidates: {}, pivot:{}, heap-min: {}, ubound_pivot:{}'.format(candidates_print,
                                                                                             pivot_term, heap[0], bound_pivot))
        result = []
        while len(heap) > 0:
            (simil, train_uid) = heapq.heappop(heap)
            result.append((train_uid, simil))
        result.reverse()
        self._print('<sim-end fase 1: elapsed {}'.format(time.time() - start_time))
        self.info['instances'] = candidates_total
        self.info['pairs'] = pairs
        self.info['elapsed'] = time.time() - a_start_time
        self._print('info: training instances: {}, pairs: {}'.format(candidates_total, pairs))
        return result    
    '''
    
    '''
    WAND  fontoura/broder
    '''
    def compute_similarity_DAAT_pruning_with_WAND_2(self, test_uid, top_k): 
        self._print('>compute_similarity_TAAT_then_DAAT_pruning_WAND_2')
        a_start_time = start_time = time.time()
        self._print('>sim-start fase 1')
        test_features = [(feature, value) for (feature,value) in self.data.test_features[test_uid].items()]
        test_features = sorted(test_features, key=lambda pair: self.sorted_features.get(pair[0], sys.maxsize)) #sort features on INCREASING density, not trivial!
        pairs = 0
        heap = []
        #REMARK: should be created at index time
        maxweights_after_400 = [self.sorted_train_features_inv_maxweight.get(feature,0.0) * value for feature, value in test_features] 
        offsets = [0 for i in range(0,len(test_features))] 
        iter = 0
        candidates_total = 0
        if self.super_debug:
            print('Index:')
            for (feature,value) in self.data.test_features[test_uid].items():
                print('feature {}:'.format(feature), end='')
                for (train_uid, feature_value) in self.sorted_train_features_inv_docid[feature]:
                    print('<{},{}>'.format(train_uid, feature_value), end='')
                print('')
        while True:
            #traverse in document order
            candidates = defaultdict(set)    
            for i in range(0,len(test_features)):
                feature, test_feature_value = test_features[i]
                doclist_i = self.sorted_train_features_inv_docid[feature]
                if offsets[i] >= 0 and offsets[i] < len(doclist_i): 
                    train_uid, train_feature_value= doclist_i[offsets[i]]
                    candidates[train_uid].add(i)
            candidates = sorted(candidates.items(), key=lambda tuple: tuple[0]) #sort on docId
            pivot_term = None
            bound_pivot = 0
            for train_uid, features_set in candidates:  #contained bug
                for i in features_set:
                    bound_pivot += maxweights_after_400[i] #e.g. als geen candidates meer over -> geen term voor ubound
                if len(heap) < top_k or bound_pivot > heap[0][0]:
                    pivot_term = (train_uid, features_set)
                    break
            if len(candidates) == 0:
                break;
            if pivot_term is None:
                pivot_term = candidates[-1]
            #compute similarty of pivot
            candidates_total+=1
            pivot_train_uid = pivot_term[0]
            simil = 0
            for i, (feature, feature_value) in enumerate(test_features): #note: must not loop over all features, only features in pivot? unclwar
                doclist_i = self.sorted_train_features_inv_docid[feature]
                next_index, train_feature_value = self._search_doclist(doclist_i, offsets[i], pivot_train_uid) 
                offsets[i] = next_index #main wand/m-wand difference from fontoura
                if train_feature_value != 0.0:
                    pairs +=1
                    simil += feature_value * train_feature_value
            if len(heap) < top_k:
                heapq.heappush(heap, (simil, pivot_train_uid))
            else:
                heapq.heappushpop(heap, (simil, pivot_train_uid))
            if self.super_debug:
                self._print('iteration: {}, candidates: {}, pivot:{}, heap-min: {}, ubound_pivot:{}'.format(
                    iter, candidates, pivot_term, heap[0], bound_pivot))
      
            iter+=1
        result = []
        while len(heap) > 0:
            (simil, train_uid) = heapq.heappop(heap)
            result.append((train_uid, simil))
        result.reverse()
        self._print('<sim-end fase 1: elapsed {}'.format(time.time() - start_time))
        self.info['instances'] = candidates_total
        self.info['pairs'] = pairs
        self.info['elapsed'] = time.time() - a_start_time   
        self._print('info: training instances: {}, pairs: {}'.format(candidates_total, pairs))
        return result
        
    
    '''
    TAAT top-m + DAAT pruning based on partition by Feremans
    '''
    def compute_similarity_TAAT_then_DAAT_pruning_WAND(self, test_uid, top_k): 
        self._print('>compute_similarity_TAAT_then_DAAT_pruning_WAND')
        a_start_time = start_time = time.time()
        self._print('>sim-start fase 1')
        if self.super_debug:
            print('FULL index:')
            for (feature,value) in self.data.test_features[test_uid].items():
                print('feature {}:'.format(feature), end='')
                for (train_uid, feature_value, s, m) in self.sorted_train_features_inv[feature]:
                    print('<{},{}>'.format(train_uid, feature_value), end='')
                print('')
            print("index TAAT:")
            for (feature,value) in self.data.test_features[test_uid].items():
                print('feature {}:'.format(feature), end='')
                for (train_uid, feature_value) in self.train_features_inv_taat[feature]:
                    print('<{},{}>'.format(train_uid, feature_value), end='')
                print('')
            print("index DAAT:") 
            for (feature,value) in self.data.test_features[test_uid].items():
                print('feature {}:'.format(feature), end='')
                for (train_uid, feature_value) in self.train_features_inv_daat[feature]:
                    print('<{},{}>'.format(train_uid, feature_value), end='')
                print('')
        test_features = [(feature, value) for (feature,value) in self.data.test_features[test_uid].items()]
        test_features = sorted(test_features, key=lambda pair: self.sorted_features.get(pair[0], sys.maxsize)) #sort features on INCREASING density, not trivial!
        pairs = 0
        #compute min value of t
        simils_instance = defaultdict(lambda:0.0)
        for (feature,value) in test_features:
            for (train_uid, feature_value) in self.train_features_inv_taat[feature]:
                simils_instance[train_uid]+= value *  feature_value
                pairs +=1
        candidates_total= len(simils_instance)
        candidates_total_taat = candidates_total
        pairs_total_taat = pairs
        #Add all top heap
        heap = []
        for train_uid, simil in simils_instance.items():
            if len(heap) < top_k:
                heapq.heappush(heap, (simil, train_uid))
            else:
                heapq.heappushpop(heap, (simil, train_uid))   
        if self.debug:
            heap_temp = copy(heap)
            for i in range(0,100):
                if len(heap_temp) > 0:
                    (simil_i, train_uid) = heapq.heappop(heap_temp)
                    print('sim_{}: {}'.format(train_uid, simil_i))
        self._print('<sim-end fase 1: elapsed {}'.format(time.time() - start_time))
        #REMARK: should be created at index time
        maxweights_after_400 = [self.train_features_inv_daat_max.get(feature,0.0) * value for feature, value in test_features] 
        offsets = [0 for i in range(0,len(test_features))]
        self._print('>sim-start fase 2')
        start_time = time.time()    
        iter = 0
        while True:
            #traverse in document order
            candidates = defaultdict(set)    
            for i in range(0,len(test_features)):
                feature, test_feature_value = test_features[i]
                doclist_i = self.train_features_inv_daat[feature]
                if offsets[i] >= 0 and offsets[i] < len(doclist_i): 
                    train_uid, train_feature_value= doclist_i[offsets[i]]
                    candidates[train_uid].add(i)
            candidates = sorted(candidates.items(), key=lambda tuple: tuple[0]) #sort on docId
            pivot_term = None
            bound_pivot = 0
            for train_uid, features_set in candidates:  #contained bug
                for i in features_set:
                    bound_pivot += maxweights_after_400[i] #e.g. als geen candidates meer over -> geen term voor ubound
                if len(heap) < top_k or bound_pivot > heap[0][0]:
                    pivot_term = (train_uid, features_set)
                    break
            if len(candidates) == 0:
                break;
            if pivot_term is None:
                pivot_term = candidates[-1]
            #compute similarty of pivot
            candidates_total+=1
            pivot_train_uid = pivot_term[0]
            simil = 0
            for i, (feature, feature_value) in enumerate(test_features): #note: must not loop over all features, only features in pivot? unclwar
                doclist_i = self.train_features_inv_daat[feature]
                next_index, train_feature_value = self._search_doclist(doclist_i, offsets[i], pivot_train_uid) 
                offsets[i] = next_index #main wand/m-wand difference from fontoura
                if train_feature_value != 0.0:
                    pairs +=1
                    simil += feature_value * train_feature_value
            if len(heap) < top_k:
                heapq.heappush(heap, (simil, pivot_train_uid))
            else:
                heapq.heappushpop(heap, (simil, pivot_train_uid))
            if self.super_debug:
                sum_txt = ''
                for (feature, feature_value) in test_features:
                    sum_txt += '{}x{} +'.format(feature_value, self.data.train_features[pivot_train_uid].get(feature,0.0))
                print('iteration: {}, candidates: {}, pivot:{}, pivot-simil:{}, heap-min: {}, ubound_pivot:{}'.format(
                    iter, candidates, pivot_term, sum_txt, heap[0], bound_pivot))
            iter+=1
        result = []
        while len(heap) > 0:
            (simil, train_uid) = heapq.heappop(heap)
            result.append((train_uid, simil))
        result.reverse()
        self._print('<sim-end fase 2: elapsed {}'.format(time.time() - start_time))
        self.info['instances'] = candidates_total
        self.info['pairs'] = pairs
        self.info['elapsed'] = time.time() - a_start_time
        self.info['instances TAAT only'] = candidates_total_taat
        self.info['pairs TAAT only'] = pairs_total_taat        
        self._print('info: training instances: {}, pairs: {}, instances TAAT only:{}, pairs TAAT only:{}'.format(candidates_total, pairs, candidates_total_taat, pairs_total_taat))
        return result    
    
    #for WAND                  
    def _search_doclist(self, doclist_i, offset, train_uid):
        for i in range(offset, len(doclist_i)):
            train_uid2, fvalue = doclist_i[i] 
            if train_uid2 < train_uid:
                continue
            elif train_uid2 == train_uid:
                return i+1, fvalue
            else:
                return i, 0.0
        return len(doclist_i), 0.0
  
        
  
class Experiments:
    
    def __init__(self):
        self.topK_parameter = 20
        self.topM_start = 0
        self.topM_end = 1
        self.debug=False
        #self.train_file = '../data/IMDB-F-train.libsvm'
        #self.test_file = '../data/IMDB-F-test.libsvm'
        #self.train_file = '../data/XMLRepository/Eurlex/eurlex_train.txt'
        #self.test_file = '../data/XMLRepository/Eurlex/eurlex_test.txt'
        #self.train_file = '../data/XMLRepository/Wiki10/wiki10_train.txt'
        #self.test_file = '../data/XMLRepository/Wiki10/wiki10_test.txt'
        #self.train_file = '../data/XMLRepository/AmazonCat/amazonCat_train.txt'
        #self.test_file = '../data/XMLRepository/AmazonCat/amazonCat_test.txt'
        #self.train_file = '../data/XMLRepository/RCV1-X/rcv1x_train.txt'
        #self.test_file = '../data/XMLRepository/RCV1-X/rcv1x_test.txt'
        #self.train_file = '../data/XMLRepository/WikiLSHTC/wikiLSHTC_train.txt'
        #self.test_file = '../data/XMLRepository/WikiLSHTC/wikiLSHTC_test.txt'
        
    
    def print_results(self, title, simils, simils_golden_standard):
        print("--------------------------")
        print(title)
        print("--------------------------")
        print("top 20:")
        for i in range(0,20):
            print('sim_ {}: {:.4f}'.format(simils[i][0], simils[i][1]))
        overlap = 0
        for (train_uid, simil) in simils:
            if train_uid in  simils_golden_standard:
                overlap +=1
        print('overlap top-{}: {}'.format(len(simils_golden_standard), overlap))
        print("--------------------------")
        print('')
         
    def experiment_check_correctness_and_pruning(self, test_uid = 0):
        #test _merge_n_sort_lists_get_top_k 
        #r = userKNN._merge_n_sort_lists_get_top_k([[('jos', 500),('joke', 400), ('bart',200), ('koen',1),('jan',-10)], 
        #                                           [('len',100), ('pascale',50), ('leon', 40), ('jules', 30)],
        #                                           [('kevin',5), ('bob',4), ('elvis', 3)]], 10)
        
        data = Data(self.train_file, self.test_file, skipFirstLine=True, loadBinary=False)
        #data.print_stats()
        data.normalize_row()
        print("first row:{}".format(data.train_features[0]))
        
        
        if test_uid == 0:
            test_uid = random.randint(0, len(data.test_features) -1)
            
        pruningTest = TopKPruningTest(data)
        pruningTest.debug = True
        pruningTest.makeIndexes()
        pruningTest.makeIndexesPartition(self.topM)
        #Evaluate correctness
        simils1 = pruningTest.compute_similarity_TAAT_no_pruning(test_uid, self.topK_parameter)
        simils1_uids= {train_uid: simil for (train_uid,simil) in simils1[0:self.topK_parameter]}
        self.print_results("compute_similarity_TAAT_no_pruning", simils1, simils1_uids)
        
        simils2 = pruningTest.compute_similarity_DAAT_pruning_with_Fagin_Threshold(test_uid, self.topK_parameter)
        self.print_results('compute_similarity_DAAT_pruning_with_Fagin_Threshold', simils2, simils1_uids)
     
        #simils3 = pruningTest.compute_similarity_DAAT_pruning_with_WAND(test_uid, self.topK_parameter)
        #self.print_results('compute_similarity_DAAT_pruning_with_WAND', simils3, simils1_uids)
        
        simils3 = pruningTest.compute_similarity_DAAT_pruning_with_WAND_2(test_uid, self.topK_parameter)
        self.print_results('compute_similarity_DAAT_pruning_with_WAND_2', simils3, simils1_uids)
    
        simils4 = pruningTest.compute_similarity_TAAT_then_DAAT_pruning_WAND(test_uid, self.topK_parameter);    
        self.print_results('compute_similarity_TAAT_then_DAAT_pruning_WAND', simils4, simils1_uids)
        
        
    def _mean(self, numbers):
        return sum(numbers)/float(len(numbers))
    
    def _printp(self, i):
        print('-',end='')
        sys.stdout.flush()
        if i % 50 == 0:
            print('')
    
    def experiment_check_average_number_of_full_evaluations(self, noTestInstances):
        print('loading ' + self.train_file)
        print('parameters: noTestInstances={}, topK={}'.format(noTestInstances, self.topK_parameter))
        data = Data(self.train_file, self.test_file, skipFirstLine=True, loadBinary=False)
        data.normalize_row()
        
        print('makeIndexes')
        pruningTest = TopKPruningTest(data)
        pruningTest.debug = True
        pruningTest.makeIndexes()
        pruningTest.debug = False
        
        test_uids = sorted(data.test_features.keys())
        if noTestInstances != -1:
            random.shuffle(test_uids)
            if noTestInstances > len(test_uids):
                noTestInstances = len(test_uids)
            test_uids = test_uids[0:noTestInstances]
        
        #1) Baseline
        print('TAAT')
        no_instances_baseline = []
        elapsed_baseline = []
        i=0
        for test_uid in test_uids:
            i+=1
            self._printp(i)
            pruningTest.compute_similarity_TAAT_no_pruning(test_uid, self.topK_parameter)
            no_instances_compared = pruningTest.info['instances']
            elapsed_time = pruningTest.info['elapsed']
            no_instances_baseline.append(no_instances_compared)
            elapsed_baseline.append(elapsed_time)
        print('Avg query time: {}. Min query time:{}. Max query time:{}. '.format(self._mean(elapsed_baseline), min(elapsed_baseline), max(elapsed_baseline)))
        print('Avg no instances: {}. Min no instances: {}. Max no instances: {}'.format(self._mean(no_instances_baseline), min(no_instances_baseline), max(no_instances_baseline)))
            
        print('FAGIN')
        no_instances_fagin = []
        elapsed_fagin = []
        i=0
        for test_uid in test_uids:
            i+=1
            self._printp(i)
            pruningTest.compute_similarity_DAAT_pruning_with_Fagin_Threshold(test_uid, self.topK_parameter)
            no_instances_compared = pruningTest.info['instances']
            elapsed_time = pruningTest.info['elapsed']
            no_instances_fagin.append(no_instances_compared)
            elapsed_fagin.append(elapsed_time)
        print('Avg query time: {}. Min query time:{}. Max query time:{}. '.format(self._mean(elapsed_fagin), min(elapsed_fagin), max(elapsed_fagin)))
        print('Avg no instances: {}. Min no instances: {}. Max no instances: {}'.format(self._mean(no_instances_fagin), min(no_instances_fagin), max(no_instances_fagin)))
     
        #no_instances_wand = []
        #elapsed_wand = []
        #i=0
        #for test_uid in test_uids:
        #    i+=1
        #    if i % 100 == 0: print('-')
        #    pruningTest.compute_similarity_DAAT_pruning_with_WAND(test_uid, self.topK_parameter)
        #    no_instances_compared = pruningTest.info['instances']
        #    elapsed_time = pruningTest.info['elapsed']
        #    no_instances_wand.append(no_instances_compared)
        #    elapsed_wand.append(elapsed_time)
        #print('Avg query time: {}. Min query time:{}. Max query time:{}. '.format(self._mean(elapsed_wand), min(elapsed_wand), max(elapsed_wand)))
        #print('Avg no instances: {}. Min no instances: {}. Max no instances: {}'.format(self._mean(no_instances_wand), min(no_instances_wand), max(no_instances_wand)))
        print('WAND')
        no_instances_wand = []
        elapsed_wand = []
        i=0
        for test_uid in test_uids:
            i+=1
            self._printp(i)
            pruningTest.compute_similarity_DAAT_pruning_with_WAND_2(test_uid, self.topK_parameter)
            no_instances_compared = pruningTest.info['instances']
            elapsed_time = pruningTest.info['elapsed']
            no_instances_wand.append(no_instances_compared)
            elapsed_wand.append(elapsed_time)
        print('Avg query time: {}. Min query time:{}. Max query time:{}. '.format(self._mean(elapsed_wand), min(elapsed_wand), max(elapsed_wand)))
        print('Avg no instances: {}. Min no instances: {}. Max no instances: {}'.format(self._mean(no_instances_wand), min(no_instances_wand), max(no_instances_wand)))
        
        print('PARTITION')
        for topM in range(self.topM_start,self.topM_end+1):
            print('topM={}'.format(topM))
            print('makeIndexesPartition')
            pruningTest.makeIndexesPartition(topM)
            print('run simil topM')
            i=0 
            no_instances_partition = []
            elapsed_partition = []
            for test_uid in test_uids:
                i+=1
                self._printp(i)
                pruningTest.compute_similarity_TAAT_then_DAAT_pruning_WAND(test_uid, self.topK_parameter)
                no_instances_compared = pruningTest.info['instances']
                elapsed_time = pruningTest.info['elapsed']
                no_instances_partition.append(no_instances_compared)
                elapsed_partition.append(elapsed_time)  
            print('Avg query time: {}. Min query time:{}. Max query time:{}. '.format(self._mean(elapsed_partition), min(elapsed_partition), max(elapsed_partition)))
            print('Avg no instances: {}. Min no instances: {}. Max no instances: {}'.format(self._mean(no_instances_partition), min(no_instances_partition), max(no_instances_partition)))
                


def unit_test1():
    pruningTest = TopKPruningTest(Data(None,None))
    pruningTest.debug = True
    pruningTest.super_debug = True
    pruningTest.data.test_features[0] = {0:5,1:4,2:1}
    pruningTest.sorted_features = [0, 1, 2]
    pruningTest.sorted_train_features_inv_docid[0] = {1:3, 2:4, 10:2}
    pruningTest.sorted_train_features_inv_docid[1] = {1:4, 2:2, 7:10, 8:5, 9:2, 11:5}
    pruningTest.sorted_train_features_inv_docid[2] = {1:6, 2:8, 5:1, 6:20, 10:1, 11:7}
    pruningTest.data.train_features_inv =  pruningTest.sorted_train_features_inv_docid
    #pruningTest.data.train_features[1] = {0:3,1:4,2:6}
    for feature, doclist in pruningTest.sorted_train_features_inv_docid.items():
        for train_uid, featurevalue in doclist.items():
            if not(train_uid in pruningTest.data.train_features):
                pruningTest.data.train_features[train_uid] = {}
            pruningTest.data.train_features[train_uid][feature] = featurevalue    
    pruningTest.makeIndexes(topM=1)
    #similstest = pruningTest.compute_similarity_DAAT_pruning_with_WAND(0, 6)
    #print(similstest)
    similstest = pruningTest.compute_similarity_DAAT_pruning_with_WAND_2(0, 6)
    print(similstest)
    similstest = pruningTest.compute_similarity_TAAT_then_DAAT_pruning_WAND(0, 6)
    print(similstest)
   
        
                
if __name__ == "__main__":
    unit_tests = False
    correctness = False
    experiment_no_full_evaluations = True
    
    noTestInstances=100
    topM_start=1
    topM_end=1
    topK=20
    train_file = None
    test_file = None
    
    if len(sys.argv) > 1:
        topM_start=int(sys.argv[1])
        topM_end=int(sys.argv[2])
        topK = int(sys.argv[3])
        noTestInstances = int(sys.argv[4])
        train_file = str(sys.argv[5])
        test_file = str(sys.argv[6])
    else:
        print("Error: set parameters")
        
        
    if unit_tests:
        unit_test1()    
    
    if correctness:
        experiments = Experiments()
        experiments.experiment_check_correctness_and_pruning()
    
    if experiment_no_full_evaluations:
        experiments = Experiments()
        experiments.topM_start = topM_start
        experiments.topM_end = topM_end
        experiments.topK_parameter = topK
        experiments.train_file = train_file
        experiments.test_file = test_file
        experiments.experiment_check_average_number_of_full_evaluations(noTestInstances)
        
        