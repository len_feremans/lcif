./run_lcif_on_extreme.sh exampleBasedAccuracy >> ./results2/extreme_acc.txt
./run_lcif_on_extreme.sh microF1 >> ./results2/extreme_microF1.txt
./run_lcif_on_extreme.sh macroF1 >> ./results2/extreme_macroF1.txt
./run_lcif_on_extreme.sh precision@5 >> ./results2/extreme_prec_at_5.txt
#./run_lcif_on_extreme.sh precision >> ./results/extreme_prec.txt
#./run_lcif_on_extreme.sh recall >> ./results/extreme_recall.txt