# LCIF: Combining instance and feature neighbours for extreme multi-label classification #
Implementation of _Combining instance and feature neighbours for extreme multi-label classification_, 
by Len Feremans, data scientist at the University of Antwerp (Belgium) within the Adrem Data lab research group.

> Extreme multi-label classification problems occur in different applications such as prediction of tags or advertisements. We propose a new algorithm that predicts labels using a linear ensemble of labels from instance- and feature-based nearest neighbours. In the feature-based nearest neighbours method, we precompute a matrix containing the similarities between each feature and label. For the instance-based nearest neighbourhood, we create an algorithm that uses an inverted index to compute cosine similarity on sparse datasets efficiently. We extend this baseline with a new top- k query algorithm that combines term-at-a-time and document-at-a-time traversal with tighter pruning based on a partition of the dataset. On ten real-world datasets, we find that our method outperforms state-of-the-art methods such as multi-label k-nearest neighbours, instance-based logistic regression, binary relevance with support vector machines and FastXML on different evaluation metrics. We also find that our algorithm is orders of magnitude faster than these baseline algorithms on sparse datasets, and requires less than 20 ms per instance to predict labels for extreme datasets without the need for expensive hardware.

Submitted in 2020 to the International Journal of Data Science and Analytics (JDSA20) 

### What is this repository for? ###
You can use our algorithm as an efficient way to predicted multi-label classification on datasets having an extremely large number of labels.

### Quick start ###
The implementation is written in C++ and uses [Cmake](https://cmake.org/) for compilation. 
To compile the project using CMake:
```
> ccmake .
```
[Select configure and generate]
```
> make
```

LCIF can be invoked command-line *without requiring any parameters* on **large datasets** in **libsvm format**. 
In this case LCIF automatically finds the parameters optimised for micro F1 using *gridsearch* (cross-validated 10-fold).
```
> lcif GS microF1 ./data/medical-train.libsvm ./data/medical-test.libsvm
```


LCIF can be invoked command-line on **extreme datasets** in **libsvm format** *without setting parameter* .
In this case LCIF automatically finds the parameters optimised for micro F1 using a sample of 1000 instances.
```
> lcif GS_BIG microF1 ./data/XMLRepository/AmazonCat/amazonCat_train.txt ./data/XMLRepository/AmazonCat/amazonCat_test.txt
```

To run LCIF with parameters command-line:
```
> lcif RUN k=10 alpha=1.0 beta=1.0 lambda=0.5 t=0.5 ./data/medical-train.libsvm ./data/medical-test.libsvm
```
Description of all parameters is found in the paper, but in summary:

* Training and test files are in **libsvm format**. We also expect the first line to contain the number-of-instances number-of-features and number-of-labels. 
* **k**: number of neighbours used for instanced-based weighted predictions. Good choices are {1,5,50,100,..350}
* **alpha**: power transform of similarities for instance-based weighted predictions. Good choices are {0.5,1.0,1.5,2.0}
* **beta**: power transform of similarities for feature-based predictions. Good choices are {0.5,1.0,1.5,2.0}
* **lambda**: give more weight to instance-based of feature-based predictions. Between 1.0 and 0.0 (1.0=only instance-based predictions, 0.0=only feature-based predictions)
* **t**: threshold for making predictions. Between 1.0 and 0.0.


To run code from python, you use *subprocess*, i.e.:
```python
def run_lcif(mode, metric, fname_train, fname_test):
    LCIF_PATH = '~/git/lcif/'
    cmd = ["./lcif", str(mode), metric, fname_train, fname_test]
    p = subprocess.Popen(cmd, cwd=LCIF_PATH)
    p.wait()
```


To run code from C++, you use  the API, i.e.:
```C++
Data train = Data("./data/medical-train.libsvm");
train.makeIndex();
Data test = Data("./data/medical-test.libsvm", -1);
//Instance-based kNN 
vector<float> normsInstancesTrain = train.normalize_rows();
vector<float> normsInstancesTest = test.normalize_rows();
RowKNNPruning rowKNNPruning(train, test);
rowKNNPruning.setParameters(10, false); //k, verbose parameter
rowKNNPruning.runTAATPreProcessing(100); //parameter m
rowKNNPruning.runDAATPreProcessing();
rowKNNPruning.computeSimilarityMatrix(64); //no threads
RowPredictions rowPredictions(train,test,rowKNNPruning.getKNNMatrix());
rowPredictions.setParameters(10, 1.0, 1000); //k, alpha and internal parameter (storek)
rowPredictions.computeScores(64, false); //no threads
const vector<vector<pair<int,float>>>& scoresRowKNN = rowPredictions.getPredictions();
//Features-based kNN
train.unnormalize_rows(normsInstancesTrain);
test.unnormalize_rows(normsInstancesTest);
unordered_map<int,float> norms = train.normalize_features();
test.normalize_features(norms);
FeatureKNN featureKNN(train, false); //verbose parameter
featureKNN.setParameters(1, 1000, 1.0); //k (set to 1), internal parameter (storek), beta
featureKNN.computeSimilarityMatrix(64); //no threads
featureKNN.computeTopKSortedSimilarityMatrixForFeatures(1000); //internal parameter (candidateSelectionK)
featureKNN.computeSortedSimilarityMatrixForLabels();
featureKNN.computeScores(test, 64, false); //no threads, verbose
const vector<vector<pair<int,float>>>& scoresFeatureKNN = featureKNN.getPredictions();
//LCIF
LCIF lcif;
vector<vector<pair<int,float>>> scoresLCIF = lcif.combinePredictions(0.5, 1000, scoresRowKNN, scoresFeatureKNN); //lambda, internal parameter (storek)
SingleThreshold st(test);
Evaluator evaluator(train,test);
cout << " Ins. KNN: "  << evaluateMetric(evaluator, st, "precision@5", false, scoresRowKNN)
     << " Feat. KNN: " << evaluateMetric(evaluator, st, "precision@5", false, scoresFeatureKNN)
     << " LCIF: " << evaluateMetric(evaluator, st, "precision@5", false, scoresLCIF) << endl;
```     
### Status ###

Current status:

* Done: First public version
* TODO: RUN method

### More information for researchers and contributors ###
The current version is 0.9.0. Created 3 Februari 2020, Last Februari 2020.

To reproduce the experiments for JDSA20 see scripts folder.

To contribute to this project: The design of the C++ code is _straightforward_ and you could extend this codebase for research. For example,
it should be relatively straigthforward to adjust the prediction function and make implementation of other nearest neighbour methods scalable. 

### Who do I talk to? ###
E-mail Len Feremans (gmail) for more information.

### Licence ###
Copyright (c) [2020] [Universiteit Antwerpen - Len Feremans]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
