# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.2

# Default target executed when no arguments are given to make.
default_target: all
.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/local/Cellar/cmake/3.2.2/bin/cmake

# The command to remove a file.
RM = /usr/local/Cellar/cmake/3.2.2/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /Users/lfereman/git2/lcif

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /Users/lfereman/git2/lcif

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake cache editor..."
	/usr/local/Cellar/cmake/3.2.2/bin/ccmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache
.PHONY : edit_cache/fast

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/local/Cellar/cmake/3.2.2/bin/cmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache
.PHONY : rebuild_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /Users/lfereman/git2/lcif/CMakeFiles /Users/lfereman/git2/lcif/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /Users/lfereman/git2/lcif/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean
.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named lcif

# Build rule for target.
lcif: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 lcif
.PHONY : lcif

# fast build rule for target.
lcif/fast:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/build
.PHONY : lcif/fast

src/Data.o: src/Data.cpp.o
.PHONY : src/Data.o

# target to build an object file
src/Data.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Data.cpp.o
.PHONY : src/Data.cpp.o

src/Data.i: src/Data.cpp.i
.PHONY : src/Data.i

# target to preprocess a source file
src/Data.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Data.cpp.i
.PHONY : src/Data.cpp.i

src/Data.s: src/Data.cpp.s
.PHONY : src/Data.s

# target to generate assembly for a file
src/Data.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Data.cpp.s
.PHONY : src/Data.cpp.s

src/Evaluator.o: src/Evaluator.cpp.o
.PHONY : src/Evaluator.o

# target to build an object file
src/Evaluator.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Evaluator.cpp.o
.PHONY : src/Evaluator.cpp.o

src/Evaluator.i: src/Evaluator.cpp.i
.PHONY : src/Evaluator.i

# target to preprocess a source file
src/Evaluator.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Evaluator.cpp.i
.PHONY : src/Evaluator.cpp.i

src/Evaluator.s: src/Evaluator.cpp.s
.PHONY : src/Evaluator.s

# target to generate assembly for a file
src/Evaluator.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Evaluator.cpp.s
.PHONY : src/Evaluator.cpp.s

src/Facade.o: src/Facade.cpp.o
.PHONY : src/Facade.o

# target to build an object file
src/Facade.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Facade.cpp.o
.PHONY : src/Facade.cpp.o

src/Facade.i: src/Facade.cpp.i
.PHONY : src/Facade.i

# target to preprocess a source file
src/Facade.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Facade.cpp.i
.PHONY : src/Facade.cpp.i

src/Facade.s: src/Facade.cpp.s
.PHONY : src/Facade.s

# target to generate assembly for a file
src/Facade.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Facade.cpp.s
.PHONY : src/Facade.cpp.s

src/FeatureKNN.o: src/FeatureKNN.cpp.o
.PHONY : src/FeatureKNN.o

# target to build an object file
src/FeatureKNN.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/FeatureKNN.cpp.o
.PHONY : src/FeatureKNN.cpp.o

src/FeatureKNN.i: src/FeatureKNN.cpp.i
.PHONY : src/FeatureKNN.i

# target to preprocess a source file
src/FeatureKNN.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/FeatureKNN.cpp.i
.PHONY : src/FeatureKNN.cpp.i

src/FeatureKNN.s: src/FeatureKNN.cpp.s
.PHONY : src/FeatureKNN.s

# target to generate assembly for a file
src/FeatureKNN.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/FeatureKNN.cpp.s
.PHONY : src/FeatureKNN.cpp.s

src/FeatureSelection.o: src/FeatureSelection.cpp.o
.PHONY : src/FeatureSelection.o

# target to build an object file
src/FeatureSelection.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/FeatureSelection.cpp.o
.PHONY : src/FeatureSelection.cpp.o

src/FeatureSelection.i: src/FeatureSelection.cpp.i
.PHONY : src/FeatureSelection.i

# target to preprocess a source file
src/FeatureSelection.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/FeatureSelection.cpp.i
.PHONY : src/FeatureSelection.cpp.i

src/FeatureSelection.s: src/FeatureSelection.cpp.s
.PHONY : src/FeatureSelection.s

# target to generate assembly for a file
src/FeatureSelection.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/FeatureSelection.cpp.s
.PHONY : src/FeatureSelection.cpp.s

src/FileTransformAddStatistics.o: src/FileTransformAddStatistics.cpp.o
.PHONY : src/FileTransformAddStatistics.o

# target to build an object file
src/FileTransformAddStatistics.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/FileTransformAddStatistics.cpp.o
.PHONY : src/FileTransformAddStatistics.cpp.o

src/FileTransformAddStatistics.i: src/FileTransformAddStatistics.cpp.i
.PHONY : src/FileTransformAddStatistics.i

# target to preprocess a source file
src/FileTransformAddStatistics.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/FileTransformAddStatistics.cpp.i
.PHONY : src/FileTransformAddStatistics.cpp.i

src/FileTransformAddStatistics.s: src/FileTransformAddStatistics.cpp.s
.PHONY : src/FileTransformAddStatistics.s

# target to generate assembly for a file
src/FileTransformAddStatistics.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/FileTransformAddStatistics.cpp.s
.PHONY : src/FileTransformAddStatistics.cpp.s

src/Gridsearch.o: src/Gridsearch.cpp.o
.PHONY : src/Gridsearch.o

# target to build an object file
src/Gridsearch.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Gridsearch.cpp.o
.PHONY : src/Gridsearch.cpp.o

src/Gridsearch.i: src/Gridsearch.cpp.i
.PHONY : src/Gridsearch.i

# target to preprocess a source file
src/Gridsearch.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Gridsearch.cpp.i
.PHONY : src/Gridsearch.cpp.i

src/Gridsearch.s: src/Gridsearch.cpp.s
.PHONY : src/Gridsearch.s

# target to generate assembly for a file
src/Gridsearch.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Gridsearch.cpp.s
.PHONY : src/Gridsearch.cpp.s

src/LCIF.o: src/LCIF.cpp.o
.PHONY : src/LCIF.o

# target to build an object file
src/LCIF.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/LCIF.cpp.o
.PHONY : src/LCIF.cpp.o

src/LCIF.i: src/LCIF.cpp.i
.PHONY : src/LCIF.i

# target to preprocess a source file
src/LCIF.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/LCIF.cpp.i
.PHONY : src/LCIF.cpp.i

src/LCIF.s: src/LCIF.cpp.s
.PHONY : src/LCIF.s

# target to generate assembly for a file
src/LCIF.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/LCIF.cpp.s
.PHONY : src/LCIF.cpp.s

src/MyMinHeap.o: src/MyMinHeap.cpp.o
.PHONY : src/MyMinHeap.o

# target to build an object file
src/MyMinHeap.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/MyMinHeap.cpp.o
.PHONY : src/MyMinHeap.cpp.o

src/MyMinHeap.i: src/MyMinHeap.cpp.i
.PHONY : src/MyMinHeap.i

# target to preprocess a source file
src/MyMinHeap.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/MyMinHeap.cpp.i
.PHONY : src/MyMinHeap.cpp.i

src/MyMinHeap.s: src/MyMinHeap.cpp.s
.PHONY : src/MyMinHeap.s

# target to generate assembly for a file
src/MyMinHeap.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/MyMinHeap.cpp.s
.PHONY : src/MyMinHeap.cpp.s

src/MyTimer.o: src/MyTimer.cpp.o
.PHONY : src/MyTimer.o

# target to build an object file
src/MyTimer.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/MyTimer.cpp.o
.PHONY : src/MyTimer.cpp.o

src/MyTimer.i: src/MyTimer.cpp.i
.PHONY : src/MyTimer.i

# target to preprocess a source file
src/MyTimer.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/MyTimer.cpp.i
.PHONY : src/MyTimer.cpp.i

src/MyTimer.s: src/MyTimer.cpp.s
.PHONY : src/MyTimer.s

# target to generate assembly for a file
src/MyTimer.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/MyTimer.cpp.s
.PHONY : src/MyTimer.cpp.s

src/RowKNN.o: src/RowKNN.cpp.o
.PHONY : src/RowKNN.o

# target to build an object file
src/RowKNN.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/RowKNN.cpp.o
.PHONY : src/RowKNN.cpp.o

src/RowKNN.i: src/RowKNN.cpp.i
.PHONY : src/RowKNN.i

# target to preprocess a source file
src/RowKNN.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/RowKNN.cpp.i
.PHONY : src/RowKNN.cpp.i

src/RowKNN.s: src/RowKNN.cpp.s
.PHONY : src/RowKNN.s

# target to generate assembly for a file
src/RowKNN.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/RowKNN.cpp.s
.PHONY : src/RowKNN.cpp.s

src/RowKNNPruning.o: src/RowKNNPruning.cpp.o
.PHONY : src/RowKNNPruning.o

# target to build an object file
src/RowKNNPruning.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/RowKNNPruning.cpp.o
.PHONY : src/RowKNNPruning.cpp.o

src/RowKNNPruning.i: src/RowKNNPruning.cpp.i
.PHONY : src/RowKNNPruning.i

# target to preprocess a source file
src/RowKNNPruning.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/RowKNNPruning.cpp.i
.PHONY : src/RowKNNPruning.cpp.i

src/RowKNNPruning.s: src/RowKNNPruning.cpp.s
.PHONY : src/RowKNNPruning.s

# target to generate assembly for a file
src/RowKNNPruning.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/RowKNNPruning.cpp.s
.PHONY : src/RowKNNPruning.cpp.s

src/RowPredictions.o: src/RowPredictions.cpp.o
.PHONY : src/RowPredictions.o

# target to build an object file
src/RowPredictions.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/RowPredictions.cpp.o
.PHONY : src/RowPredictions.cpp.o

src/RowPredictions.i: src/RowPredictions.cpp.i
.PHONY : src/RowPredictions.i

# target to preprocess a source file
src/RowPredictions.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/RowPredictions.cpp.i
.PHONY : src/RowPredictions.cpp.i

src/RowPredictions.s: src/RowPredictions.cpp.s
.PHONY : src/RowPredictions.s

# target to generate assembly for a file
src/RowPredictions.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/RowPredictions.cpp.s
.PHONY : src/RowPredictions.cpp.s

src/SingleThreshold.o: src/SingleThreshold.cpp.o
.PHONY : src/SingleThreshold.o

# target to build an object file
src/SingleThreshold.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/SingleThreshold.cpp.o
.PHONY : src/SingleThreshold.cpp.o

src/SingleThreshold.i: src/SingleThreshold.cpp.i
.PHONY : src/SingleThreshold.i

# target to preprocess a source file
src/SingleThreshold.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/SingleThreshold.cpp.i
.PHONY : src/SingleThreshold.cpp.i

src/SingleThreshold.s: src/SingleThreshold.cpp.s
.PHONY : src/SingleThreshold.s

# target to generate assembly for a file
src/SingleThreshold.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/SingleThreshold.cpp.s
.PHONY : src/SingleThreshold.cpp.s

src/Tests.o: src/Tests.cpp.o
.PHONY : src/Tests.o

# target to build an object file
src/Tests.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Tests.cpp.o
.PHONY : src/Tests.cpp.o

src/Tests.i: src/Tests.cpp.i
.PHONY : src/Tests.i

# target to preprocess a source file
src/Tests.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Tests.cpp.i
.PHONY : src/Tests.cpp.i

src/Tests.s: src/Tests.cpp.s
.PHONY : src/Tests.s

# target to generate assembly for a file
src/Tests.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Tests.cpp.s
.PHONY : src/Tests.cpp.s

src/Utils.o: src/Utils.cpp.o
.PHONY : src/Utils.o

# target to build an object file
src/Utils.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Utils.cpp.o
.PHONY : src/Utils.cpp.o

src/Utils.i: src/Utils.cpp.i
.PHONY : src/Utils.i

# target to preprocess a source file
src/Utils.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Utils.cpp.i
.PHONY : src/Utils.cpp.i

src/Utils.s: src/Utils.cpp.s
.PHONY : src/Utils.s

# target to generate assembly for a file
src/Utils.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/Utils.cpp.s
.PHONY : src/Utils.cpp.s

src/main.o: src/main.cpp.o
.PHONY : src/main.o

# target to build an object file
src/main.cpp.o:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/main.cpp.o
.PHONY : src/main.cpp.o

src/main.i: src/main.cpp.i
.PHONY : src/main.i

# target to preprocess a source file
src/main.cpp.i:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/main.cpp.i
.PHONY : src/main.cpp.i

src/main.s: src/main.cpp.s
.PHONY : src/main.s

# target to generate assembly for a file
src/main.cpp.s:
	$(MAKE) -f CMakeFiles/lcif.dir/build.make CMakeFiles/lcif.dir/src/main.cpp.s
.PHONY : src/main.cpp.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... lcif"
	@echo "... edit_cache"
	@echo "... rebuild_cache"
	@echo "... src/Data.o"
	@echo "... src/Data.i"
	@echo "... src/Data.s"
	@echo "... src/Evaluator.o"
	@echo "... src/Evaluator.i"
	@echo "... src/Evaluator.s"
	@echo "... src/Facade.o"
	@echo "... src/Facade.i"
	@echo "... src/Facade.s"
	@echo "... src/FeatureKNN.o"
	@echo "... src/FeatureKNN.i"
	@echo "... src/FeatureKNN.s"
	@echo "... src/FeatureSelection.o"
	@echo "... src/FeatureSelection.i"
	@echo "... src/FeatureSelection.s"
	@echo "... src/FileTransformAddStatistics.o"
	@echo "... src/FileTransformAddStatistics.i"
	@echo "... src/FileTransformAddStatistics.s"
	@echo "... src/Gridsearch.o"
	@echo "... src/Gridsearch.i"
	@echo "... src/Gridsearch.s"
	@echo "... src/LCIF.o"
	@echo "... src/LCIF.i"
	@echo "... src/LCIF.s"
	@echo "... src/MyMinHeap.o"
	@echo "... src/MyMinHeap.i"
	@echo "... src/MyMinHeap.s"
	@echo "... src/MyTimer.o"
	@echo "... src/MyTimer.i"
	@echo "... src/MyTimer.s"
	@echo "... src/RowKNN.o"
	@echo "... src/RowKNN.i"
	@echo "... src/RowKNN.s"
	@echo "... src/RowKNNPruning.o"
	@echo "... src/RowKNNPruning.i"
	@echo "... src/RowKNNPruning.s"
	@echo "... src/RowPredictions.o"
	@echo "... src/RowPredictions.i"
	@echo "... src/RowPredictions.s"
	@echo "... src/SingleThreshold.o"
	@echo "... src/SingleThreshold.i"
	@echo "... src/SingleThreshold.s"
	@echo "... src/Tests.o"
	@echo "... src/Tests.i"
	@echo "... src/Tests.s"
	@echo "... src/Utils.o"
	@echo "... src/Utils.i"
	@echo "... src/Utils.s"
	@echo "... src/main.o"
	@echo "... src/main.i"
	@echo "... src/main.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

