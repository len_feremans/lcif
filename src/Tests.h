//
// Created by Len Feremans on 18/10/2019.
//

#ifndef LCIF_KNN_JOURNAL_TESTS_H
#define LCIF_KNN_JOURNAL_TESTS_H

#include <iostream>
#include <vector>
#include <unordered_map>
#include <utility>
#include <string>
#include "Data.h"

using namespace std;

class Tests {

public:
    void run_tests(){
        test_data_load();
        test_data_index();
        test_data_feature_normalisation();
    }

    void test_data_load(){
        startTest("test_data_load");
        Data train_data("./data/unittest-train.libsvm", -1, true);
        Data test_data("./data/unittest-test.libsvm", -1, true);
        check(train_data.getNumberOfPoints() == 2, "train_data.getNumberOfPoints() == 2");
        check(train_data.getNumberOfFeatures() == 5, "train_data.getNumberOfFeatures() == 5");
        check(train_data.getNumberOfLabels() == 10, "train_data.getNumberOfLabels() == 10");
        unordered_map<int,float> features_hard_coded = {{1001,1.0f}, {1002,1.0f}, {1003,1.0f}, {1004,1.0f}};
        vector<int> labels_hard_coded = {2, 3, 4};
        check(train_data.getFeatures(1) == features_hard_coded, "train_data.getFeatures(1) == features_hard_coded");
        check(train_data.getLabels(1) == labels_hard_coded, "train_data.getLabels(1) == labels_hard_coded");
    }

    void test_data_index(){
        startTest("test_data_index");
        Data train_data("./data/unittest-train.libsvm", -1, true);
        train_data.makeIndex();
        train_data.printStatistics();
        Data test_data("./data/unittest-test.libsvm", -1, true);
        check(train_data.getLabelsInverted().size() == 4, "train_data.getLabelsInverted().size()");
        check(train_data.getFeaturesInverseMap().size() == 5, "train_data.getLabelsInverted().size()");
        printIndex(train_data);
        vector<int> labels_index_hard_coded = {0,1};
        vector<pair<int,float>> feature_index_hard_coded = {{0,1},{1,1}};
        check(train_data.getLabelsInverted().at(2) == labels_index_hard_coded, "train_data.getLabelsInverted().at(2) == labels_index_hard_coded");
        check(train_data.getFeaturesInverseMap().at(1001) == feature_index_hard_coded, "train_data.getFeaturesInverseMap().at(1001) == feature_index_hard_coded");
    }

    void test_data_feature_normalisation(){
        startTest("test_data_feature_normalisation");
        Data train_data("./data/unittest-train.libsvm", -1, true);
        train_data.makeIndex();
        Data test_data("./data/unittest-test.libsvm", -1, true);
        vector<float> normsInstances = train_data.normalize_rows(); //no change
        printData(train_data);
        check(train_data.getFeatures(1).at(1001) == 0.5, "train_data.getFeatures(1).at(1001) == 0.5"); //sqrt(1^2 + 1^2 + 1^2 + 1^2) = 2
        train_data.unnormalize_rows(normsInstances);
        unordered_map<int,float> norms = train_data.normalize_features();
        test_data.normalize_features(norms);
        printData(train_data);
        check(train_data.getFeatures(0).at(1001) - 1/sqrt(2) < 0.000001, "train_data.getFeatures(0).at(1001) - 1/sqrt(2) < 0.00000"); //sqrt(1^2 + 1^2) = sqrt(2), and 1/sqrt(2) = 0.707
        //1 2 3 <1001,0.707> <1002,0.447> <1003,0.707> <1005,1>
        //2 3 4 <1001,0.707> <1002,0.894> <1003,0.707> <1004,1>
    }

private:
    // --- util ---
    void printData(Data& data){
        cout << ">>data: " << endl;
        for(int i=0; i<data.getNumberOfPoints(); i++){
            for(int label: data.getLabels(i)){
                cout << label << " ";
            }
            for(pair<int,float> pair: data.getFeatures(i)){
                cout << "<" << pair.first << "," << pair.second << "> ";
            }
            cout << endl;
        }
    }

    void printIndex(Data& data){
        //print index
        cout << ">>index:" << endl;
        for(const auto& pair: data.getLabelsInverted()){
            cout << "label[" << pair.first << "]=";
            for(const auto& docId: pair.second){
                cout << docId << ", ";
            }
        }
        cout << endl;
        for(const auto& pair: data.getFeaturesInverseMap()){
            cout << "feature[" << pair.first << "]=";
            for(const auto& pair: pair.second){
                cout << "<" << pair.first << "," << pair.second << ">, ";
            }
        }
        cout << endl;
    }


    // --- unit testing ---
    void startTest(string msg){
        cout << "\033[1;32m>>Starting unit test: " << msg << "\033[0m" << endl;
    }
    void check(bool condition, string msg){
        if(!condition){
            cerr << "\031[1;32m" << msg << ": FAILED\033[0m" << endl;
        }
        else{
            cout << "\033[1;32m" <<  msg << ": PASSED\033[0m" << endl;
        }
    }
};

#endif //LCIF_KNN_JOURNAL_TESTS_H
