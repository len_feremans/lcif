//
// Created by Len Feremans on 14/10/2019.
//

#ifndef LCIF_KNN_JOURNAL_LCIF_H
#define LCIF_KNN_JOURNAL_LCIF_H

#include <vector>
#include <unordered_map>
#include <iostream>
#include <chrono>

#include "Utils.h"
#include "Data.h"

class LCIF{

public:

    //ook leuk
    vector<vector<pair<int,float>>> combinePredictions(float lambda, unsigned int storeK,
                                                       const vector<vector<pair<int,float>>>& predictionsRowKNN,
    const vector<vector<pair<int,float>>>& predictionsFeatureKNN){
        MyTimer timer("LCIFcombinePredictions");
        vector<vector<pair<int,float>>> predictionsCombined;
        predictionsCombined.resize(predictionsRowKNN.size());
        float oneMinusLambda = 1.0 - lambda;
        for(unsigned int test_uid=0; test_uid < predictionsRowKNN.size(); test_uid++){
            unordered_map<int,float> scores;
            scores.reserve(storeK);
            for(const auto& pair: predictionsRowKNN.at(test_uid)){
                scores[pair.first] = lambda * pair.second;
            }

            for(const auto& pair: predictionsFeatureKNN.at(test_uid)){
                scores[pair.first] += oneMinusLambda * pair.second;
            }
            predictionsCombined[test_uid] = sortTopKPartialOrder(scores, storeK);
        }
        timer.stop();
        return predictionsCombined;
    }
};


#endif //LCIF_KNN_JOURNAL_LCIF_H
