//
// Created by Len Feremans on 11/10/2019.
//

#ifndef LCIF_KNN_JOURNAL_EVALUATOR_H
#define LCIF_KNN_JOURNAL_EVALUATOR_H

#include <vector>
#include <set>
#include <unordered_map>
#include <exception>
#include <iostream>

#include "Data.h"
#include "Utils.h"

class Evaluator{

public:
    Evaluator(const Data& trainDataArg, const Data& testDataArg): trainData(trainDataArg), testData(testDataArg){}

    float precisionAtK(int k,  const vector<vector<pair<int, float>>>& predictions) const{
        float precisionAtKTotal = 0.0;
        for(unsigned int i=0; i<testData.getNumberOfPoints(); i++){
            float precisionAtKInstance = 0.0;
            //const unordered_map<int,float>&  features = testData.getFeatures(i);
            const vector<int>& labels = testData.getLabels(i);
            const vector<pair<int, float>>& predictionsInstance = predictions.at(i);
            for(unsigned int j=0; j<predictionsInstance.size() && j<k; j++){
                int labelPredicted = predictionsInstance[j].first;
                if(find(labels.begin(), labels.end(), labelPredicted) != labels.end()){
                    precisionAtKInstance+=1;
                }
            }
            precisionAtKInstance = precisionAtKInstance / (float)k;
            precisionAtKTotal += precisionAtKInstance;
        }
        return precisionAtKTotal / (float)testData.getNumberOfPoints();
    }

    float precisionAtKFP(int k,  const vector<vector<pair<int, float>>>& predictions) const{
        float precisionAtKTotal = 0.0;
        for(unsigned int i=0; i<testData.getNumberOfPoints(); i++){
            float precisionAtKInstance = 0.0;
            //const unordered_map<int,float>&  features = testData.getFeatures(i);
            const vector<int>& labels = testData.getLabels(i);
            const vector<pair<int, float>>& predictionsInstance = predictions.at(i);
            for(unsigned int j=0; j<predictionsInstance.size() && j<k; j++){
                int labelPredicted = predictionsInstance[j].first;
                if(find(labels.begin(), labels.end(), labelPredicted) != labels.end()){
                    precisionAtKInstance+=1;
                }
            }
            //precisionAtKInstance = precisionAtKInstance; //DO NOT DIVIDE / (float)k;
            precisionAtKTotal += precisionAtKInstance;
        }
        return precisionAtKTotal / (float)testData.getNumberOfPoints();
    }

    //Assumes single threshold is applied to predictions
    float exampleBasedAccuracy(const vector<vector<pair<int,float>>>& predictionsThresholded) const{
        float accTotal = 0.0;
        for(unsigned int i=0; i<testData.getNumberOfPoints(); i++){
            int overlap = 0;
            const vector<int>& labels = testData.getLabels(i);
            const vector<pair<int, float>>& predictionsInstance = predictionsThresholded.at(i);
            for(unsigned int j=0; j<predictionsInstance.size(); j++){
                int labelPredicted = predictionsInstance[j].first;
                if(find(labels.begin(), labels.end(), labelPredicted) != labels.end()){
                    overlap+=1;
                }
            }
            float accuracyInstance = overlap / (float)(labels.size() + predictionsInstance.size() - overlap);
            if(labels.size() + predictionsInstance.size() - overlap == 0){
                accuracyInstance = 1.0;
            }
            accTotal += accuracyInstance;
        }
        return accTotal / (float)testData.getNumberOfPoints();
    }

    //note: copy/pasted previous
    float exampleBasedF1(const vector<vector<pair<int,float>>>& predictionsThresholded) const{
        float f1Total = 0.0;
        for(unsigned int i=0; i<testData.getNumberOfPoints(); i++){
            int overlap = 0;
            const vector<int>& labels = testData.getLabels(i);
            const vector<pair<int, float>>& predictionsInstance = predictionsThresholded.at(i);
            for(unsigned int j=0; j<predictionsInstance.size(); j++){
                int labelPredicted = predictionsInstance[j].first;
                if(find(labels.begin(), labels.end(), labelPredicted) != labels.end()){
                    overlap+=1;
                }
            }
            float f1Instance = 2 * overlap / (float)(labels.size() + predictionsInstance.size());
            f1Total += f1Instance;
        }
        return f1Total / (float)testData.getNumberOfPoints();
    }

    //note: copy/pasted previous
    float hammingLoss(const vector<vector<pair<int,float>>>& predictionsThresholded) const{
        int noDistinctLabels = this->trainData.getNumberOfLabels();
        float hammingLossTotal = 0.0;
        for(unsigned int i=0; i<testData.getNumberOfPoints(); i++){
            int overlap = 0;
            const vector<int>& labels = testData.getLabels(i);
            const vector<pair<int, float>>& predictionsInstance = predictionsThresholded.at(i);
            for(unsigned int j=0; j<predictionsInstance.size(); j++){
                int labelPredicted = predictionsInstance[j].first;
                if(find(labels.begin(), labels.end(), labelPredicted) != labels.end()){
                    overlap+=1;
                }
            }
            float hammingLossInstance = (labels.size() + predictionsInstance.size() - 2 * overlap) / (float)noDistinctLabels;
            hammingLossTotal += hammingLossInstance;
        }
        return hammingLossTotal / (float)testData.getNumberOfPoints();
    }

    vector<float> computeMicroEvaluationStatistics(const vector<vector<pair<int,float>>>& predictionsThresholded) const{
        unordered_map<int, int> tp;
        unordered_map<int, int> fp;
        unordered_map<int, int> fn;
        computeConfusionMatrix(tp,fp,fn, predictionsThresholded);
        int tp_total = 0;
        int fp_total = 0;
        int fn_total = 0;
        for(const auto& pair: tp){
            tp_total += pair.second;
        }
        for(const auto& pair: fp){
            fp_total += pair.second;
        }
        for(const auto& pair: fn){
            fn_total += pair.second;
        }
        float prec = tp_total / (float)(tp_total + fp_total);
        float recall = tp_total / (float)(tp_total + fn_total);
        float microF1= 2 * prec * recall / (prec + recall);
        if(prec + recall== 0.0){
            microF1 = 0.0;
        }
        return {microF1, prec, recall};
    }

    float microF1(const vector<vector<pair<int,float>>>& predictionsThresholded) const{
        return computeMicroEvaluationStatistics(predictionsThresholded)[0];
    }


    float precision(const vector<vector<pair<int,float>>>& predictionsThresholded) const{
        return computeMicroEvaluationStatistics(predictionsThresholded)[1];
    }

    float recall(const vector<vector<pair<int,float>>>& predictionsThresholded) const {
        return computeMicroEvaluationStatistics(predictionsThresholded)[2];
    }

    //Remark: classic definitions of macroF1 loop over ALL labels... if label does not occur in any test instance or any predictions... p
    //precision/recall will be 1.0
    //here: skip over labels that do not occur in training
    //posible todo: remove labels only occurring in test data?
    float macroF1_2(const vector<vector<pair<int,float>>>& predictionsThresholded) const{
        set<int> all_labels;
        for(const auto& pair: this->trainData.getLabelsInverted()){
            all_labels.insert(pair.first);
        }
        unordered_map<int, int> tp;
        unordered_map<int, int> fp;
        unordered_map<int, int> fn;
        computeConfusionMatrix(tp,fp,fn, predictionsThresholded);
        float totalMacroF1 = 0.0;
        int labelsNotOccurring = 0;
        for(int label: all_labels){
            int tp_label = tp[label];
            int fp_label = fp[label];
            int fn_label = fn[label];
            float precision = tp_label / (float)(tp_label + fp_label);
            if(tp_label + fp_label == 0){
                precision = 0.0; //never predicted
            }
            float recall = tp_label / (float)(tp_label + fn_label);
            if(tp_label + fn_label == 0) {
                labelsNotOccurring +=1;
                continue;
            }
            float f1= 2 * precision * recall / (precision + recall);
            if(precision + recall == 0.0){
                f1 = 0.0;
            }
            if(tp_label + fp_label + fn_label == 0.0){ //does not occur in test data
                labelsNotOccurring +=1;
                continue;
            }
            totalMacroF1 += f1;
            //cout << "label tp" << tp_label  << " fp" << fp_label << " fn" << fn_label << " prec" << precision << " recall" << recall << "f1" << f1 << endl;
        }
        float macroF1 = totalMacroF1 / (double)(all_labels.size() - labelsNotOccurring);
        return macroF1;
    }

    float macroF1(const vector<vector<pair<int,float>>>& predictionsThresholded) const{
        set<int> all_labels;
        for(const auto& pair: this->trainData.getLabelsInverted()){
            all_labels.insert(pair.first);
        }
        unordered_map<int, int> tp;
        unordered_map<int, int> fp;
        unordered_map<int, int> fn;
        computeConfusionMatrix(tp,fp,fn, predictionsThresholded);
        //Remark: loop over ALL labels... if label does not occur in any test instance or any predictions... precision/recall will be 1.0
        float totalMacroF1 = 0.0;
        for(int label: all_labels){
            int tp_label = tp[label];
            int fp_label = fp[label];
            int fn_label = fn[label];
            float precision = tp_label / (float)(tp_label + fp_label);
            if(tp_label + fp_label == 0){
                precision = 0.0; //never predicted
            }
            float recall = tp_label / (float)(tp_label + fn_label);
            if(tp_label + fn_label == 0) {
                recall = 1.0; // does not occur
            }
            float f1= 2 * precision * recall / (precision + recall);
            if(precision + recall == 0.0){
                f1 = 0.0;
            }
            if(tp_label + fp_label + fn_label == 0.0){ //does not occur in test data
                f1 = 1.0;
            }
            totalMacroF1 += f1;
            //cout << "label tp" << tp_label  << " fp" << fp_label << " fn" << fn_label << " prec" << precision << " recall" << recall << "f1" << f1 << endl;
        }
        float macroF1 = totalMacroF1 / (double)all_labels.size();
        return macroF1;
    }

    float evaluateMetric(string metric, const vector<vector<pair<int,float>>>& predictionsThresholded){
        vector<string> metrics = {"precision@1", "precision@3", "precision@5", "exampleBasedAccuracy", "exampleBasedF1",
                                  "hammingLoss", "microF1", "macroF1", "macroF1_2", "precision", "recall"};
        if(find(metrics.begin(), metrics.end(), metric) == metrics.end()){
            throw invalid_argument("Error: evaluateMetric");
        }
        if(metric == "exampleBasedAccuracy"){
            return exampleBasedAccuracy(predictionsThresholded);
        }
        else if(metric == "exampleBasedF1"){
            return exampleBasedF1(predictionsThresholded);
        }
        else if(metric == "hammingLoss"){
            return hammingLoss(predictionsThresholded);
        }
        else if(metric == "microF1"){
            return microF1(predictionsThresholded);
        }
        else if(metric == "macroF1"){
            return macroF1(predictionsThresholded);
        }
        else if(metric == "macroF1_2"){
            return macroF1_2(predictionsThresholded);
        }
        else if(metric == "precision"){
            return precision(predictionsThresholded);
        }
        else if(metric == "recall"){
            return recall(predictionsThresholded);
        }
        else if(metric == "precision@1"){
            return precisionAtK(1,predictionsThresholded);
        }
        else if(metric == "precision@3"){
            return precisionAtK(3,predictionsThresholded);
        }
        else if(metric == "precision@5"){
            return precisionAtK(5,predictionsThresholded);
        }
        else{
            throw invalid_argument("Error: evaluateMetric");
        }
    }


    /*
     * From: https://www.javatips.net/api/Mulan-master/mulan/src/mulan/evaluation/measure/InformationRetrievalMeasures.java
    public static  double fMeasure(double tp, double fp, double fn, double beta) {
        if (tp + fp + fn == 0) {
            return 1;
        }
        double beta2 = beta * beta;
        return ((beta2 + 1) * tp) / ((beta2 + 1) * tp + beta2 * fn + fp);
    }
     */



private:
    //for each label, store true positives, false positives and false negatives
    void computeConfusionMatrix(unordered_map<int, int>& tp, unordered_map<int, int>& fp, unordered_map<int, int>& fn,
                                const vector<vector<pair<int,float>>>& predictionsThresholded) const{
        for(unsigned int i=0; i<testData.getNumberOfPoints(); i++) {
            const vector<int> &labels = testData.getLabels(i);
            const vector<pair<int, float>> &predictionsInstance = predictionsThresholded.at(i);
            for (unsigned int j = 0; j < predictionsInstance.size(); j++) {
                int labelPredicted = predictionsInstance[j].first;
                if (find(labels.begin(), labels.end(), labelPredicted) != labels.end()) {
                    tp[labelPredicted] += 1;
                } else {
                    fp[labelPredicted] += 1;
                }
            }
            for (unsigned int j = 0; j < labels.size(); j++) {
                int label = labels[j];
                bool found = false;
                for (const auto &pair: predictionsInstance) {
                    if (pair.first == label) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    fn[label] += 1;
                }
            }
        }
    }

    //Data
    const Data& trainData; //used for compute some statistics, such as numberOfLabels or the set of all labels (in the training part)
    const Data& testData;
};


#endif //LCIF_KNN_JOURNAL_EVALUATOR_H
