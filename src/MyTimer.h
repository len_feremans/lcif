//
// Created by Len Feremans on 24/09/2018.
//

#ifndef LCIF_KNN_JOURNAL_TIMER_H
#define LCIF_KNN_JOURNAL_TIMER_H

#include <string>
#include <iostream>
#include <chrono>

using namespace std;

class MyTimer {
public:
    MyTimer(string msg): msg(msg){
        start = chrono::steady_clock::now();
    }

    void progress(string msg){
        chrono::steady_clock::time_point end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
        cout << "\033[0;37m" << ">>" << msg << " elapsed " << elapsed.count() << "ms" << "\033[0m" << endl;
    }

    long stop(){
        chrono::steady_clock::time_point end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
        cout << "\033[0;37m"  << ">>" << msg << " took " << elapsed.count() << "ms" << "\033[0m" << endl;
        return elapsed.count();
    }

    void reset(string msg){
        start = chrono::steady_clock::now();
        this->msg = msg;
    }
private:
    string msg;
    chrono::steady_clock::time_point start;
};


#endif //LCIF_KNN_JOURNAL_TIMER_H
