#include <vector>
#include <queue>
#include <set>
#include <unordered_map>

#include <algorithm>
#include <functional>
#include <iterator>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

#include <numeric>
#include <utility>
#include <math.h>
#include <chrono>

#include "Utils.h"
#include "Data.h"
#include "MyTimer.h"
#include "MyMinHeap.h"

#include "FeatureKNN.h"
#include "RowKNN.h"
#include "RowKNNPruning.h"
#include "RowPredictions.h"
#include "FeatureSelection.h"
#include "Evaluator.h"
#include "SingleThreshold.h"
#include "LCIF.h"
#include "Facade.h"
#include "Gridsearch.h"
#include "Tests.h"
#include "FileTransformAddStatistics.h"

using namespace std;

int ROW_KNN_PRUNING_ON = 1;
int ROW_KNN_PRUNING_OFF = 0;

int run_lcif_GS(string filenameTrain, string filenameTest, string metricToOptimizeOn) {
    int folds = 10;
    cout << "---------- Grid search ------------- " << endl;
    Gridsearch gs;
    ParameterLCIF best = gs.search_best_parameters_folded(filenameTrain, metricToOptimizeOn, folds);
    cout << "---------- Final result ------------- " << endl;
    Facade facade;
    facade.run_lcif(filenameTrain, filenameTest,
             best.lambda, best.kRow, best.kRowPower, best.kFeature, best.kFeaturePower,
             metricToOptimizeOn, -1, true);
    return 0;
}

//for big dataset do not use cross-validation
int run_lcif_GS_BIG(string filenameTrain, string filenameTest, string metricToOptimizeOn) {
    cout << "---------- Grid search ------------- " << endl;

    //load data
    int sample_test = 100;
    Data train = Data(filenameTrain);
    train.makeIndex();
    Data test = Data(filenameTest);
    //set sample test for gridsearch adaptively
    if(test.getNumberOfPoints() > 1000){
        sample_test = 1000;
    }
    if(test.getNumberOfPoints() > 10000){
        sample_test = 10000;
    }

    //1) find optimal representation
    FeatureSelection fs;
    int optimal_k_select = fs.search_best_representation(filenameTrain, filenameTest, metricToOptimizeOn, sample_test);

    //2) transform data
    MyTimer timer("GS_BIG");
    unordered_map<int,float> feature_scores = fs.computeEntropyAllFeatures(train);
    vector<pair<int, float>>  topKFeatures = fs.computeTopK(feature_scores, optimal_k_select);
    string train_output = filenameTrain + "_top" + to_string(optimal_k_select) + ".csv";
    string test_output = filenameTest + "_top" + to_string(optimal_k_select) + ".csv";
    fs.transformDataKeepTopKFeatures(train, topKFeatures, train_output);
    fs.transformDataKeepTopKFeatures(test, topKFeatures, test_output);

    //3) find optimal paramters
    Gridsearch gs;
    gs.verbose = true;
    ParameterLCIF best = gs.search_best_parameters(train_output, test_output, metricToOptimizeOn,sample_test);
    timer.stop();

    //4) run outer loop: run optimal parameters on optimal representation
    Facade facade;
    facade.run_lcif(train_output, test_output,
             best.lambda, best.kRow, best.kRowPower, best.kFeature, best.kFeaturePower,
             metricToOptimizeOn, -1, true);
    return 0;
}

vector<long> run_experiment_pruning_performance(string filenameTrain, string filenameTest, int test_sample_size){

	int noThreads = 64; // 32;
    int topK = 100;

	//1) Load data
    Data train = Data(filenameTrain);
    train.makeIndex();
    train.printStatistics();
    Data test = Data(filenameTest, test_sample_size);

    //2) Prepare data for row knn
    train.normalize_rows();
    test.normalize_rows();

    vector<long> elapsedAll;
    //3) Run PARTITION (or RowKNNPruning) for varyingTopM
    // Note that if topM=0 -> Same as DAAT with WAND pruning, as TAAT index will be empty
    //           if topM is very large -> same as TAAT, as DAAT index will be empty
    vector<int> topMOptions = {1,20,100,500,1000,10000,0};
    for(int topM: topMOptions){
        cout << "*******************************************" << endl;
        cout << "TOP_M= " << topM << endl;
        MyTimer myTimer0("PARTITION indexing");
        //4.1) Run indexing
        RowKNNPruning rowKNNPruning(train,test);
        rowKNNPruning.verbose = false;
        rowKNNPruning.runTAATPreProcessing(topM);
        rowKNNPruning.runDAATPreProcessing();
        rowKNNPruning.setParameters(topK, false);
        myTimer0.stop();
        //4.2) Run simmilarity
        MyTimer myTimer("PARTITION simil");
        rowKNNPruning.computeSimilarityMatrix(noThreads);
        long elapsed = myTimer.stop();
        cout << "\033[1;30m" << "Elapsed topM=" << topM << ": " << elapsed << " ms ("
             << setprecision(3) << fixed << elapsed/(float)test_sample_size << " ms/sample) " << "\033[0m" << endl;
        elapsedAll.push_back(elapsed);
    }
    cout << "\033[1;30m" << "Elapsed ms/sample: topM={1,5,10,15,20,100,250,500,1000},TAAT,WAND";
    for(long elapsed: elapsedAll){
        cout << setprecision(6) << fixed << elapsed/(float)test_sample_size << " ";
    }
    cout << endl;
    return elapsedAll;
}

//report numberOfCandidates
void run_experiment_pruning_performance2(string filenameTrain, string filenameTest, int test_sample_size){
    int noThreads = 1; // 32;
    int topK = 100;
    //1) Load data
    Data train = Data(filenameTrain);
    train.makeIndex();
    train.printStatistics();
    Data test = Data(filenameTest, test_sample_size);
    //2) Prepare data for row knn
    train.normalize_rows();
    test.normalize_rows();
    //3) Run PARTITION (or RowKNNPruning) for varyingTopM
    vector<int> topMOptions = {1,5,10,15,20,25,100,10000,0};
    vector<float> numberOfCandidates;
    float minNumberOfCandidates = 100000000;
    int bestM = -1;
    for(int topM: topMOptions){
        cout << "*******************************************" << endl;
        cout << "TOP_M= " << topM << endl;
        RowKNNPruning rowKNNPruning(train,test);
        rowKNNPruning.verbose = true;
        rowKNNPruning.runTAATPreProcessing(topM);
        rowKNNPruning.runDAATPreProcessing();
        rowKNNPruning.setParameters(topK, false);
        rowKNNPruning.computeSimilarityMatrix(noThreads); //also print stats on pruning
        rowKNNPruning.printPruningStats();
        float nrOfCandidates = rowKNNPruning.getAverageNumberOfCandidates();
        numberOfCandidates.push_back(nrOfCandidates);
        if(nrOfCandidates < minNumberOfCandidates){
            minNumberOfCandidates  = nrOfCandidates;
            bestM = topM;
        }
    }
    cout << "Pruning experiments: nr of candidates " << endl
         << " Taat: " << setprecision(0) << numberOfCandidates[numberOfCandidates.size()-2]
                      << "(" << setprecision(3)  << numberOfCandidates[numberOfCandidates.size()-2]/(float)train.getNumberOfPoints() << ")"  << endl
         << " M-Wand: " << setprecision(0) << numberOfCandidates[numberOfCandidates.size()-1]
                      << "(" << setprecision(3)  << numberOfCandidates[numberOfCandidates.size()-1]/(float)train.getNumberOfPoints() << ")"  << endl
         << " FastKnn: " << setprecision(0) << minNumberOfCandidates << " M=" << bestM
                      << " (" << setprecision(3)  << minNumberOfCandidates/(float)train.getNumberOfPoints() << ")"  << endl;
    numberOfCandidates.erase(numberOfCandidates.end()-1);
    numberOfCandidates.erase(numberOfCandidates.end()-1);
    int indexMaxElement = distance(numberOfCandidates.begin(), min_element(numberOfCandidates.begin(), numberOfCandidates.end()));
    cout << " FastKNN (M>0)" << setprecision(0 ) << numberOfCandidates[indexMaxElement] << " M=" << topMOptions[indexMaxElement]
         << " (" << setprecision(3)  << numberOfCandidates[indexMaxElement]/(float)train.getNumberOfPoints() << ")"  << endl;
    return;
}

int main(int argc, char *argv[]) {
    if(argc < 2){
        cout << "LCIF: Expected first parameter to be either FS (feature selection), GS/GS_BIG (grid search), PRUNING_PERF or UNITTEST." << endl;
        return -1;
    }
    if(string(argv[1]) == "UNITTEST") {
        Tests tests;
        tests.run_tests();
        return 0;
    }
    if(string(argv[1]) == "FS") {
        if(argc != 6) {
            cout << "LCIF Feature selection: expecting argument: \n"
                    "0) FS\n"
                    "1) topK\n"
                    "2) Filename Train data file in libsvm format (starting at line 1)\n"
                    "3) Filename Test data file\n"
                    "4) Filename label data file (or \"none\")" << endl;
        }
        int topK = stoi(argv[2]);
        string filenameTrain = string(argv[3]);
        string filenameTest = string(argv[4]);
        string filenameLabels = string(argv[5]);
        Data train = Data(filenameTrain);
        train.makeIndex();
        FeatureSelection featureSelection;
        unordered_map<int,float> feature_scores = featureSelection.computeEntropyAllFeatures(train);
        vector<pair<int,float>> topKFeatures = featureSelection.computeTopK(feature_scores, topK);
        //read labels, and show labels top-10 best features
        vector<string> labels;
        if(filenameLabels != "none") {
            ifstream myfile(filenameLabels);
            if (myfile.is_open()) {
                string line;
                while (getline(myfile, line)) {
                    labels.push_back(line);
                }
                myfile.close();
            } else cout << "Unable to open labels file";
            //show features
            cout << "top 25 features" << endl;
            int k = 0;
            for (const auto &pair: topKFeatures) {
                cout << labels[pair.first - 1] << " (" << pair.first << " )" << pair.second << endl;
                if (k++ > 25)
                    break;
            }
        }
        featureSelection.transformDataKeepTopKFeatures(train, topKFeatures, filenameTrain + "_top" + to_string(topK) + ".csv");
        Data test = Data(filenameTest);
        featureSelection.transformDataKeepTopKFeatures(test, topKFeatures, filenameTest + "_top" + to_string(topK) + ".csv");
    }
    if(string(argv[1]) == "HEADER"){
        if(argc != 3){
            cout << "LCIF adding header to file with nrOfInstances, nrOfFeatures and nrOfLabels: \n"
                    "1) HEADER\n"
                    "2) Filename Train or test data file in libsvm format (starting at line 1)\n" << endl;
            return -1;
        }
        string filenameTrain = string(argv[2]);
        FileTransformAddStatistics ft;
        ft.transform(filenameTrain);
    }
    if(string(argv[1]) == "INFO"){
        if(argc != 4){
            cout << "INFO\n"
                    "1) Filename Train file in libsvm format (starting at line 1)\n" <<
                    "2) Filename Test file in libsvm format (starting at line 1)" << endl;
            return -1;
        }
        string filenameTrain = string(argv[2]);
        string filenameTest = string(argv[3]);
        Data dataTrain(filenameTrain, -1, true);
        Data dataTest(filenameTest, -1, true);
        dataTrain.join(dataTest);
        dataTrain.printStatistics();
        cout << endl;
        return 0;
    }
    if(string(argv[1]) == "GS" || string(argv[1]) == "GS_BIG"){
        if(argc != 5){
            cout << "LCIF Gridsearch: expecting argument: \n"
                    "1) GS/GS_BIG\n"
                    "2) Metric to optimise on\n"
                    "3) Filename Train data file in libsvm format (starting at line 1)\n"
                    "4) Filename Test data file" << endl;
            return -1;
        }
        string metric = string(argv[2]);
        vector<string> metrics = {"precision@1", "precision@3", "precision@5", "exampleBasedAccuracy", "exampleBasedF1",
                                  "hammingLoss", "microF1", "macroF1", "macroF1_2", "precision", "recall"};
        if(find(metrics.begin(), metrics.end(), metric) == metrics.end()){
            cerr << "Metric not in precision@1, precision@3, precision@5, exampleBasedAccuracy, exampleBasedF1,"
                 << "hammingLoss, microF1, macroF1, precision, recall" << endl;
            return -1;
        }
        string filenameTrain = string(argv[3]);
        string filenameTest = string(argv[4]);
        if(string(argv[1]) == "GS")
            run_lcif_GS(filenameTrain, filenameTest, metric);
        else{
            run_lcif_GS_BIG(filenameTrain, filenameTest, metric);
        }
    }
    else if(string(argv[1]) == "PRUNING"){
        if(argc != 4){
            cout << "LCIF Pruning experiment FastInstanceKnn: expecting argument: \n"
                    "1) PRUNING_PERF\n"
                    "2) Filename Train data file in libsvm format (starting at line 1)\n"
                    "3) Filename Test data file" << endl;
            return -1;
        }
        int test_sample_size=1000;
        string filenameTrain = string(argv[2]);
        string filenameTest = string(argv[3]);
        run_experiment_pruning_performance2(filenameTrain, filenameTest, test_sample_size);
    }
    else if(string(argv[1]) == "PRUNING_PERF"){
        if(argc != 4){
            cout << "LCIF Performance experiment FastInstanceKnn: expecting argument: \n"
                    "1) PRUNING_PERF\n"
                    "2) Filename Train data file in libsvm format (starting at line 1)\n"
                    "3) Filename Test data file" << endl;
            return -1;
        }
        int test_sample_size=1000;
        string filenameTrain = string(argv[2]);
        string filenameTest = string(argv[3]);
        int repeat=10;
        vector<vector<long>> elapsedRuns;
        for(int i=0; i<repeat;i++) {
            vector<long> elapsed = run_experiment_pruning_performance(filenameTrain, filenameTest, test_sample_size);
            elapsedRuns.push_back(elapsed);
        }
        //report average en std
        cout << "\033[1;30m" << "Elapsed time prunning experiment " << filenameTrain << " averaged over 10 runs: " << endl;
        for(int i=0; i<elapsedRuns[0].size(); i++){
            float avg = 0.0f;
            for(int j=0; j<elapsedRuns.size(); j++){
                avg += elapsedRuns[j][i];
            }
            avg /= (float)elapsedRuns.size();
            float std = 0.0f;
            for(int j=0; j<elapsedRuns.size(); j++){
                std += pow(elapsedRuns[j][i] - avg,2);
            }
            std = sqrt(std/(float)elapsedRuns.size());
            cout << fixed << setprecision(3) << avg/1000.0f
                    << ("+-") << fixed << setprecision(3) << std/1000.0f << " ";
        }
        cout << "\033[0m" << endl;
    }

    return 0;
}
