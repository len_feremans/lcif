//
// Created by Len Feremans on 24/09/2018.
//

#ifndef LCIF_KNN_JOURNAL_ROWKNNPRUNING_H
#define LCIF_KNN_JOURNAL_ROWKNNPRUNING_H

#include <vector>
#include <unordered_map>
#include <unordered_set>

#include <algorithm>
#include <functional>

#include <iostream>

#include <numeric>
#include <utility>
#include <math.h>
#include <chrono>

#include <thread>

#include "Data.h"
#include "Utils.h"
#include "MyMinHeap.h"
#include "MyTimer.h"

using namespace std;


class RowKNNPruning{

public:
    bool verbose = true;

    RowKNNPruning(Data& trainData, Data& testData): trainData(trainData), testData(testData){
    }

    void setParameters(unsigned int topK, bool onlyTAAT){
        this->topK = topK;
        this->onlyTAAT = onlyTAAT;
    }
    /**
     * Create index for TAAT.
     *
     * For fast TAAT (term-at-a-time) to get approximate highest neighbours, sort each document on feature-value,
     * and maintain top-K index, where possible choice is K = sqrt(N), also maintain the hightest possible value
     * for each dimension.
     *
     * Do not sort dimensions on density, as not important.
     *
     * In addition: for top-K UIDS, store ALL feature values in any dimension in "complete" index
     */
    void runTAATPreProcessing(unsigned int topKIndex){
        auto start = chrono::steady_clock::now();
        taatIndex.reserve(trainData.getNumberOfFeatures());
        for(const auto& postingListIterator: trainData.getFeaturesInverseMap()){
            int postingListFeature = postingListIterator.first;
            if(topKIndex != 0){
                vector<pair<int,float>> postingList = postingListIterator.second; //make copy of a list of all matching "documents" together with there value for the feature
                //cout << "all values: " << to_string(postingListWithValues) << endl;
                vector<pair<int,float>> sortedTopK = sortTopKPartialOrder(postingList, topKIndex);
                taatIndex[postingListFeature] = sortedTopK;
            }
            else{
                taatIndex[postingListFeature] = {};
            }
        }
        //step 2: make TAAT index complete
        taatIndexComplete.reserve(trainData.getNumberOfFeatures());
        taatUIDS.reserve(1000);
        for(const auto& postingListIterator: taatIndex){
            const vector<pair<int,float>>& postingList = postingListIterator.second;
            for(const auto& postingListPair: postingList) {
                taatUIDS.insert(postingListPair.first);
            }
        }
        for(int trainUID: taatUIDS) {
            //for each feature, check if value higher than lowest value in in TOP-K index, if not, add to back
            const unordered_map<int, float> &trainFeatures = trainData.getFeatures(trainUID);
            for (const auto &featurePair: trainFeatures) {
                int feature = featurePair.first;
                float value = featurePair.second;
                vector<pair<int, float>> &postingList = taatIndex.at(feature);
                float minValueInIndex = postingList.back().second;
                //cout << value << "<?" << minValueInIndex << endl;
                if (value < minValueInIndex) {
                    taatIndexComplete[feature].emplace_back(make_pair(trainUID, value));
                    //cout << "x" << endl;
                }
            }
        }
        //debug: print index sizes
        if(verbose)
            cout <<  "\033[0;37m" << "TAAT Index: " <<  fixed << setprecision(6) << (float)taatUIDS.size() / (float)trainData.getNumberOfPoints()
                 << "(" << taatUIDS.size() << "/" << trainData.getNumberOfPoints() << ")" << "\033[0m" << endl;
        /*
        double factorTopK = 0.0;
        double factorTopKComplete = 0.0;
        for(const auto& indexPair: trainData.getFeaturesInverseMap()){
            int feature = indexPair.first;
            unsigned long length = indexPair.second.size();
            unsigned long lengthTop = taatIndex.find(feature) != taatIndex.end()?
									  taatIndex.at(feature).size():0;
            unsigned long lengthTopComplete = taatIndexComplete.find(feature) != taatIndexComplete.end()?
											  taatIndexComplete.at(feature).size():0;
            factorTopK += lengthTop / (float)length;
            factorTopKComplete += lengthTopComplete / (float)length;
        }
        factorTopK /=  (float)trainData.getNumberOfFeatures();
        factorTopKComplete /=  (float)trainData.getNumberOfFeatures();
        if(verbose)
            cout <<  "\033[0;37m" << setprecision(10) << "TAAT Index compared to full index: " << factorTopK
             << " complete piece: " << factorTopKComplete <<  "\033[0m" << endl;
        */
        //int firstFeature =   trainData.getFeaturesInverseMap().begin()->first;
        //cout << "first training row " << to_string(trainData.getFeatures(0)) << endl;
        //cout << "feature << " << firstFeature <<endl;
        //cout << "posting list: " << to_string(trainData.getFeaturesInverseMap().at(firstFeature)) << endl;
        //cout << "posting list top K: " << to_string(taatIndex[firstFeature]) << endl;
        //cout << "posting list complete top K: " << to_string(taatIndexComplete[firstFeature]) << endl;
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
        if(verbose)
            cout << "\033[0;37m" << ">>RowKNNPruning.runTAATPreProcessing took " << elapsed.count() << "ms" << "\033[0m" << endl;
    }

    /**
     * For fast DAAT (document at-a-a-time), first remove, in each posting list top-k documents from TAAT index, then sort DAAT index on
     * document-id. During search traverse documents parallel in document order and do pruning based on memory-based version of weak-and.
     *
     * Not that this idea is similar, as in hybrid of TAAT+m-wand as in Evaluation Strategies for Top-k Queries over Memory-Resident Inverted Indexes,
     * by Fontoura et al (google), but better pruning since upper-bound is tighter, as not based on max(posting-list), but highest-after removing all top-k
     * largest values from TAAT index.
     */
    void runDAATPreProcessing(){
        auto start = chrono::steady_clock::now();
        daatIndex.reserve(trainData.getNumberOfFeatures());
        for(const auto& postingListIterator: trainData.getFeaturesInverseMap()) {
            int postingListFeature = postingListIterator.first;
            const vector<pair<int,float>> &postingList = postingListIterator.second;
            //filter rows from postingList that are in TAAT index
            vector<pair<int, float>> postingListWithValues;
            postingListWithValues.reserve(postingList.size());
            for (const auto& documentValuePair: postingList) {
                int trainUID = documentValuePair.first;
                float trainFeatureValue = documentValuePair.second;
                if (taatUIDS.find(trainUID) == taatUIDS.end()) { //if not in TAAT index
                    postingListWithValues.emplace_back(make_pair(trainUID, trainFeatureValue));
                }
            }
            daatIndex[postingListFeature] = postingListWithValues; //input is already sorted on documented-id, so no sorting needed
        }
        int firstFeature =   trainData.getFeaturesInverseMap().begin()->first;
        //if(verbose) {
        //    cout << "feature << " << firstFeature << endl;
        //    cout << "posting list: " << to_string(trainData.getFeaturesInverseMap().at(firstFeature)) << endl;
        //    cout << "posting list TAAT " << to_string(taatIndex[firstFeature]) << endl;
        //    cout << "posting list TAAT complete: " << to_string(taatIndexComplete[firstFeature]) << endl;
        //    cout << "posting list DAAT: " << to_string(daatIndex[firstFeature]) << endl;
        //}
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
        if(verbose)
            cout << "\033[0;37m" << ">>RowKNNPruning.runDAATPreProcessing took " << elapsed.count() << "ms" <<  "\033[0m" << endl;
    }

    void debugKNNTrainInstance(int train_uid, int test_uid){
        cout << "Features " << train_uid << ": " << to_string(trainData.getFeatures(train_uid)) << endl;
        cout << "In taat index? " << (taatUIDS.find(train_uid) != taatUIDS.end()) << endl;
        cout << "Simil: ";
        float simil = 0.0;
        for(const auto& featurePair: testData.getFeatures(test_uid)){
            if(trainData.getFeatures(train_uid).find(featurePair.first) != trainData.getFeatures(train_uid).end()) {
                float term = featurePair.second  * trainData.getFeatures(train_uid).at(featurePair.first);
                cout << term << "(" << featurePair.first << ") + ";
                simil += term;
            }
        }
        cout << " = " << simil << endl;
        cout << "In posting lists DAAT?" << endl;
        for(const auto& featurePair: testData.getFeatures(test_uid)){
            if(trainData.getFeatures(train_uid).find(featurePair.first) != trainData.getFeatures(train_uid).end()){
                cout << " Feature " << featurePair.first;
                int idx = 0;
                int foundIdx = -1;
                for(auto& postingListPair: daatIndex.at(featurePair.first)){
                    if(postingListPair.first == train_uid){
                        foundIdx = idx;
                    }
                    idx++;
                }
                if(foundIdx == -1){
                    cout << " not in DAAT index" <<endl;
                }
                else {
                    cout << " Posting list index:  " << foundIdx
                         << " size postingList: " << daatIndex[featurePair.first].size();
                    cout << " max-value pruning: " << taatIndex[featurePair.first].back().second;
                    cout << " value: " << daatIndex[featurePair.first][foundIdx].second;
                }
                cout << endl;
            }
            else{
                cout << " Feature " << featurePair.first;
                cout << " Posting list index:  " << -1
                     << " size postingList: " << daatIndex[featurePair.first].size();
                cout << " max-value pruning: " << taatIndex[featurePair.first].back().second;
                cout << endl;
            }
        }
    }


    /**
     * @param test_uid
     */
    void computeSimilarityInstanceTAAT(int test_uid){
        //MyTimer timer("computeSimilarityInstanceTAAT(" + to_string(test_uid) + ")");
        const unordered_map<int,float>& featuresTest = testData.getFeatures(test_uid);
        unordered_map<int,float> similsTestInstance;
        similsTestInstance.reserve(taatUIDS.size());
        for(const auto& featureValuePair: featuresTest){
            int featureTest = featureValuePair.first;
            float featureValueTest = featureValuePair.second;
            if (taatIndex.find(featureTest) != taatIndex.end()) {
                const vector<pair<int, float>> &training_rows_with_feature = taatIndex.at(featureTest);
                for (const pair<int, float> &trainValuePair : training_rows_with_feature) {
                    int trainUID = trainValuePair.first;
                    float featureValueTrain = trainValuePair.second;
                    if (similsTestInstance.find(trainUID) == similsTestInstance.end()) {
                        similsTestInstance[trainUID] = featureValueTest * featureValueTrain;
                    } else {
                        similsTestInstance[trainUID] += featureValueTest * featureValueTrain;
                    }
                }
            }
            if (taatIndexComplete.find(featureTest) != taatIndexComplete.end()) {
                const vector<pair<int, float>> &training_rows_with_feature2 = taatIndexComplete.at(featureTest);
                for (const pair<int, float> &trainValuePair : training_rows_with_feature2) {
                    int trainUID = trainValuePair.first;
                    float featureValueTrain = trainValuePair.second;
                    if (similsTestInstance.find(trainUID) == similsTestInstance.end()) {
                        similsTestInstance[trainUID] = featureValueTest * featureValueTrain;
                    } else {
                        similsTestInstance[trainUID] += featureValueTest * featureValueTrain;
                    }
                }
            }
        }
        if(simMatrixTopK.size() <= test_uid){
            simMatrixTopK.resize(test_uid+1);
        }
        nrOfTaatCandidates += similsTestInstance.size();
        simMatrixTopK[test_uid] = sortTopKPartialOrder(similsTestInstance, topK);
        //timer.stop();
    }


    /**
     * Loop parallel over postlists for each feature, sorted on document id.
     * We maintain an iterator over each postlist, using an offset,
     * We than select a 'pivot' element that can be used to prune the postlists, if an upperbound
     * is satisfied.
    */
    void computeSimilarityInstanceDAAT(int test_uid) {
        //init heap
        MyMinHeap myHeap(topK);
        //assumed to be computed by computeSimilarityInstanceTAAT
        if(simMatrixTopK.size() <= test_uid){
            simMatrixTopK.resize(test_uid+1);
        }
        for(const auto& pair: simMatrixTopK.at(test_uid)) {
            myHeap.add(pair.first, pair.second);
        }
        //features as vector of pairs
        vector<pair<int, float>> featureTestVec;
        featureTestVec.reserve(testData.getFeatures(test_uid).size());
        for (const auto& featureValuePair: testData.getFeatures(test_uid)) {
            featureTestVec.emplace_back(make_pair(featureValuePair.first, featureValuePair.second));
        }
        //compute max per dimension
        vector<float> maxWeights;
        maxWeights.reserve(featureTestVec.size());
        for (const auto &featureValuePair: featureTestVec) {
            int featureTest = featureValuePair.first;
            //cout << "taatIndex[" << featureTest << "]:" << to_string(taatIndex[featureTest])
            //     << "max:" << taatIndex[featureTest].back().second << endl;
            if(taatIndex.find(featureTest) != taatIndex.end() && taatIndex.at(featureTest).size() > 0) {
                float maxWeight = taatIndex.at(featureTest).back().second;
                maxWeights.emplace_back(maxWeight * featureValuePair.second); //BUGFIX FORGOT THIS!
            }
            else{ //taatIndex is empty
				if (daatIndex.find(featureTest) == daatIndex.end()) {
					maxWeights.emplace_back(0.0);
				}
				else {
					const auto& maximumItem = max_element(daatIndex.at(featureTest).begin(), daatIndex.at(featureTest).end(), sortByWeightAscending);
					if (maximumItem != daatIndex.at(featureTest).end()) {
						float maxWeight = maximumItem->second;
						maxWeights.emplace_back(maxWeight * featureValuePair.second);
					}
					else {
						maxWeights.emplace_back(0.0);
					}
				}
            }
        }
        //     cout << "max weights: " << to_string(maxWeights) << endl;
        vector<int> offsets;
        offsets.resize(featureTestVec.size());
        // Traverse in document order
        //cout << "Starting DAAT " << endl;
        int current_doc = 0;
        while (true) {
            //find candidates, by traversing all posting list parallel
            vector<pair<int,int>> candidates; //for candidate, keep trainUID and feature index
            candidates.reserve(featureTestVec.size());
            for (int i = 0; i < featureTestVec.size(); i++) {
                pair<int, float>& featureValuePair = featureTestVec.at(i);
                int featureTest = featureValuePair.first;
                if(daatIndex.find(featureTest) != daatIndex.end() && 
					offsets.at(i) < daatIndex.at(featureTest).size()){
                    pair<int, float>& trainValuePair = daatIndex.at(featureTest).at(offsets.at(i));
                    candidates.emplace_back(make_pair(trainValuePair.first, i));
                }
            }
            sort(candidates.begin(), candidates.end()); //by default pair first sorts on first, and if equal, on second
            //    cout << "candidates " << to_string(candidates) << endl;
            //    cout << "heap " << to_string(myHeap.getValues()) << endl;
            pair<int, int> pivot = make_pair(-999,-999);
            float upper_bound = 0.0;
            float min_value_heap = -9999;
            if(myHeap.isFull()){
                min_value_heap = myHeap.getMin();
            }
            for(pair<int, int>& candidate: candidates){
                upper_bound += maxWeights.at(candidate.second);
                if(upper_bound > min_value_heap){
                    pivot = candidate;
                    break;
                }
                //    if (upper_bound <= min_value_heap) {
                //        cout << " pruning " << candidate.first << " ubound:" << upper_bound << " heap:" << min_value_heap
            }
            //cout << "pivot: " << pivot.first << " ubound:" << upper_bound << " heap:" << min_value_heap << endl;
            if(pivot.first == -999){
                break;
            }
            //compute similarity pf pivot
            float simil = 0.0;
			for (int i = 0; i < featureTestVec.size(); i++) {
				pair<int, float>& featureValuePair = featureTestVec.at(i);
				int featureTest = featureValuePair.first;
				int next_offset = 0;
				float trainFeatureValue = 0.0;
				if (daatIndex.find(featureTest) != daatIndex.end()) {
					_advance_doclist(daatIndex.at(featureTest), offsets.at(i), pivot.first, next_offset, trainFeatureValue);
					offsets[i] = next_offset; //main wand/m-wand difference from fontoura
					simil += featureValuePair.second * trainFeatureValue;
				}
            }
            myHeap.add(pivot.first, simil);
            nrOfDaatPivots++; //for stats
        }
        simMatrixTopK[test_uid] = myHeap.getValues();
    }

    /**
     * Search next position in postlist (sorted on train uids) starting from current offset.
     * If trainUID is found, advance to element after trainUID, and save feature value,
     * else advande to element with UID higher than trainUID, and save 0.0 for feature value
     *
     */
    inline void _advance_doclist(const vector<pair<int,float>>& postlist, int current_offset, int trainUID, int& next_offset, float& featureValue){
        for(next_offset=current_offset; next_offset < postlist.size(); next_offset++){
            const pair<int,float>& item = postlist.at(next_offset);
            if(item.first < trainUID){
                continue;
            }
            else if(item.first == trainUID){
                next_offset++;
                featureValue = item.second;
                return;
            }
            else{
                featureValue = 0.0;
                return;
            }
        }
    }

    const vector<pair<int,float>>& getTopKNeighbours(int test_uid) const{
        return simMatrixTopK[test_uid];
    }


    //note: copy-pasted from RowKNN
    void computeSimilarityMatrix(unsigned int noThreads){
        auto start = chrono::steady_clock::now();
        simMatrixTopK.resize(testData.getNumberOfPoints());
        vector<thread> threads;
        unsigned int loopsize =  testData.getNumberOfPoints() / noThreads + 1;
        unsigned int start_index=0;
        unsigned int end_index=min(start_index + loopsize, (unsigned int)testData.getNumberOfPoints());
        for(unsigned int threadid = 0; threadid< noThreads; threadid++){
            threads.push_back( thread(&RowKNNPruning::computeSimilarityMatrixRange,
                                      this, threadid, start_index, end_index));
            if(verbose && trainData.getNumberOfPoints() > 100000)
                cout <<  "\033[0;37m" << " Adding thread " << threadid << " for test instances "
                     << start_index << "-" << end_index <<  "\033[0m" << endl;
            start_index += loopsize;
            end_index = start_index + loopsize;
            if(start_index >= testData.getNumberOfPoints()){
                break;
            }
            if(end_index > testData.getNumberOfPoints()){
                end_index = testData.getNumberOfPoints();
            }
        }
        for(auto& t : threads){
            t.join();
        }
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
        if(verbose)
            cout << "\033[0;37m" << ">>RowKNNPruning.computeSimilarityMatrix took "
                 << elapsed.count() << "ms" <<  "\033[0m" << endl;
    }

    //note: copy-pasted from RowKNN
    void computeSimilarityMatrixRange(int threadid, unsigned int start_range, unsigned int end_range){
        auto start_start = chrono::steady_clock::now();
        auto start = chrono::steady_clock::now();
        for(unsigned int i=start_range; i < end_range; i++) {
            if (i % 1000 == 0 && i!=0) {
                auto end = chrono::steady_clock::now();
                auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
                if(verbose && elapsed.count() > 100)
                    cout <<  "\033[0;37m" << ">>RowKNNPruning.computeSimilarityInstances thread #" << threadid << ": "
                     << setw(6) << i << " from range " << start_range << "-"  << i
                     << " took " << elapsed.count() << "ms" << " todo " << i << "-" << end_range <<  "\033[0m" << endl;
                start = end;
            }
            computeSimilarityInstanceTAAT(i);
            if (!onlyTAAT) {
                computeSimilarityInstanceDAAT(i);
            }
        }
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start_start);
        if(verbose && elapsed.count() > 100)
            cout  << "\033[0;37m" << ">>RowKNNPruning.computeSimilarityInstances thread "
                  << threadid << " took " << elapsed.count() << "ms" <<  "\033[0m" << endl;
    }

    const vector<vector<pair<int,float>>> & getKNNMatrix() const{
        return simMatrixTopK;
    }

    void clearIndex(){
        taatIndex.clear();
        taatIndexComplete.clear();
        taatUIDS.clear();
        daatIndex.clear();
    }

    void printPruningStats(){
        cout << ">RowKNNPruning.computeSimilarityMatrix stats: " << endl
             << " nrOfInstancesTotal:" << trainData.getNumberOfPoints() << endl
             << " nrOfInstancesTaat: " << taatUIDS.size()
             << " nrOfInstancesTaat relative: " << setprecision(3) <<  taatUIDS.size() / (float)trainData.getNumberOfPoints() << endl
             << " nrOfInstancesDaat: " << (trainData.getNumberOfPoints() - taatUIDS.size())
             << " nrOfInstancesDaat relative: " << setprecision(3) <<  (trainData.getNumberOfPoints() - taatUIDS.size()) / (float)trainData.getNumberOfPoints() << endl
             << " nrOfTaatPostingsLists (normal/complete):" << taatIndex.size() << " " << taatIndexComplete.size()<< endl
             << " nrOfDaatPostingLists: " << daatIndex.size() << endl
             << " avgNrOfTaatCandidates: " << setprecision(8) << nrOfTaatCandidates /(float)testData.getNumberOfPoints() << endl
             << " avgNrOfDaatCandidates: " << setprecision(8) << nrOfDaatPivots /(float)testData.getNumberOfPoints() << endl
             << " avgNrOfCandidates: " << setprecision(8) << (nrOfTaatCandidates +  nrOfDaatPivots)/(float)(testData.getNumberOfPoints()) << endl
             << " avgNrOfTaatCandidates relative to index: " << setprecision(8) << (nrOfTaatCandidates /(float)testData.getNumberOfPoints())/(float)taatUIDS.size() << endl
             << " avgNrOfDaatCandidates relative to index : " << setprecision(8) << (nrOfDaatPivots /(float)testData.getNumberOfPoints())/(float) (trainData.getNumberOfPoints() - taatUIDS.size())<< endl
             << " avgNrOfCandidates relative : " << setprecision(8) << ((nrOfTaatCandidates +  nrOfDaatPivots)/(float)(testData.getNumberOfPoints()))/(float)trainData.getNumberOfPoints() << endl;
    }

    float getAverageNumberOfCandidates(){
        return (nrOfTaatCandidates +  nrOfDaatPivots)/(float)(testData.getNumberOfPoints());
    }


private:
    //parameters:
    int topK;
    bool onlyTAAT;

    //input
    Data& trainData; //training
    Data& testData;

    //intermediate index
    unordered_map<int, vector<pair<int,float>>> taatIndex;
    unordered_map<int, vector<pair<int,float>>> taatIndexComplete;
    unordered_set<int> taatUIDS;
    unordered_map<int, vector<pair<int,float>>> daatIndex;
    long nrOfTaatCandidates = 0;
    long nrOfDaatPivots = 0;

    //output
    vector<vector<pair<int,float>>> simMatrixTopK;
};


#endif //LCIF_KNN_JOURNAL_ROWKNNPRUNING_H
