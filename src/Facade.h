//
// Created by Len Feremans on 18/10/2019.
//

#ifndef LCIF_KNN_JOURNAL_FACADE_H
#define LCIF_KNN_JOURNAL_FACADE_H

#include <string>
#include <vector>
#include <unordered_map>
#include <fstream>

#include "Evaluator.h"
#include "SingleThreshold.h"
#include "Data.h"
#include "FeatureKNN.h"
#include "RowKNNPruning.h"
#include "RowPredictions.h"
#include "LCIF.h"

using namespace std;

class Facade {

public:
    //Note: copy pasted from GridSearch
    float evaluateMetric(Evaluator& evaluator, SingleThreshold& st, const string& metricToOptimizeOn, bool atLeastOneScore, const vector<vector<pair<int, float>>>& scores) {
        //score based metrics
        if(metricToOptimizeOn.find("@") != string::npos){
            return evaluator.evaluateMetric(metricToOptimizeOn, scores);
        }
        //bipartition based metrics
        else {
            float optimalT = st.findOptimalThreshold(scores);
            vector<vector<pair<int, float>>> scoresThresholded = st.applyThreshold(atLeastOneScore, optimalT, scores);
            //for hamming loss no * -1 is needed as for optimalisation
            return evaluator.evaluateMetric(metricToOptimizeOn, scoresThresholded);
        }
    }

    tuple<float,float,float> run_lcif(string filenameTrain, string filenameTest,
                 float lambda, unsigned int kRow, float powerRow,
                 unsigned int kFeature, float powerFeature,
                 string metric, int sampleTest, bool verbose){
        //1) Load data
        cout << "\033[0;37m" << "LCIF: started on " << filenameTrain << "," << filenameTest <<  "\033[0m" << endl;
        auto start = chrono::steady_clock::now();
        Data train = Data(filenameTrain);
        train.makeIndex();
        //train.printStatistics();
        Data test = Data(filenameTest, sampleTest);

        //2) RowKNN
        //2.1) Prepare data
        vector<float> normsInstancesTrain = train.normalize_rows();
        vector<float> normsInstancesTest = test.normalize_rows();
        //2.2) Run row based KNN using PARTITION algorithm
        RowKNNPruning rowKNNPruning(train, test);
        rowKNNPruning.verbose = verbose;
        rowKNNPruning.setParameters(kRow, false);
        rowKNNPruning.runTAATPreProcessing(100);
        rowKNNPruning.runDAATPreProcessing();
        rowKNNPruning.computeSimilarityMatrix(noThreads);
        //2.3) Make Row KNN based predictions
        RowPredictions rowPredictions(train,test,rowKNNPruning.getKNNMatrix());
        rowPredictions.setParameters(kRow, powerRow, storeK);
        rowPredictions.computeScores(noThreads, verbose);
        const vector<vector<pair<int,float>>>& scoresRowKNN = rowPredictions.getPredictions();

        //3) FeatureKNN
        //3.1) Prepare data
        train.unnormalize_rows(normsInstancesTrain);
        test.unnormalize_rows(normsInstancesTest);
        unordered_map<int,float> norms = train.normalize_features();
        test.normalize_features(norms);
        //3.2) Run feature based knn
        FeatureKNN featureKNN(train, verbose);
        featureKNN.setParameters(kFeature, storeK, powerFeature);
        featureKNN.computeSimilarityMatrix(noThreads);
        featureKNN.computeTopKSortedSimilarityMatrixForFeatures(candidateSelectionK);
        featureKNN.computeSortedSimilarityMatrixForLabels();
        //3.3) Make feature-knn based predictions
        featureKNN.computeScores(test, noThreads, verbose);
        const vector<vector<pair<int,float>>>& scoresFeatureKNN = featureKNN.getPredictions();

        //4) Combine results (just weighted average)
        LCIF lcif;
        vector<vector<pair<int,float>>> scoresLCIF = lcif.combinePredictions(lambda, storeK, scoresRowKNN, scoresFeatureKNN);
        if(savePredictions){
            cout << "Saving predictions to ./temp/predictions.txt" << endl;
            savePredictionsToFile(scoresLCIF);
        }
        //5) Apply threshold
        SingleThreshold st(test);
        st.verbose = false;
        bool atLeastOneScoreRule = test.getMinimalNumberOfLabels() > 0;

        //6) Evaluate on selected metric
        MyTimer eval("evaluate");
        Evaluator evaluator(train,test);
        cout.precision(3);
        if(metric == "precision@5"){
            for(string relatedmetric: {"precision@1","precision@3"}){
                cout << "\033[1;30m" << relatedmetric << ":" << " "
                    << " Ins. KNN: "  << evaluateMetric(evaluator, st, relatedmetric, atLeastOneScoreRule, scoresRowKNN)
                    << " Feat. KNN: " << evaluateMetric(evaluator, st, relatedmetric, atLeastOneScoreRule, scoresFeatureKNN)
                    << " LCIF: " << evaluateMetric(evaluator, st, relatedmetric, atLeastOneScoreRule, scoresLCIF)
                    << "\033[0m" << endl;
            }
        }
        if(metric == "macroF1_2"){
            for(string relatedmetric: {"macroF1"}){
                cout << "\033[1;30m" << relatedmetric << ":" << " "
                     << " Ins. KNN: "  << evaluateMetric(evaluator, st, relatedmetric, atLeastOneScoreRule, scoresRowKNN)
                     << " Feat. KNN: " << evaluateMetric(evaluator, st, relatedmetric, atLeastOneScoreRule, scoresFeatureKNN)
                     << " LCIF: " << evaluateMetric(evaluator, st, relatedmetric, atLeastOneScoreRule, scoresLCIF)
                     << "\033[0m" << endl;
            }
        }
        float resultRow = evaluateMetric(evaluator, st, metric, atLeastOneScoreRule, scoresRowKNN);
        float resultFeature = evaluateMetric(evaluator, st, metric, atLeastOneScoreRule, scoresFeatureKNN);
        float resultLCIF = evaluateMetric(evaluator, st, metric, atLeastOneScoreRule, scoresLCIF);
        cout << "\033[1;30m" << metric << ":" << " " << " Ins. KNN: "  << resultRow << " Feat. KNN: "
             << resultFeature << " LCIF: " << resultLCIF << "\033[0m" << endl;
        eval.stop();
        //7) Report runtime
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
        cout <<  "\033[0;37m" << ">>LCIF total " << elapsed.count() << " ms" <<  "\033[0m" << endl;
        return {resultRow,resultFeature,resultLCIF};
    }

public:
    int storeK = 1000;
    int noThreads = 64;
    int candidateSelectionK = 10;
    bool savePredictions=true;

private:
    void savePredictionsToFile(vector<vector<pair<int,float>>>& scores){
        ofstream of("./temp/predictions.txt");
        for(vector<pair<int,float>>& instanceScores: scores){
            for(pair<int,float>& pair: instanceScores){
                of << pair.first << ":" << pair.second << " ";
            }
            of << endl;
        }
        of.close();
    }
};


#endif //LCIF_KNN_JOURNAL_FACADE_H
