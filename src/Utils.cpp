//
// Created by Len Feremans on 10/09/2018.
//
#include "Utils.h"
#include <fstream>

string to_string(const vector<int> &data) {
    ostringstream os;
    for(auto it = data.begin(); it != data.end(); it++){
        os << *it << " ";
    }
    return os.str();
}

string to_string(const vector<string> &data) {
    ostringstream os;
    for(auto it = data.begin(); it != data.end(); it++){
        os << *it << " ";
    }
    return os.str();
}

string to_string(const vector<float> &data) {
    ostringstream os;
    for(auto it = data.begin(); it != data.end(); it++){
        os << setprecision(3) << *it << " ";
    }
    return os.str();
}

string to_string(const set<int> &data) {
    ostringstream os;
    for(auto it = data.begin(); it != data.end(); it++){
        os << *it << " ";
    }
    return os.str();
}

string to_string(const unordered_map<int,float> &data) {
    ostringstream os;
    for(auto it = data.begin(); it != data.end(); it++){
        os << it->first << ":" << fixed << setprecision(3) << it->second << " ";
    }
    return os.str();
}

string to_string(const vector<pair<int,float>> &data) {
    ostringstream os;
    for(auto it = data.begin(); it != data.end(); it++){
        os << it->first << ":" << fixed << setprecision(3) << it->second << " ";
    }
    return os.str();
}

string to_string(const vector<pair<int,int>> &data) {
    ostringstream os;
    for(auto it = data.begin(); it != data.end(); it++){
        os << it->first << ":" << it->second << " ";
    }
    return os.str();
}



//see https://stackoverflow.com/questions/236129/the-most-elegant-way-to-iterate-the-words-of-a-string
//adjusted with multiple seperators
vector<string> split(const string &text, const vector<char>& seperators) {
    string text2 = text;
    text2.erase( std::remove(text2.begin(), text2.end(), '\r'), text2.end());
    vector<string> tokens;
    size_t start = 0, end = 0, tmp = 0;
    while (true) {
        end = 999999;
        //len added, bit of hack, but whatever
        for(char sep: seperators){
            tmp = text2.find(sep, start);
            if(tmp == string::npos){
                continue;
            }
            else{
                end = min(tmp,end);
            }
        }
        if(end == string::npos || end == 999999){
            break;
        }
        if (end != start) {
            tokens.push_back(text2.substr(start, end - start));
        }
        start = end + 1;
    }
    if (end != start) {
        tokens.push_back(text2.substr(start));
    }
    return tokens;
}


bool sortByWeightDescending(pair<int, float> p1, pair<int, float> p2) {
    return p1.second > p2.second; //true if smaller, e.g. thus sorting Descending on weight
}
bool sortByWeightAscending(pair<int,float> p1, pair<int,float> p2) {
    return p1.second < p2.second;
}

vector<pair<int,float>> sortTopKPartialOrder(const unordered_map<int,float>& myunordered_map, unsigned long topK) {
    vector<pair<int,float>> myvector(myunordered_map.size());
    int i=0;
    for(const auto& mpair: myunordered_map){
        myvector[i++] = make_pair(mpair.first,mpair.second);
    }
    unsigned long middle = min(topK,(unsigned long)myvector.size());
    partial_sort(myvector.begin(), myvector.begin() + middle, myvector.end(), sortByWeightDescending);
    return vector<pair<int,float>>(myvector.begin(),myvector.begin()+middle);
}

vector<pair<int,float>> sortTopKPartialOrder(vector<pair<int,float>>& myvector, unsigned long topK) {
    unsigned long middle = min(topK,(unsigned long)myvector.size());
    partial_sort(myvector.begin(), myvector.begin() + middle, myvector.end(), sortByWeightDescending);
    return vector<pair<int,float>>(myvector.begin(),myvector.begin()+middle);
}

vector<string> splitFile(string filename, long partition_size, bool skipFirstLine){
    //see https://stackoverflow.com/questions/5166263/how-to-get-iostream-to-perform-better:
    char Buffer[1042*1024]; //default 8KB, now 1MB
    ifstream myfile (filename);
    myfile.rdbuf()->pubsetbuf(Buffer, 1042*1024);
    if (!myfile.is_open()) {
        cerr << "Could not read file " << filename << endl;
        return vector<string>();
    }
    string line;
    if(skipFirstLine){
        getline (myfile,line);
    }
    long i=0;
    vector<string> filenames_splits;
    int k = 1;
    string currentOutputFilename = filename + "_part_" + to_string(k);
    filenames_splits.push_back(currentOutputFilename);
    ofstream currentOutput(currentOutputFilename);
    while ( getline (myfile,line) ) {
        i++;
        currentOutput << line << endl;
        if(i!=0 && i % partition_size == 0){
            currentOutput.close();
            cout << "saved " << currentOutputFilename << endl;
            k+=1;
            currentOutputFilename = filename + "_part_" + to_string(k);
            filenames_splits.push_back(currentOutputFilename);
            currentOutput = ofstream(currentOutputFilename);
        }
    }
    if(currentOutput.is_open()) {
        currentOutput.close();
        cout << "saved " << currentOutputFilename << endl;
    }
    myfile.close();
    return filenames_splits;
}


vector<char> delims_libsvm = {',', ' '};

//e.g. parse:
//1852, 1863, 1913 513:1 516:1 6:1 1799:1 1546:1 1295:1 1296:1 1808:1 1553:1 22:1 23:1 1304:1 1826:1 807:1 1831:1 296:1 44:1 813:1 558:1 1071:1 1327:1 305:1 1585:1 306:1 53:1 565:1 1590:1 55:1 570:1 830:1 1087:1 326:1 1608:1 329:1 588:1 78:1 1617:1 1620:1 870:1 1129:1 364:1 370:1 117:1 375:1 121:1 1402:1 123:1 1147:1 1660:1 895:1 1663:1 129:1 1153:1 1666:1 1667:1 1668:1 1413:1 1414:1 1676:1 911:1 1680:1 1682:1 1684:1 1175:1 665:1 1181:1 1693:1 1442:1 677:1 1446:1 1196:1 942:1 178:1 181:1 182:1 1206:1 1464:1 185:1 698:1 443:1 1212:1 1469:1 448:1 1473:1 450:1 1479:1 1736:1 714:1 1230:1 1743:1 1744:1 210:1 211:1 217:1 733:1 989:1 735:1 1247:1 738:1 232:1 233:1 1769:1 1516:1 1774:1 752:1 754:1 244:1 1780:1 1528:1 506:1 507:1 767:1 1791:1
void parseLineLibSVM(const string& line, vector<int>& labels, unordered_map<int,float>& features){
    //cout << line << endl;
    vector<string> tokens = split(line, delims_libsvm);
    //cout << to_string(tokens) << endl;
    for(const string& token: tokens){
        int idx = token.find(':',0);
        if(token == "")
            continue;
        if(idx == string::npos){
            labels.emplace_back(stoi(token));
        }
        else{
            int feature_id = stoi(token.substr(0,idx));
            float value = stod(token.substr(idx+1,token.length()));
            features[feature_id] = value;
            if(value < 0){
                throw invalid_argument("Error: negative feature value while parsing line " + line);
            }
        }
    }
}
