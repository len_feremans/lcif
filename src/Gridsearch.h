//
// Created by Len Feremans on 14/10/2019.
//

#ifndef LCIF_KNN_JOURNAL_GRIDSEARCH_H
#define LCIF_KNN_JOURNAL_GRIDSEARCH_H

#include <vector>
#include <set>
#include <unordered_map>
#include <chrono>
#include <iostream>
#include <sstream>

#include "Data.h"
#include "Utils.h"

#include "FeatureKNN.h"
#include "RowKNN.h"
#include "RowKNNPruning.h"
#include "RowPredictions.h"
#include "FeatureSelection.h"
#include "Evaluator.h"
#include "SingleThreshold.h"
#include "LCIF.h"
#include "Facade.h"

struct ParameterLCIF{
    int kRow;
    float kRowPower;
    int kFeature;
    float kFeaturePower;
    float lambda;

    string toString(){
        ostringstream os;
        os.precision(3);
        os << " kRow=" << kRow << ", kRowPower=" << kRowPower << ", kFeature="
           << kFeature << ", kFeaturePower"  << kFeaturePower << ", lambda:" << lambda;
        return os.str();
    }
};

class Gridsearch {

public:
    ParameterLCIF search_best_parameters_folded(
            string filenameTrain,
            string metricToOptimizeOn,
            int folds) {
        //split file into k folds
        vector<pair<string,string>> kFoldFilenames = createKFolds(filenameTrain, folds);
        //run gridsearch on each fold
        vector<ParameterLCIF> best_parameters;
        int foldIdx = 1;
        bool verbose = true;
        for(pair<string,string> fold: kFoldFilenames){
            if(verbose)
                cout << "\033[0;37m" << "------------- GRID SEARCH FOLD " << (foldIdx++) << "-------------" << "\033[0m" << endl;
            string filenameTrainFold = fold.first;
            string filenameTestFold = fold.second;
            ParameterLCIF best = search_best_parameters(filenameTrainFold, filenameTestFold, metricToOptimizeOn, -1);
            cout << "\033[1;30m" << "Best fold " << foldIdx << ": " << best.toString() << "\033[0m" << endl;
            best_parameters.emplace_back(best);
        }
        //Best: Take best parameters for kRow, kFeature and lambda using majority voting
        /*
        unordered_map<string,int> votingKrow;
        unordered_map<string,int> votingKfeature;
        unordered_map<string,int> votingKLambda;
        if(verbose)
            cout << "Best parameter in each fold:" << endl;
        for(ParameterLCIF par: best_parameters){
            votingKrow[to_string(par.kRow) + "_" + to_string(par.kRowPower)] +=1;
            votingKfeature[to_string(par.kFeature) + "_" + to_string(par.kFeaturePower)] +=1;
            votingKLambda[to_string(par.lambda)] +=1;
            if(verbose)
                cout << par.toString() << endl;
        };
        ParameterLCIF bestVoted = best_parameters[0];

        //best parameters for krow
        int maxCount = 1;
        for(ParameterLCIF par: best_parameters){
            int count = votingKrow[to_string(par.kRow) + "_" + to_string(par.kRowPower)];
            if(count > maxCount){
                maxCount = count;
                bestVoted.kRow = par.kRow;
                bestVoted.kRowPower = par.kRowPower;
            }
        }
        //best parameters for kfeature
        maxCount = 1;
        for(ParameterLCIF par: best_parameters){
            int count = votingKfeature[to_string(par.kFeature) + "_" + to_string(par.kFeaturePower)];
            if(count > maxCount){
                maxCount = count;
                bestVoted.kFeature = par.kFeature;
                bestVoted.kFeaturePower = par.kFeaturePower;
            }
        }
        //best lambda
        maxCount = 1;
        for(ParameterLCIF par: best_parameters){
            int count = votingKLambda[to_string(par.lambda)];
            if(count > maxCount){
                maxCount = count;
                bestVoted.lambda = par.lambda;
            }
        }
         */
        //Best take average value...
        float kRowAvg=0.0f,kRowPowerAvg=0.0f,kFeatureAvg=0.0f,kFeaturePowerAvg=0.0f,lambdaAvg=0.0f;
        for(ParameterLCIF par: best_parameters){
            kRowAvg += par.kRow;
            kRowPowerAvg += par.kRowPower;
            kFeatureAvg += par.kFeature;
            kFeaturePowerAvg += par.kFeaturePower;
            lambdaAvg += par.lambda;
        }
        float l = best_parameters.size();
        kRowAvg /= l ; kRowPowerAvg /= l; kFeatureAvg /=l; kFeaturePowerAvg /= l; lambdaAvg /= l;
        ParameterLCIF best;
        best.kRow = (int)round(kRowAvg);
        best.kFeature = (int) round(kFeatureAvg);
        best.kRowPower = kRowPowerAvg;
        best.kFeaturePower = kFeaturePowerAvg;
        best.lambda = lambdaAvg;
        if(verbose)
            cout << "\033[1;30m" << "Best parameters:" << best.toString() << "\033[0m" << endl;
        return best;
    }

    //Note copy-paste from Facade.run_lcif but necessary
    ParameterLCIF search_best_parameters(
                    string filenameTrain, string filenameTest,
                    string metricToOptimizeOn,
                    int sampleTest) {

        //1) Load data
        cout << "\033[0;37m" << "LCIF: started on " << filenameTrain << "," << filenameTest << endl << "\033[0m";
        auto start = chrono::steady_clock::now();
        Data train = Data(filenameTrain);
        train.makeIndex();
        Data test = Data(filenameTest, sampleTest);
        cout.precision(3);

        //2) Init thresholding and evaluation
        SingleThreshold st(test);
        st.verbose = false; //not really interesting
        bool atLeastOneScoreRule = test.getMinimalNumberOfLabels() > 0;
        Evaluator evaluator(train, test);

        //3) RowKNN
        //3.1) ----- init data  -----
        vector<float> normsInstancesTrain = train.normalize_rows();
        vector<float> normsInstancesTest =  test.normalize_rows();
        //3.2)  -----  Run KNN search -----
        int kRowMax = *(max_element(kRange.begin(), kRange.end()));
        RowKNNPruning rowKNNPruning(train, test);
        rowKNNPruning.verbose = verbose; //otherwise to much output
        rowKNNPruning.setParameters(kRowMax, false);
        rowKNNPruning.runTAATPreProcessing(topMIndex);
        rowKNNPruning.runDAATPreProcessing();
        rowKNNPruning.computeSimilarityMatrix(noThreads);
        //3.3)  -----  Run grid search on K for predictions  -----
        RowPredictions rowPredictions(train, test, rowKNNPruning.getKNNMatrix());
        float power = 1.0; //default
        int maxK = -1;
        float maxScore = -1;
        for (int k: kRange) {
            rowPredictions.setParameters(k, power, storeK);
            rowPredictions.computeScores(noThreads, false); // note: uses k < maxK
            float resultsOnMetric = evaluateMetric(evaluator, st, metricToOptimizeOn, atLeastOneScoreRule, rowPredictions.getPredictions());
            if (verbose) cout << "RowKNN " << " *k:" << k << " pow: " << power << " " << metricToOptimizeOn << ": " << resultsOnMetric << endl;
            if (resultsOnMetric > maxScore) {
                maxK = k; maxScore = resultsOnMetric;
            }
        }
        //3.4) -----  Run grid search on power transform for predictions  -----
        maxScore = -1;
        float maxPower = -1;
        for (float power: powerRange) {
            rowPredictions.setParameters(maxK, power, storeK);
            rowPredictions.computeScores(noThreads, false);
            float resultsOnMetric = evaluateMetric(evaluator, st, metricToOptimizeOn, atLeastOneScoreRule, rowPredictions.getPredictions());
            if (verbose) cout << "RowKNN " << " k:" << maxK << " *pow: " << power << " " << metricToOptimizeOn << ": " << resultsOnMetric << endl;
            if (resultsOnMetric > maxScore) {
                maxPower = power; maxScore = resultsOnMetric;
            }
        }
        rowPredictions.setParameters(maxK, maxPower, storeK);
        rowPredictions.computeScores(noThreads, false);
        if (verbose) cout << "\033[1;30m" << "GS RowKNN. Best k:" << maxK
            << " power:" << maxPower << " " << metricToOptimizeOn << ": " << maxScore << "\033[0m" << endl;

        //4) FeatureKNN
        //4.1) ----- init data  -----
        train.unnormalize_rows(normsInstancesTrain); //could also reload data
        test.unnormalize_rows(normsInstancesTest);
        unordered_map<int,float> norms = train.normalize_features();
        test.normalize_features(norms);
        //2)  -----  Run KNN search -----
        float powerFeature = 1.0;
        float kFeature = 1;
        FeatureKNN featureKNN(train);
        featureKNN.setParameters(kFeature, storeK, powerFeature);
        featureKNN.computeSimilarityMatrix(noThreads);
        featureKNN.computeTopKSortedSimilarityMatrixForFeatures(candidateSelectionK);
        featureKNN.computeSortedSimilarityMatrixForLabels();
        //3)  -----  Run grid search on predictions  -----
        //3.1) Optimize k for feature KNN
        int maxKFeature = -1;
        float maxScoreFeature = -1;
        kFeatureRange = {1}; //for new method!
        for (int k: kFeatureRange) {
            featureKNN.setParameters(k, storeK, powerFeature);
            featureKNN.computeScores(test, noThreads, false);
            float resultsOnMetric = evaluateMetric(evaluator, st, metricToOptimizeOn, atLeastOneScoreRule, featureKNN.getPredictions());
            if (verbose) cout << "FeatureKNN " << " *k:" << k << " pow: " << powerFeature << " " << metricToOptimizeOn << ": " << resultsOnMetric << endl;
            if (resultsOnMetric > maxScoreFeature) {
                maxKFeature = k; maxScoreFeature = resultsOnMetric;
            }
        }
        //3.2) Optimize power transform for feature KNN
        maxScoreFeature = -1;
        float maxPowerFeature = -1;
        for (float power: powerFeatureRange) {
            featureKNN.setParameters(maxKFeature, storeK, power);
            featureKNN.computeScores(test, noThreads, false);
            float resultsOnMetric = evaluateMetric(evaluator, st, metricToOptimizeOn, atLeastOneScoreRule, featureKNN.getPredictions());
            if (verbose) cout << "FeatureKNN " << " k:" << maxKFeature << " *pow: " << power << " " << metricToOptimizeOn << ": " << resultsOnMetric << endl;
            if (resultsOnMetric > maxScoreFeature) {
                maxPowerFeature = power; maxScoreFeature = resultsOnMetric;
            }
        }
        featureKNN.setParameters(maxKFeature, storeK, maxPowerFeature);
        featureKNN.computeScores(test, noThreads, false);
        if (verbose) cout << "\033[1;30m" << "GS FeatureKNN. Best k:" << maxKFeature
            << " power:" << maxPowerFeature << " " << metricToOptimizeOn << ": " << maxScoreFeature << "\033[0m" << endl;

        //5) Combine score in Linear ensemble
        LCIF lcif;
        //5.1) ------ Run grid search to ptimize lambda  -----
        float maxScoreLCIF = -1;
        float maxLambda = -1;
        for (float lambda: lambdaRange) {
            vector<vector<pair<int, float>>> scoresLCIF = lcif.combinePredictions(lambda, storeK,
                    rowPredictions.getPredictions(), featureKNN.getPredictions());
            float resultsOnMetric = evaluateMetric(evaluator, st, metricToOptimizeOn, atLeastOneScoreRule, scoresLCIF);
            if (verbose) cout << "LCIF " << " *lambda:" << lambda << " " << metricToOptimizeOn << ": " << resultsOnMetric << endl;
            if (resultsOnMetric > maxScoreLCIF) {
                maxLambda = lambda; maxScoreLCIF = resultsOnMetric;
            }
        }
        if (verbose) cout << "\033[1;30m" << "GS LCIF. Best lambda:" << maxLambda << endl << " " << metricToOptimizeOn << ": " << maxScoreLCIF << "\033[0m" << endl;
        const vector<vector<pair<int, float>>> &scoresRowKNN = rowPredictions.getPredictions();
        const vector<vector<pair<int, float>>> &scoresFeatureKNN = featureKNN.getPredictions();
        const vector<vector<pair<int, float>>> scoresLCIFFinal = lcif.combinePredictions(maxLambda, storeK, scoresRowKNN,scoresFeatureKNN);

        //6) Print results if verbose
        if (verbose) {
            cout << "\033[1;30m" << metricToOptimizeOn << ":" << " "
                 << " Ins. KNN: " << evaluateMetric(evaluator,st,metricToOptimizeOn, atLeastOneScoreRule, scoresRowKNN)
                 << " Feat. KNN: " <<  evaluateMetric(evaluator,st,metricToOptimizeOn, atLeastOneScoreRule, scoresFeatureKNN)
                 << " LCIF: " << evaluateMetric(evaluator,st,metricToOptimizeOn, atLeastOneScoreRule, scoresLCIFFinal)
                 << "\033[0m" << endl;
            auto end = chrono::steady_clock::now();
            auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
            cout << "\033[0;37m" << ">>LCIF total " << elapsed.count() << " ms" << "\033[0m" << endl;
        }

        //7) Return best parameters
        ParameterLCIF parameter;
        parameter.kRow = maxK;
        parameter.kRowPower = maxPower;
        parameter.kFeature = maxKFeature;
        parameter.kFeaturePower = maxPowerFeature;
        parameter.lambda = maxLambda;
        return parameter;
    }

private:

    float evaluateMetric(Evaluator& evaluator, SingleThreshold& st,
            const string& metricToOptimizeOn, bool atLeastOneScoreRule, const vector<vector<pair<int, float>>>& scores) {
        //score based metrics
        if(metricToOptimizeOn.find("@") != string::npos){
            return evaluator.evaluateMetric(metricToOptimizeOn, scores);
        }
        //bipartition based metrics
        else {
            float optimalT = st.findOptimalThreshold(scores);
            vector<vector<pair<int, float>>> scoresThresholded = st.applyThreshold(atLeastOneScoreRule, optimalT, scores);
            float score =  evaluator.evaluateMetric(metricToOptimizeOn, scoresThresholded);
            //metrics where lower is better:
            if (metricToOptimizeOn == "hammingLoss") {
                score *= -1;
            }
            return score;
        }
    }

    void saveLine(ofstream& myfile, Data& train, int uid){
        //example output in lib svm format:
        //1852, 1863, 1913 513:1 516:1 6:1 1799:1 1546:1 1295:1 1296:0.1
        //labels           features
        const vector<int>& labels = train.getLabels(uid);
        const unordered_map<int,float>&  features = train.getFeatures(uid);
        //save labels
        if(labels.size() > 0){
            myfile << labels[0];
            for(int labelIdx = 1; labelIdx < labels.size(); labelIdx++){
                myfile << ", " << labels[labelIdx];
            }
            myfile << " ";
        }
        //save features
        for(const auto& pair: features){
            myfile << pair.first << ":" << pair.second << " ";
        }
        myfile << endl;
    }

    vector<pair<string,string>> createKFolds(string filenameTrain, int folds) {
        Data train = Data(filenameTrain);
        int noSamples = train.getNumberOfPoints();
        int size = noSamples / folds;
        vector<pair<string, string>> kFoldFilenames;
        for (int fold = 1; fold <= folds; fold++) {
            //save train/test file
            int testStart = (fold - 1) * size;
            string filename_fold_test = "./temp/fold-test-" + to_string(fold) + ".svm";
            string filename_fold_train = "./temp/fold-train-" + to_string(fold) + ".svm";
            ofstream myfile(filename_fold_test);
            myfile << size << " " << train.getNumberOfFeatures() << " " << train.getNumberOfLabels() << endl;
            if (myfile.is_open()) {
                //for instance in range of 'test' fold[i]
                for (int i = testStart; i < testStart + size; i++) {
                    saveLine(myfile, train, i);
                }
                myfile.close();
            } else {
                cerr << "Error can not write: " << filename_fold_test << endl;
            }
            myfile = ofstream(filename_fold_train);
            myfile << (noSamples - size) << " " << train.getNumberOfFeatures() << " " << train.getNumberOfLabels() << endl;
            if (myfile.is_open()) {
                //for instance in range of 'train' fold[i]
                for (int i = 0; i < noSamples; i++) {
                    if (i >= testStart && i < testStart + size) {
                        continue;
                    } else {
                        saveLine(myfile, train, i);
                    }
                }
                myfile.close();
            }
            cout << "saved fold " << fold << endl;
            kFoldFilenames.push_back(make_pair(filename_fold_train, filename_fold_test));
        }
        return kFoldFilenames;
    }


public:
    bool verbose = false;
    int storeK = 1000; //was 1000
    int noThreads = 64;
    int candidateSelectionK = 10;
    int topMIndex = 100;
    vector<int> kRange = {1,5, 50, 100, 150, 200, 250, 300, 350};
    vector<int> kFeatureRange = {1}; //{1, 5, 10, 15, 20, 25, 30, 50, 100};
    vector<float> powerRange = {0.5f, 1.0f, 1.5f, 2.0f}; //-2.0f, -1.5f, -0.5f,
    vector<float> powerFeatureRange = {0.5f, 1.0f, 1.5f, 2.0f}; //-2.0f, -1.5f, -0.5f,
    vector<float> lambdaRange = {0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
};

#endif //LCIF_KNN_JOURNAL_GRIDSEARCH_H
