//
// Created by Len Feremans on 26/09/2018.
//

#ifndef LCIF_KNN_JOURNAL_MYMINHEAP_H
#define LCIF_KNN_JOURNAL_MYMINHEAP_H

#include <vector>
#include <algorithm>
#include "Utils.h"
using namespace std;

//see also https://codeconnect.wordpress.com/2013/09/05/max-min-heap-using-c-stl/
class MyMinHeap {

public:
    MyMinHeap() {}
    MyMinHeap(int maxSize): maxSize(maxSize){}

    void setMaxSize(int maxSize){
        this->maxSize = maxSize;
    }
    void add(int id, float value){
        if(heap.size() < maxSize){
            heap.push_back(make_pair(id,value));
            make_heap(heap.begin(), heap.end(), sortByWeightDescending);
        }
        else{
            float minValue = heap.front().second;
            if(minValue < value){
                pop_heap(heap.begin(), heap.end(), sortByWeightDescending);
                heap.pop_back();
                heap.push_back(make_pair(id,value));
                push_heap(heap.begin(), heap.end(), sortByWeightDescending);
            }
        }
    }

    float getMin(){
        return heap.front().second;
    }

    bool isFull(){
        return heap.size() == maxSize;
    }

    vector<pair<int,float>> getValues(){
        vector<pair<int,float>> copy = heap;
        sort(copy.begin(), copy.end(), sortByWeightDescending);
        return copy;
    };

private:
    int maxSize;
    vector<pair<int,float>> heap;
};


#endif //LCIF_KNN_JOURNAL_MYMINHEAP_H
