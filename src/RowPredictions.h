//
// Created by Len Feremans on 26/09/2018.
//

#ifndef LCIF_KNN_JOURNAL_ROWPREDICTIONS_H
#define LCIF_KNN_JOURNAL_ROWPREDICTIONS_H

#include <chrono>
#include <thread>
#include <vector>
#include <iostream>

#include "Data.h"

using namespace std;

class RowPredictions {
public:
    RowPredictions(Data& trainData, Data& testData, const vector<vector<pair<int,float>>>& knnMatrix):
            trainData(trainData), testData(testData), simMatrixTopK(knnMatrix){}

    void setParameters(unsigned int topK, float power, unsigned int storeK){
        this->topK = topK;
        this->power = power;
        this->storeK = storeK;
    }

    //note: copy-pasted featureKNN
    void computeScores(int noThreads, bool verboseTiming){
        this->verbose = verboseTiming;
        this->predictions.resize(testData.getNumberOfPoints());
        auto startTotal = chrono::steady_clock::now();
        vector<thread> threads;
        unsigned int loopsize =  testData.getNumberOfPoints() / noThreads + 1;
        unsigned int start_index=0;
        unsigned int end_index=min(start_index + loopsize, (unsigned int) testData.getNumberOfPoints());
        for(int threadid = 0; threadid< noThreads; threadid++){
            threads.push_back( thread(&RowPredictions::computeScoresRange,  this, threadid, start_index, end_index));
            start_index += loopsize;
            end_index = min(start_index + loopsize, (unsigned int)testData.getNumberOfPoints());
        }
        for(auto& t : threads){
            t.join();
        }
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - startTotal);
        if(verboseTiming)
            cout <<  "\033[0;37m" << ">>RowKNN.computeScores took " << elapsed.count() << "ms" <<  "\033[0m"<< endl;
        return;
    }

    void computeScoresRange(int thread_id, unsigned int start_index, unsigned int end_index){
        auto start_start = chrono::steady_clock::now();
        auto start = chrono::steady_clock::now();
        vector<vector<pair<int,float>>> predictions;
        for(unsigned int i=start_index; i<end_index; i++){
            if(i % 1000 == 0 && i!=0){
                auto end = chrono::steady_clock::now();
                auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
                if(verbose && elapsed.count() > 100){
                    cout <<  "\033[0;37m" << ">>RowKNN.computeScoresRange thread #" << thread_id << ":" << setw(6) << (i-start_index) <<  "/" << (end_index-start_index)
                    << " took " << elapsed.count() << "ms" <<  "\033[0m" << endl;
                    start = end;
                }
            }
            computeScoreInstance(i);
        }
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start_start);
        if(verbose && elapsed.count() > 100)
            cout << "\033[0;37m" << ">>RowKNN.computeScoresRange thread #" << thread_id << " took "
                 << elapsed.count() << "ms" <<  "\033[0m" << endl;
        return;
    }

    void computeScoreInstance(int test_uid){
        const vector<pair<int,float>>& neighbors = simMatrixTopK[test_uid];
        unordered_map<int, float> scores;
        float denominator = 0.0;
        unsigned int i = 0;
        for(const pair<int,float>& neighbor: neighbors){
            const vector<int>& train_labels_neighbor = trainData.getLabels(neighbor.first);
            float simil = pow(neighbor.second, power);
            for(int label: train_labels_neighbor){
                scores[label] += simil;
            }
            denominator += simil;
            i++;
            if(i >= topK) //could be that similarity is computed using higher k, e.g. in grid-search scenario's
                break;
        }
        for(pair<const int,float>& score: scores){
            score.second = score.second / denominator;
        }
        predictions[test_uid] = sortTopKPartialOrder(scores,storeK);
    }

    const vector<vector<pair<int,float>>>& getPredictions(){
        return predictions;
    };

private:
    //parameters
    unsigned int topK; //if knn is compute with higher k
    float power;
    unsigned int storeK;
    bool verbose;

    //input data
    Data& trainData; //training
    Data& testData;
    const vector<vector<pair<int,float>>>& simMatrixTopK;

    //stored data
    vector<vector<pair<int,float>>> predictions;
};


#endif //LCIF_KNN_JOURNAL_ROWPREDICTIONS_H
