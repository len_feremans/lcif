//
// Created by Len Feremans on 10/09/2018.
//

#ifndef LCIF_KNN_JOURNAL_DATA_H
#define LCIF_KNN_JOURNAL_DATA_H

#include <vector>
#include <unordered_map>

#include <algorithm>
#include <cmath>
#include <functional>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <utility>

#include <chrono>

#include "Utils.h"
#include "MyTimer.h"


using namespace std;

class Data{
public:

    Data(const string& filename): Data(filename, -1, true){
    }

    Data(const string& filename, int sample_size): Data(filename, sample_size, true){
    }

    Data(const string& filename, int sample_size, bool verbose){
        this->verbose = verbose;
        this->filename = filename;
        load_data(sample_size);
        //if(verbose) {
        //    cout << ">>labels[0]: " << to_string(getLabels(0)) << endl
        //         << ">>features[0]: " << to_string(getFeatures(0)) << endl;
        //}
    }

    /**
     * Loading LIBSVM format
     * In XML (Extreme Multi-Label) repo, first line contains number of rows, features and labels, and is used
     * @param skipFirstLine
     * @param makeIndex
     */
    void load_data(int sample_size){
        MyTimer myTimer("load_data");
        string line;
        ifstream myfile (filename);
        if (!myfile.is_open()) {
            cerr << "Error: could not read file: '" << filename << "'" << endl;
            return;
        }
        getline (myfile,line);
        //first line is nrOfTrainingPoints nrOfFeature nrOfLabels
        vector<string> tokens = split(line, {' '});
        nrOfInstances = stoi(tokens[0]);
        nrOfFeatures = stoi(tokens[1]);
        nrOfLabels = stoi(tokens[2]);
        if(verbose) {
            cout << "load_data " << filename << " #instances: " << nrOfInstances << " #features: " << nrOfFeatures << " #labels: "
                 << nrOfLabels;
            if(sample_size > 0){
                nrOfInstances = min(sample_size,nrOfInstances);
                cout << " #sample: " << nrOfInstances;
            }
            cout << endl;
        }
        //init datastructure sizes
        this->labels.reserve(nrOfInstances);
        this->features.reserve(nrOfInstances);
        int i=0;
        while ( getline (myfile,line) )
        {
            if(i% 100000 == 0 && i!=0){
                if(verbose)
                    myTimer.progress("load_data "+ to_string(i) + "/" + to_string(nrOfInstances));
            }
            vector<int> labels;
            unordered_map<int,float> features;
            parseLineLibSVM(line, labels, features);
            this->labels.emplace_back(labels);
            this->features.emplace_back(features);
            if(sample_size != -1 && sample_size <= i){
                break;
            }
            i++;
        }
        if(i!=nrOfInstances){
            cerr << "Warning: nrOfInstances is not correct. " << endl;
            nrOfInstances = i;
        }
        myfile.close();
        //done
        if(verbose)
            myTimer.stop();
    }


    inline int getNumberOfPoints() const{
        return nrOfInstances;
    }

    inline int getNumberOfFeatures() const{
        return nrOfFeatures;
    }

    inline int getNumberOfLabels() const{
        return nrOfLabels;
    }
    const vector<int>& getLabels(int uid) const{
        return this->labels[uid];
    }

    inline const unordered_map<int,float>& getFeatures(int uid) const{
        return this->features[uid];
    }

    // ------- Index -----
    void makeIndex(){
        indexFeatures();
        indexLabels();
    }

    void indexFeatures() {
        //index features
        features_inv.clear();
        this->features_inv.reserve(nrOfFeatures);
        for (int i = 0; i < nrOfInstances; i++) {
            const unordered_map<int, float> &featuresI = features.at(i);
            for (const auto &featureValuePair: featuresI) {
                int feature = featureValuePair.first;
                float featureVal = featureValuePair.second; //Add feature value in index
                auto it = this->features_inv.find(feature);
                if (it != this->features_inv.end()) {
                    it->second.push_back(make_pair(i, featureVal));
                } else {
                    this->features_inv[feature] = vector<pair<int, float>>{make_pair(i, featureVal)};
                }
            }
        }
    }

    void indexLabels(){
        //index labels
        labels_inv.clear();
        labels_inv.reserve(nrOfLabels);
        for(int i=0; i< nrOfInstances; i++) {
            const vector<int> &labelsI = labels.at(i);
            for (int label: labelsI) {
                auto it = labels_inv.find(label);
                if (it != labels_inv.end()) {
                    it->second.push_back(i);
                } else {
                    labels_inv[label] = vector<int>{i};
                }
            }
        }
    }


    inline const vector<pair<int,float>>& getTrainRowsWithFeatures(int feature) const{
        auto it = this->features_inv.find(feature);
        if(it == this->features_inv.end()){
            return empty_vec;
        }
        else{
            return it->second;
        }
    }

    const unordered_map<int,vector<pair<int,float>>>& getFeaturesInverseMap() const{
        return features_inv;
    }

    const unordered_map<int,vector<int>>& getLabelsInverted() const{
        return this->labels_inv;
    }

    // ------ normalise features (for RowKNN, for each row, for FeatureKNN, for each column) ------
    vector<float> normalize_rows(){
        //bug, was L1 normalisation + should also update new index!
        /*
        for(auto& featureMap: this->features){
            float norm = sqrt(featureMap.size());
            for(const auto& pair: featureMap){
                int feature = pair.first;
                float value = pair.second;
                featureMap[feature] = value / norm;
            }
        }
         */
        vector<float> norms;
        for(int i=0; i<nrOfInstances; i++){
            unordered_map<int,float>& featuresI = features[i];
            //compute L2 norm
            float l2NormInstance= 0.0;
            for(pair<const int,float>& pair: featuresI){
                l2NormInstance += pair.second * pair.second;
            }
            l2NormInstance = sqrt(l2NormInstance);
            //update data
            for(pair<const int,float>& pair2: featuresI){
                pair2.second = pair2.second / l2NormInstance;
            }
            norms.push_back(l2NormInstance);
        }
        //update index: easier this way
        if(!features_inv.empty())
            indexFeatures();
        return norms;
    }

    void unnormalize_rows(vector<float> norms){
        for(int i=0; i<nrOfInstances; i++){
            unordered_map<int,float>& featuresI = features[i];
            //update data
            for(pair<const int,float>& pair2: featuresI){
                pair2.second = pair2.second * norms[i];
            }
        }
        //update index: easier this way
        if(!features_inv.empty())
            indexFeatures();
    }

    unordered_map<int,float>  normalize_features(){
        unordered_map<int,float> featureNorms;
        featureNorms.reserve(nrOfFeatures);
        if(features_inv.empty()){
            throw invalid_argument("normalize_features() can only be run on Data that is indexed");
        }
        //for each feature
        for(auto& pair1: features_inv){
            //compute norm
            int feature = pair1.first;
            vector<pair<int,float>>& rows = pair1.second;
            float sumOfSquares = 0.0;
            for(auto& pair2: rows){
                float featureValue = pair2.second;
                sumOfSquares += featureValue*featureValue;
            }
            float norm = sqrt(sumOfSquares);
            featureNorms[feature] = norm; //norms are also returned
            //apply norm to both data and index
            for(auto& pair2: rows){
                int docId = pair2.first;
                float featureValue = pair2.second;
                features[docId][feature] = features[docId][feature] / norm; //apply norm to data
                pair2.second = pair2.second / norm; //apply norm to index
            }
        }
        return featureNorms;
    }

    /**
     * For normalizing test features using norms calculated on training data
    * @param featureNorms
    */
    void normalize_features(const unordered_map<int,float>& featureNorms){
        for(unsigned int i=0; i<nrOfInstances; i++) {
            unordered_map<int,float>& features_instance = features[i];
            for(const auto& feature_value_pair: features_instance){
                int feature = feature_value_pair.first;
                float norm = 1.0;
                if(featureNorms.find(feature) != featureNorms.end()){
                    norm = featureNorms.at(feature);
                }
                //if feature only occurs in test data -> no normalisation
                features_instance[feature] =  features_instance[feature] / norm;
            }
        }
        //check if index
        if(!features_inv.empty()){
            throw invalid_argument("For indexed data you can not apply normalize_features(norms).");
        }
    }

    // ------ Statistics ------
    float getLabelCardinality() const{
        float cntLabels = 0;
        for (int i = 0; i < nrOfInstances; i++) {
            cntLabels += this->labels[i].size();
        }
        float avgLabels = cntLabels / (float)nrOfInstances;
        return avgLabels;
    }

    unsigned long getMinimalNumberOfLabels() const{
        unsigned long minimum = 10000000;
        for (int i = 0; i < nrOfInstances; i++) {
            minimum = min(this->labels[i].size(),minimum);
        }
        return minimum;
    }

    unsigned long  getMaximalNumberOfLabels() const{
        unsigned long  maximum = 0;
        for (int i = 0; i < nrOfInstances; i++) {
            maximum = max(this->labels[i].size(),maximum);
        }
        return maximum;
    }

    //note: copy paste previous
    float getFeatureCardinality() const{
        float cntFeatures = 0;
        for (int i = 0; i < nrOfInstances; i++) {
            cntFeatures += this->features[i].size();
        }
        float avgFeatures = cntFeatures / (float)nrOfInstances;
        return avgFeatures;
    }

    float getAvgLabelDensity() const{
        float totalDensity = 0.0;
        for(auto& it: labels_inv){ //ignores labels that do not occur
            totalDensity += it.second.size() / (float) nrOfInstances;
        }
        float avgDensity = totalDensity / (float) nrOfLabels;
        if(labels_inv.size() == 0){
            avgDensity = -1;
        }
        return avgDensity;
    }

    //note: copy paste previous
    float getAvgFeatureDensity() const{
        float totalDensity = 0.0;
        for(auto& it: this->features_inv){ //ignores features that do not occur
            totalDensity += it.second.size() / (float) nrOfInstances;
        }
        float avgDensity = totalDensity / (float) nrOfFeatures;
        if(this->features_inv.size() == 0){
            avgDensity = -1;
        }
        return avgDensity;
    }

    void printStatistics(){
        cout << "#instances: " << nrOfInstances << " #features: " << nrOfFeatures << " #labels: " << nrOfLabels << endl;
        cout << "Stats " << filename << endl
             << "     label cardinality:" << setprecision(3) << getLabelCardinality() << endl
             << "     minimal number of labels:" << getMinimalNumberOfLabels() << endl
             << "     maximal number of labels:" << getMaximalNumberOfLabels() << endl
             << "     label avg density:" << setprecision(3) << getAvgLabelDensity() << endl
             << "     feature cardinality:" << setprecision(3) << getFeatureCardinality() << endl
             << "     feature avg density:" << setprecision(3) << getAvgFeatureDensity() << endl;
    }

    void clear(){
        features.clear();
        labels.clear();
        features_inv.clear();
        labels_inv.clear();
    }

    void join(Data & other){
        features.insert(features.end(),other.features.begin(),other.features.end());
        labels.insert(labels.end(), other.labels.begin(), other.labels.end());
        nrOfInstances += other.nrOfInstances;
        makeIndex();
        nrOfFeatures = features_inv.size();
        nrOfLabels = labels_inv.size();
    }

private:
    vector<pair<int,float>> empty_vec;

public:
    bool verbose = false;
private:
    //data
    string filename; //LIBSVM FORMAT
    int nrOfInstances=-1;
    int nrOfFeatures=-1;
    int nrOfLabels=-1;
    vector<unordered_map<int,float>> features;
    vector<vector<int>> labels;
    //index:
    unordered_map<int,vector<int>> labels_inv; //for feature KNN, see FeatureKNN.computeSimilarityMatrix
    unordered_map<int,vector<pair<int,float>>> features_inv; //CONTAINS FEATURE value -> Much faster! e.g. factor 4-5
};

#endif //LCIF_KNN_JOURNAL_DATA_H
