//
// Created by Len Feremans on 10/09/2018.
//

#ifndef LCIF_KNN_JOURNAL_ROWKNN_H
#define LCIF_KNN_JOURNAL_ROWKNN_H


#include <vector>
#include <unordered_map>
#include <algorithm>
#include <functional>
#include <iostream>
#include <numeric>
#include <utility>
#include <math.h>
#include <chrono>
#include <thread>

#include "Data.h"
#include "Utils.h"

using namespace std;

/**
 * Technically this code is redundant, that is calling RowKNNPruning with topM=very high, should give same result
 * Remark: had binary mode, but not used for now, so removed.
 */
class RowKNN{

public:

    RowKNN(Data& trainData, Data& testData): trainData(trainData), testData(testData){
        cout << "RowKNN. #rows train: " << trainData.getNumberOfPoints() << " #rows test: " << testData.getNumberOfPoints() << endl;
    }

    void setParameters(unsigned int topK){ // bool binary
        this->topK = topK;
        //this->binary = binary;
    }

    void computeSimilarityInstance(int test_uid){
        const unordered_map<int,float>& featuresTest = testData.getFeatures(test_uid);
        unordered_map<int,float> similsTestInstance;
        similsTestInstance.reserve(1000);
        for(const auto& pair1: featuresTest){
            int featureTest = pair1.first;
            float featureTestvalue = pair1.second;
            //SLOW: speed-down of factor 5
            //const vector<int>& training_rows_with_feature = trainData.getTrainRowsWithFeatures(featureTest);
            //for(int train_uid: training_rows_with_feature){
            //    const unordered_map<int,float>& featuresTrain = trainData.getFeatures(train_uid);
                // float featureTrainValue = featuresTrain.at(featureTest);
            //FAST:
            const vector<pair<int,float>>& training_rows_with_feature = trainData.getTrainRowsWithFeatures(featureTest);
            for(const auto& pair2: training_rows_with_feature){
                int train_uid = pair2.first;
                float featureTrainValue = pair2.second;
               // update partial dot product
               if(similsTestInstance.find(train_uid) == similsTestInstance.end()){
                    similsTestInstance[train_uid] = featureTestvalue * featureTrainValue;
                }
                else{
                    similsTestInstance[train_uid] += featureTestvalue * featureTrainValue;
                }
            }
        }
        if(simMatrixTopK.size() <= test_uid){
            simMatrixTopK.resize(test_uid+1);
        }
        simMatrixTopK[test_uid] = sortTopKPartialOrder(similsTestInstance, topK);
        //if(test_uid == 0) {
        //    cout << "#simils " << similsTestInstance.size() << endl;
        //    cout << "simils top k:" << to_string(simMatrixTopK[test_uid]) << endl;
        //}
    }

    //note: copy-pasted from FeatureKNN
    void computeSimilarityMatrix(unsigned int noThreads){
        auto start = chrono::steady_clock::now();
        simMatrixTopK.resize(testData.getNumberOfPoints());
        vector<thread> threads;
        unsigned int loopsize =  testData.getNumberOfPoints() / noThreads + 1;
        unsigned int start_index=0;
        unsigned int end_index= start_index + loopsize;
        if(end_index > testData.getNumberOfPoints()){
            end_index = testData.getNumberOfPoints();
        }
        for(unsigned int threadid = 0; threadid< noThreads; threadid++){
            threads.push_back( thread(&RowKNN::computeSimilarityMatrixRange,
                                      this, threadid, start_index, end_index));
            if(verbose)
                cout << " Adding thread " << threadid << " for test instances " << start_index << "-" << end_index << endl;
            start_index += loopsize;
            end_index = start_index + loopsize;
            if(start_index >= testData.getNumberOfPoints()){
                break;
            }
            if(end_index > testData.getNumberOfPoints()){
                end_index = testData.getNumberOfPoints();
            }
        }
        for(auto& t : threads){
            t.join();
        }
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
        if(verbose)
            cout << ">>RowKNN.computeSimilarityMatrix took " << elapsed.count() << "ms" << endl;
    }

    void computeSimilarityMatrixRange(int threadid, unsigned int start_range, unsigned int end_range){
        auto start_start = chrono::steady_clock::now();
        auto start = chrono::steady_clock::now();
        for(unsigned int i=start_range; i < end_range; i++){
            if(i % 1000 == 0){
                auto end = chrono::steady_clock::now();
                auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
                if(verbose)
                    cout << ">>RowKNN.computeSimilarityMatrix thread #" << threadid << ": "
                     << setw(6) << (i - start_range) <<  "/" << (end_range - start_range)
                     << " took " << elapsed.count() << "ms" << endl;
                start = end;
            }
            //if(binary)
            //    computeSimilarityInstanceBinary(i);
            computeSimilarityInstance(i);
        }
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start_start);
        if(verbose)
            cout << ">>RowKNN.computeSimilarityMatrix thread " << threadid << " took " << elapsed.count() << "ms" << endl;
    }

    const vector<pair<int,float>>& getTopKNeighbours(int test_uid) const{
        return simMatrixTopK[test_uid];
    }

    vector<vector<pair<int,float>>>& getKNNMatrix(){
        return simMatrixTopK;
    }

public:
    bool verbose = false;

private:
    //parameters
    unsigned int topK;
    //bool binary;

    //input data
    Data& trainData; //training
    Data& testData;

    //output
    vector<vector<pair<int,float>>> simMatrixTopK;
};



#endif //LCIF_KNN_JOURNAL_ROWKNN_H
