//
// Created by Len Feremans on 17/10/2019.
//

#ifndef LCIF_KNN_JOURNAL_FILETRANSFORMADDSTATISTICS_H
#define LCIF_KNN_JOURNAL_FILETRANSFORMADDSTATISTICS_H

#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <vector>
#include <unordered_map>
#include <cstdio>

#include "Utils.h"

using namespace std;
/**
 * In XML (Extreme Multi-Label) repo, first line contains number of rows, features and labels, and is used
 * If loading LIBSVM format without this parameters, add them
 */
class FileTransformAddStatistics {
public:

    //compute nrOfInstances, nrOfFeatures and nrOfLabels and save this in filename
    void transform(string filename) {
        long nrOfInstances = 0;
        set<int> featuresSet;
        set<int> labelsSet;
        string line;
        vector<int> labelsLine;
        unordered_map<int, float> featuresLine;
        ifstream myfile(filename);
        //TODO IF Time experiment with buffer sizes, e.g.
        //see https://stackoverflow.com/questions/5166263/how-to-get-iostream-to-perform-better:
        //char Buffer[N];
        //ifstream file("file.txt");
        //file.rdbuf()->pubsetbuf(Buffer, N);
        if (!myfile.is_open()) {
            throw invalid_argument("File can not be read");
        }
        while (getline(myfile, line)) {
            nrOfInstances++;
            labelsLine.clear();
            featuresLine.clear();
            parseLineLibSVM(line, labelsLine, featuresLine);
            for (int label: labelsLine) {
                labelsSet.insert(label);
            }
            for (auto &pair: featuresLine) {
                featuresSet.insert(pair.first);
            }
        }
        myfile.close();
        //save file
        string header =
                to_string(nrOfInstances) + " " + to_string(featuresSet.size()) + " " + to_string(labelsSet.size());
        saveToBeginning(filename, "./temp/temp_data", header);
        cout << "Added header " << header << " to " << filename << endl;
    }

private:
    //from https://stackoverflow.com/questions/11108238/adding-text-and-lines-to-the-beginning-of-a-file-c
    void saveToBeginning(string input, string tempfilename, string line) {
        ofstream outputFile(tempfilename);
        ifstream inputFile(input);
        string tempString;
        outputFile << line << endl;
        outputFile << inputFile.rdbuf();
        inputFile.close();
        outputFile.close();
        remove(input.c_str());
        rename(tempfilename.c_str(), input.c_str());
    }
};


#endif //LCIF_KNN_JOURNAL_FILETRANSFORMADDSTATISTICS_H
