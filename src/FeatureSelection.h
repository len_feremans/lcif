// Created by Len Feremans on 21/06/2019.
//

#ifndef LCIF_KNN_JOURNAL_FEATURESELECTION_H
#define LCIF_KNN_JOURNAL_FEATURESELECTION_H


#include <vector>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <functional>
#include <iterator>
#include <iostream>
#include <numeric>
#include <utility>
#include <math.h>
#include <chrono>
#include <thread>
#include <exception>
#include "Data.h"
#include "Utils.h"
#include "Facade.h"
using namespace std;



class FeatureSelection {

public:
    FeatureSelection(){
    }

    /**
     * Returns best representation, that is return "K" best features, measured using entropy
     *
     * @param filenameTrain
     * @param filenameTest
     * @return
     */
    int search_best_representation(string filenameTrain, string filenameTest, string metricToOptimizeOn, int sampleTest){
        Data train0 = Data(filenameTrain);
        train0.makeIndex();
        Data test0 = Data(filenameTest, sampleTest);
        unordered_map<int,float> featureScores = computeEntropyAllFeatures(train0); //feature selected only using training data
        int kRow = 100, kFeature=10;
        float kRowPower=1.0, kFeaturePower=1.0, fixedLambda = 0.5; //defaults
        float maxScoreRow=-1,maxScoreFeature=-1,maxScoreLCIF=-1;
        int maxKRow=-1,maxKFeature=-1,maxKLCIF=-1;
        //TODO: or seperate topK selection for feature/instance based?
        Facade facade;
        for(float kFeatureSelectionPercentage: kFeatureSelectionRange) {
            //select top k features
            int kFeatureSelection = round(kFeatureSelectionPercentage * train0.getNumberOfFeatures());
            vector<pair<int, float>> topKFeatures = computeTopK(featureScores, kFeatureSelection);
            string train_output = filenameTrain + "_top" + to_string(kFeatureSelection) + ".csv";
            string test_output = filenameTest + "_top" + to_string(kFeatureSelection) + "_sample.csv";
            transformDataKeepTopKFeatures(train0, topKFeatures, train_output);
            transformDataKeepTopKFeatures(test0, topKFeatures, test_output);
            //run LCIF
            //evaluation
            tuple<float,float,float> results = facade.run_lcif(train_output, test_output, fixedLambda, kRow, kRowPower
                    , kFeature, kFeaturePower, metricToOptimizeOn, sampleTest, false);
            if (verbose) cout <<  "\033[1;30m" << "GS Representation: " << " *k:" << kFeatureSelection << " (" << kFeatureSelectionPercentage << ") "
                              << metricToOptimizeOn << " row:" << get<0>(results) << ", feat:" << get<1>(results)
                              << ", lcif:" << get<2>(results) <<  "\033[0m" << endl;
            if(get<0>(results) > maxScoreRow){
                maxKRow = kFeatureSelection;
                maxScoreRow = get<0>(results);
            }
            if(get<1>(results) > maxScoreFeature){
                maxKFeature = kFeatureSelection;
                maxScoreFeature = get<1>(results);
            }
            if(get<2>(results) > maxScoreLCIF){
                maxKLCIF = kFeatureSelection;
                maxScoreLCIF = get<2>(results);
            }
        }
        cout <<  "\033[1;30m" << "Best top-k features using entropy."
             << " RowKNN: " << maxKRow << " (" << metricToOptimizeOn << "=" << maxScoreRow << ")"
             << " FeatureKNN: " << maxKFeature << " (" << metricToOptimizeOn << "=" << maxScoreFeature << ")"
             << " LCIF: " << maxKLCIF << " (" << metricToOptimizeOn << "=" << maxScoreLCIF << ")" <<  "\033[0m" << endl;
        return maxKLCIF;
    }

    /**
    *  Voor elk attribuut:
    Bereken projectie op attribuut, e.g. alle rijen met feature_x=1
    Bereken entropie van class labels, e.g. als A,B,C
    -> als D niet voorkomt, entropy = 0
    -> Als, A,B,C voorkomen, dan
            #A/#size-projectie * log2( #A/#size-projectie)
    +dito B en C, dan negatief maken
    */
    unordered_map<int,float> computeEntropyAllFeatures(Data& data) {
        auto &features_inv = data.getFeaturesInverseMap();
        int nrOfFeatures = data.getNumberOfFeatures();
        unordered_map<int,float> featureScores;
        featureScores.reserve(data.getNumberOfFeatures());
        auto start = chrono::steady_clock::now();
        unordered_map<int, int> labelCounts;
        int i = 0;
        for (const auto &pair1 : features_inv) {
            if (i % 10000 == 0 && i!=0) {
                auto end = chrono::steady_clock::now();
                auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
                if (verbose && elapsed.count() > 100) {
                    cout << "\033[0;37m" << "Processing features " << i << "/" << nrOfFeatures
                         << " took " << elapsed.count() << "ms" << "\033[0m" << endl;
                }
            }
            labelCounts.clear(); //clear!
            int feature = pair1.first;
            const vector<pair<int,float>> &rows = pair1.second; //contains all 'documents' matching with feature + value of that feature
            float noRows = (float) rows.size();
            for (unsigned int i = 0; i < rows.size(); i++) {
                int row = rows[i].first;
                const vector<int> &labels = data.getLabels(row);
                for (auto label : labels) {
                    labelCounts[label] += 1;
                }
            }
            float mutual_info = 0.0;
            for (const auto &pair : labelCounts) {
                int label = pair.first;
                int cnt = pair.second;
                float ratio = cnt / noRows;
                mutual_info += ratio * log2(ratio);
            }
            featureScores[feature] = mutual_info; //lower is beter
            i += 1;
        }
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
        cout << "\033[0;37m" << ">>FeatureSelectiono.computeEntropyAllFeatures took " << elapsed.count() << "ms" << "\033[0m" << endl;
        return featureScores;
    }

    vector<pair<int, float>> computeTopK(unordered_map<int,float> featureScores, int topK) {
        vector<pair<int, float>> result = sortTopKPartialOrder(featureScores, topK);
        return result;
    }

    void transformDataKeepTopKFeatures(const Data& trainOrTestData, vector<pair<int, float>>& topK, string fileOutput) {
        ofstream myfile(fileOutput);
        if (!myfile.is_open()) {
            throw "Error saving output";
        }
        unordered_set<int> topKFeatureSet;
        topKFeatureSet.reserve(topK.size());
        for (auto& pair : topK) {
            topKFeatureSet.insert(pair.first);
        }
        unordered_map<int, float> features_new;
        string header = to_string(trainOrTestData.getNumberOfPoints()) + " " + to_string(topK.size()) + " " + to_string(trainOrTestData.getNumberOfLabels());
        myfile << header << endl;
        for (int i = 0; i < trainOrTestData.getNumberOfPoints(); i++) {
            const unordered_map<int, float>& features = trainOrTestData.getFeatures(i);
            const vector<int>&  labels = trainOrTestData.getLabels(i);
            features_new.clear();
            for (auto& pair : features) {
                if (topKFeatureSet.find(pair.first) != topKFeatureSet.end()) {
                    features_new[pair.first] = pair.second;
                }
            }
            for (const int label : labels) {
                myfile << label << " ";
            }
            if (features_new.size() == 0) {
                //continue; OLD
                myfile <<  1234567 << ":" << 1.0 << " ";
            }
            for (auto& pair : features_new) {
                myfile << pair.first << ":" << pair.second << " ";
            }
            myfile << endl;
        }
        myfile.close();
    }

public:
    vector<float> kFeatureSelectionRange = {0.01, 0.1, 0.25, 0.5, 0.75, 0.99, 1.0};
    bool verbose = true;
};


#endif //LCIF_KNN_JOURNAL_FEATURESELECTION_H
