//
// Created by Len Feremans on 10/09/2018.
//

#ifndef LCIF_KNN_JOURNAL_UTILS_H_H
#define LCIF_KNN_JOURNAL_UTILS_H_H

#include <vector>
#include <set>
#include <unordered_map>

#include <algorithm>
#include <functional>
#include <iterator>

#include <iostream>
#include <iomanip>
#include <sstream>

using namespace std;


string to_string(const vector<int> &data);

string to_string(const vector<string> &data);

string to_string(const vector<float> &data);

string to_string(const set<int> &data);

string to_string(const unordered_map<int,float> &data);

string to_string(const vector<pair<int,float>> &data);

string to_string(const vector<pair<int,int>> &data);

vector<string> split(const string &text, const vector<char>& seperators);


bool sortByWeightDescending(pair<int, float> p1, pair<int, float> p2);

bool sortByWeightAscending(pair<int,float> p1, pair<int,float> p2);

vector<pair<int,float>> sortTopKPartialOrder(const unordered_map<int,float>& myunordered_map, unsigned long topK);

vector<pair<int,float>> sortTopKPartialOrder(vector<pair<int,float>>& myvector, unsigned long topK);

vector<string> splitFile(string filename, long partition_size, bool skipFirstLine);

void parseLineLibSVM(const string& line, vector<int>& labels, unordered_map<int,float>& features);

#endif //LCIF_KNN_JOURNAL_UTILS_H_H
