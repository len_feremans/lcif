//
// Created by Len Feremans on 11/10/2019.
//

#ifndef LCIF_KNN_JOURNAL_SINGLETHRESHOLD_H
#define LCIF_KNN_JOURNAL_SINGLETHRESHOLD_H

#include <vector>
#include <set>
#include <unordered_map>

#include <iostream>

#include "Data.h"
#include "Utils.h"

/**
 * Compute single threshold that minimizes the difference between label cardinality in the actual and predicted labels
 */
class SingleThreshold {

public:
    bool verbose=true;

    SingleThreshold(const Data& testDataArg): testData(testDataArg){}

    float findOptimalThreshold(const vector<vector<pair<int, float>>>& predictions){
        float labelCardinality = testData.getLabelCardinality();
        vector<float> t = {0.0f, 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.9f};
        float bestT = 0.0;
        float bestDiff = 99999.0;
        //for each threshold, compute difference in label cardinality, take threshold with lowest error
        for(float threshold: t){
            float predictedLabelCardinality = computeLabelCardinalityPredictions(threshold, predictions);
            float error = abs(predictedLabelCardinality-labelCardinality);
            if(error < bestDiff){
                bestT = threshold;
                bestDiff = error;
            }
        }
        vector<float> t_finegrained = {bestT-0.05f, bestT-0.04f, bestT-0.03f, bestT-0.02f, bestT-0.01f, bestT,
                           bestT+0.01f, bestT+0.02f, bestT+0.03f,bestT+0.04f,bestT+0.05f};
        bestDiff = 99999.0;
        for(float threshold: t_finegrained){
            float predictedLabelCardinality = computeLabelCardinalityPredictions(threshold, predictions);
            float error = abs(predictedLabelCardinality-labelCardinality);
            if(error < bestDiff){
                bestT = threshold;
                bestDiff = error;
            }
        }
        if(verbose)
            cout << "Optimal threshold:" << setprecision(2) << bestT
             << " Label cardinality:" << setprecision(3) << labelCardinality
             << " Predictions label cardinality:" << setprecision(3)
             << computeLabelCardinalityPredictions(bestT, predictions) << endl;
        return bestT;
    }

    vector<vector<pair<int, float>>> applyThreshold(bool atLeastOneScoreRule,
                                                    float threshold, const vector<vector<pair<int, float>>>& predictions){
        vector<vector<pair<int, float>>> allPredictions;
        allPredictions.reserve(predictions.size());
        for(const auto& predictionsNotThresholdedI: predictions){
            vector<pair<int, float>> predictionsI;
            for(const auto& pair: predictionsNotThresholdedI){
                if(pair.second > threshold){
                    predictionsI.emplace_back(pair);
                }
            }
            if(atLeastOneScoreRule && predictionsI.size() == 0 && predictionsNotThresholdedI.size() >= 1){
                predictionsI.emplace_back(predictionsNotThresholdedI[0]);
            }
            allPredictions.emplace_back(predictionsI);
        }
        return allPredictions;
    };

private:
    float computeLabelCardinalityPredictions(float threshold, const vector<vector<pair<int, float>>>& predictions){
        //for each test instance
        float cntPredictedTotal = 0;
        for(unsigned int i=0; i<testData.getNumberOfPoints(); i++){
            const vector<int>& labels = testData.getLabels(i);
            const vector<pair<int, float>>& predictionsInstance = predictions.at(i);
            //count the number of predictable label, that is label, where the score is higher than threshold
            int cntPredicted = 0;
            for(unsigned int j=0; j<predictionsInstance.size(); j++){
                double labelPredictionScore = predictionsInstance[j].second;
                if(labelPredictionScore > threshold){
                    cntPredicted+=1;
                }
            }
            //add to total
            cntPredictedTotal += cntPredicted;
        }
        //average
        float avgPredicted = cntPredictedTotal / (float)testData.getNumberOfPoints();
        return avgPredicted;
    }

private:
    const Data& testData;
};


#endif //LCIF_KNN_JOURNAL_SINGLETHRESHOLD_H
