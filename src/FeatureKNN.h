//
// Created by Len Feremans on 10/09/2018.
//

#ifndef LCIF_KNN_JOURNAL_FEATUREKNN_H
#define LCIF_KNN_JOURNAL_FEATUREKNN_H

#include <vector>
#include <set>
#include <unordered_map>

#include <algorithm>
#include <functional>
#include <iterator>

#include <iostream>

#include <numeric>
#include <utility>
#include <math.h>
#include <chrono>

#include <thread>

#include "Data.h"
#include "Utils.h"
using namespace std;

class FeatureKNN{
public:

    FeatureKNN(const Data& data): data(data){
    }

    FeatureKNN(const Data& data, bool verbose): verbose(verbose), data(data){
    }


    //assumes data.normalize_features() was called
    void computeSimilarityMatrix(int noThreads){
        auto start = chrono::steady_clock::now();
         if(data.getNumberOfLabels() < noThreads){
            noThreads = data.getNumberOfLabels()/2;
        }
        //init matrix rows
        for(const auto& labelPair: data.getLabelsInverted()){
            this->simMatrixLabels[labelPair.first] = unordered_map<int,float>();
        }
        vector<thread> threads;
        unsigned int loopsize =  data.getNumberOfLabels() / noThreads + 1;
        unsigned int start_index=0;
        unsigned int end_index=  start_index + loopsize;
        //note: there might be 100 labels in header file, but only 98 that occur in train data (and the other 2 only occuring in test data),
        //so add extra check here!
        int max_size = min((unsigned long)data.getNumberOfLabels(),data.getLabelsInverted().size());
        if(end_index > max_size){
            end_index = max_size;
        }
        for(int threadid = 0; threadid< noThreads; threadid++){
            threads.push_back( thread(&FeatureKNN::computeSimilarityMatrixRange,
                                      this, threadid, start_index, end_index));
            if(verbose && data.getNumberOfPoints() > 100000)
                cout <<  "\033[0;37m" << " Adding thread " << threadid << " for labels "
                     << start_index << "-" << end_index <<  "\033[0m" << endl;
            start_index += loopsize;
            end_index = start_index + loopsize;
            if(start_index >= data.getNumberOfLabels()){
                break;
            }
            if(end_index > data.getNumberOfLabels()){
                end_index = data.getNumberOfLabels();
            }
        }
        for(auto& t : threads){
            t.join();
        }
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
        //for computeScoreSingleNew
        if(simMatrixLabelsInv.empty()){
            for(const auto& pair1: simMatrixLabels){
                int label = pair1.first;
                for(const auto& pair2: pair1.second){
                    int feature = pair2.first;
                    float simil = pair2.second;
                    simMatrixLabelsInv[feature][label] = simil;
                }
            }
        }
        if(verbose)
            cout << "\033[0;37m" << ">>FeatureKNN.computeSimilarityMatrix took "
                 << elapsed.count() << "ms" <<  "\033[0m" << endl;
    }

    void computeSimilarityMatrixRange(int threadid, unsigned int start_range, unsigned int end_range){
        auto labelsInverted = data.getLabelsInverted();
        //auto startIt = next(labelsInverted.begin(),start_range);
        //auto endEnd = next(labelsInverted.begin(),end_range);
        unordered_map<int, vector<int>>::iterator startIt = labelsInverted.begin();
        for(int i=0; i<start_range && startIt != labelsInverted.end(); i++){
            startIt++;
        }
        unordered_map<int, vector<int>>::iterator endIt = labelsInverted.begin();
        for(int i=0; i<end_range && endIt != labelsInverted.end(); i++){
            endIt++;
        }
        int i =start_range;
        auto start = chrono::steady_clock::now();
        for(auto& it=startIt; it!=endIt; it++){
            if(i % 1000 == 0 && i!=0){
                auto end = chrono::steady_clock::now();
                auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
                if(verbose &&  elapsed.count() > 100)
                    cout  << "\033[0;37m" << ">>FeatureKNN.computeSimilarityMatrix thread #" << threadid << ": "
                     << setw(6) << (i - start_range) <<  "/" << (end_range - start_range)
                     << " took " << elapsed.count() << "ms" <<  "\033[0m" << endl;
                start = end;
            }
            int label = it->first;
            const vector<int>& trainingRowsWithLabel = it->second;
            float labelInverseNorm = 1.0 / sqrt(trainingRowsWithLabel.size()); //all 1, so sum-of-squares is size()
            for(int uid: trainingRowsWithLabel){
                const unordered_map<int,float>& rowFeatures = data.getFeatures(uid);
                for(const auto& featureValuePair: rowFeatures){
                    int feature = featureValuePair.first;
                    float normalizeValue = featureValuePair.second;
                    _updateSimMatrixPartialDotProduct(label,feature,normalizeValue * labelInverseNorm);
                }
            }
            i++;
        }
    }

    //note: could choose to index first by feature, or by label, not label, because of multi-threading
    inline void _updateSimMatrixPartialDotProduct(int label, int feature, float term){
        //sim_matrix_mutex.lock(); //lock if new feature map is added
        //sim_matrix_mutex.unlock();
        unordered_map<int,float>& labelRow = simMatrixLabels[label];
        if(labelRow.find(feature) == simMatrixLabels[label].end()) {
            simMatrixLabels[label][feature] = term;
        }
        else {
            simMatrixLabels[label][feature] += term;
        }
    }

    //needed for scoring test instances
    void computeSortedSimilarityMatrixForLabels(){
        auto start = chrono::steady_clock::now();
        simMatrixLabelsSorted.reserve(simMatrixLabels.size());
        for(const auto& labelpair: simMatrixLabels){
            int label = labelpair.first;
            simMatrixLabelsSorted[label] = vector<pair<int,float>>();
            for(const auto& featureScorePair: simMatrixLabels[label]){
                int feature = featureScorePair.first;
                double simil = featureScorePair.second;
                simMatrixLabelsSorted[label].push_back(make_pair(feature, simil));
            }
            sort(simMatrixLabelsSorted[label].begin(), simMatrixLabelsSorted[label].end(), mycomparator);
        }
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
        if(verbose && elapsed.count() > 100)
            cout <<  "\033[0;37m" << ">>FeatureKNN.computeSortedSimilarityMatrixForLabels took "
                 << elapsed.count() << "ms" << endl <<  "\033[0m";
        return;
    }

    bool static mycomparator(pair<int,float> p1, pair<int,float> p2){
        return p1.second > p2.second;
    }

    //needed for scoring test instances
    const vector<pair<int,float>>& getSortedSimmilaritiesForLabel(int label) const{
        if(simMatrixLabelsSorted.find(label) == simMatrixLabelsSorted.end()){
            return empty_vec;
        }
        return simMatrixLabelsSorted.at(label);
    }


    //unordered_map<int,float>& getSimmilaritiesForFeature(int feature){
    //    return simMatrix[feature];
    //};


    /*
     * Compute candidate labels based on top-k similarities for each feature.
     * Best to cache it
     */
    void computeTopKSortedSimilarityMatrixForFeatures(int selectK){
        auto start = chrono::steady_clock::now();
        simMatrixFeaturesTopK.reserve(data.getNumberOfFeatures());
        for(const auto& pair1: simMatrixLabels){
            int label = pair1.first;
            for(const auto& pair2: simMatrixLabels[label]){
                int feature = pair2.first;
                double simil = pair2.second;
                if(simMatrixFeaturesTopK.find(feature) == simMatrixFeaturesTopK.end()){
                    simMatrixFeaturesTopK[feature] = vector<pair<int,float>>();
                }
                simMatrixFeaturesTopK[feature].push_back(make_pair(label,simil));
            }
        }
        for(const auto& pair3: simMatrixFeaturesTopK){
            int feature = pair3.first;
            simMatrixFeaturesTopK[feature] = sortTopKPartialOrder(simMatrixFeaturesTopK[feature], selectK);
        }
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
        if(verbose && elapsed.count() > 100)
            cout << "\033[0;37m" << ">>FeatureKNN.computeSimilarityMatrixTopK took "
                 << elapsed.count() << "ms" <<  "\033[0m" << endl;
        return;
    }

    const vector<pair<int,float>>& getTopKSimmilaritiesForFeature(int feature) const{
        if(simMatrixFeaturesTopK.find(feature) == simMatrixFeaturesTopK.end()){
            return empty_vec;
        }
        return simMatrixFeaturesTopK.at(feature); //stupid in C++, at allows const, [] not...
    }

    /*
     * Computes candidate labels, as the union of labels, based on top-k nearest neighbours
     */
    set<int> computeCandidateLabels(const unordered_map<int,float>& features) const{
        set<int> candidateLabels;
        for(const auto & pair_feature: features){
            int feature = pair_feature.first;
            const vector<pair<int,float>>& topKMostSimilarLabels = getTopKSimmilaritiesForFeature(feature);
            for(const auto& pair_similar: topKMostSimilarLabels){
                int label = pair_similar.first;
                candidateLabels.insert(label);
            }
        }
        return candidateLabels;
    }

    /*
     * Compute score for test instance.
     * First compute candidate labels, based on features.
     * For each candidate clabel:
     *      lookup simil(test-feature,clabel) for all test_features in FULL SIM MATRIX
     *      take top-k simils
     *      score = sum of pow(simil,power) * value / k  (TIMES VALUE!), note also: value is normalized
     *      Alternative:
     *          score = sum of pow(simil * value,power) / k
     * Sort top-k-return labels by score, and return
     */
    void computeScoresSingleOld(unsigned int test_uid, const unordered_map<int,float>& features){
        //const vector<int>& candidateLabels = data.getAllLabels(); //super traag
        set<int> candidateLabels = computeCandidateLabels(features);
        unordered_map<int,float> labelScores;
        for(int label: candidateLabels){
            /*
            //version 0:
            unordered_map<int,float> similsLabelAndTestInstanceFeatures;
            for(const auto& pair_feature: features){
                int feature = pair_feature.first;
                similsLabelAndTestInstanceFeatures[feature] = simMatrix[feature][label];
            }
            vector<pair<int,float>> topKSimils = sortTopK(similsLabelAndTestInstanceFeatures, topK);
            */
            //TODO: Think about implementation...
            const vector<pair<int,float>>& similsLabelAndTestInstanceFeatures = this->getSortedSimmilaritiesForLabel(label);
            vector<pair<int,float>> topKSimils;
            for(const pair<int,float>& pair: similsLabelAndTestInstanceFeatures){
                int feature = pair.first;
                if(features.find(feature) != features.end()){
                    topKSimils.push_back(pair);
                    if(topKSimils.size() == topK){
                        break;
                    }
                }
            }
            float score = 0.0;
            for(const auto& pair_simil: topKSimils){
                int feature = pair_simil.first;
                float simil = pair_simil.second;
                //float feature_val = features.at(feature);
                //variant 0:
                //score += pow(simil,power) * feature_val;
                score += pow(simil,power);
            }
            score = score / (float)topK;
            labelScores[label] = score;
        }
        vector<pair<int,float>> scores = sortTopKPartialOrder(labelScores,topKReturn);
        this->predictions[test_uid] = scores;
        return;
    }

    void computeScoresSingle(unsigned int test_uid, const unordered_map<int,float>& features){
        set<int> candidateLabels = computeCandidateLabels(features);
        unordered_map<int,float> labelScores;
        float sumFeatures = 0.0;
        for(const auto& pair1: features){
            int feature = pair1.first;
            float featureValue = pair1.second;
            sumFeatures += featureValue;
            if(simMatrixLabelsInv.find(feature) != simMatrixLabelsInv.end()) { //could be the case
                const unordered_map<int, float> &labelSimils = simMatrixLabelsInv.at(feature);
                for (int label: candidateLabels) { //before was labels (but super-slow)
                    if(labelSimils.find(label) != labelSimils.end()){
                        float sim = labelSimils.at(label);
                        labelScores[label] += pow(sim, power) * featureValue;
                        //labelScores[label] += pow(sim, power) / (float) features.size();
                    }
                }
            }
        }
        vector<pair<int,float>> scores = sortTopKPartialOrder(labelScores,topKReturn); //also less efficient
        for(auto& pair1: scores){
           pair1.second = pair1.second / sumFeatures;
        }
        this->predictions[test_uid] = scores;
        return;
    }


    void traceComputeScores(int test_id, const unordered_map<int,float>& features, int topK, int topKReturn, float power){
        cout << "ItemKNN.computeScoresWithTracing: " << endl;
        set<int> candidateLabels = computeCandidateLabels(features);
        cout << "   Candidate labels: " << to_string(candidateLabels) << endl;
        computeScoresSingle(test_id, features);
        vector<pair<int,float>> scores = predictions[test_id];
        cout << "   Top " << topKReturn << " labels: " << to_string(scores) << endl;
        for(const auto& pair_score: scores){
            int label = pair_score.first;
            cout << " ->Label: " << label << " score " << fixed << setprecision(6) << pair_score.second << endl;
            unordered_map<int,float> similsLabelAndTestInstanceFeatures;
            for(const auto& pair_feature: features){
                int feature = pair_feature.first;
                similsLabelAndTestInstanceFeatures[feature] = simMatrixLabels.at(label).at(feature);
            }
            vector<pair<int,float>> topKSimils = sortTopKPartialOrder(similsLabelAndTestInstanceFeatures, topK);
            cout << "   Top " << topK << " most similar features: " << to_string(topKSimils) << endl;
            cout << "   scores = 1/" << topK << " * (";
            float score = 0.0;
            for(const auto& pair_simil: topKSimils){
                int feature = pair_simil.first;
                float simil = pair_simil.second;
                float feature_val = features.at(feature);
                score += pow(simil,power);
                cout << simil << "^" << power << " (simil " << feature << ") * " << feature_val << " + ";
            }
            cout << endl;
        }
    }

    void setParameters(int topK, int topKReturn, float power){
        this->topK = topK;
        this->topKReturn = topKReturn;
        this->power = power;
    }

    void computeScores(const Data &testData, int noThreads, bool verboseTiming){
        this->verbose = verboseTiming;
        this->predictions.resize(testData.getNumberOfPoints());
        auto start = chrono::steady_clock::now();
        vector<thread> threads;
        unsigned int loopsize =  testData.getNumberOfPoints() / noThreads + 1;
        unsigned int start_index=0;
        unsigned int end_index=min(start_index + loopsize, (unsigned int) testData.getNumberOfPoints());
        for(int threadid = 0; threadid< noThreads; threadid++){
            threads.push_back( thread(&FeatureKNN::computeScoresRange,
                                      this, testData, threadid, start_index, end_index));
            start_index += loopsize;
            end_index = min(start_index + loopsize, (unsigned int) testData.getNumberOfPoints());
        }
        for(auto& t : threads){
            t.join();
        }
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
        if(verbose && elapsed.count() > 100)
            cout  << "\033[0;37m" << ">>FeatureKNN.computeScores took " << elapsed.count() << "ms" <<  "\033[0m" << endl;
        return;
    }

    void computeScoresRange(const Data& testData, int thread_id, unsigned int start_index, unsigned int end_index){
        auto start_start = chrono::steady_clock::now();
        auto start = chrono::steady_clock::now();
        vector<vector<pair<int,float>>> predictions;
        for(unsigned int i=start_index; i<end_index; i++){
            if(i % 1000 == 0 && i!=0){
                auto end = chrono::steady_clock::now();
                auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
                if(verbose && elapsed.count() > 100)
                    cout <<  "\033[0;37m" << ">>FeatureKNN.computeScoresRange thread #" << thread_id << ":"
                         << setw(6) << (i-start_index) <<  "/" << (end_index-start_index)
                         << " took " << elapsed.count() << "ms" <<  "\033[0m" << endl;
                start = end;
            }
            const unordered_map<int,float>& featuresI = testData.getFeatures(i);
            //cout << to_string(featuresI) << endl;
            computeScoresSingle(i, featuresI);
            //cout << to_string(predictionsI) << endl;
        }
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start_start);
        if(verbose && elapsed.count() > 100)
            cout << "\033[0;37m" << ">>FeatureKNN.computeScoresRange took " << elapsed.count() << "ms" <<  "\033[0m" <<endl;
        return;
    }

    const vector<vector<pair<int,float>>>& getPredictions(){
        return this->predictions;
    }

    void clearTrainingData(){
        simMatrixLabels.clear();
    }

private:
    bool verbose = false;
    //mutex sim_matrix_mutex;
    unsigned int topK;
    unsigned int topKReturn;
    float power;
    const Data& data; //training only
    unordered_map<int,unordered_map<int,float>> simMatrixLabels; //training only
    unordered_map<int,unordered_map<int,float>> simMatrixLabelsInv;
    unordered_map<int,vector<pair<int,float>>> simMatrixLabelsSorted; //for score computation
    unordered_map<int,vector<pair<int,float>>> simMatrixFeaturesTopK; //for candidate selection labels
    vector<pair<int,float>> empty_vec;
    vector<vector<pair<int,float>>> predictions;
};

#endif //LCIF_KNN_JOURNAL_FEATUREKNN_H
